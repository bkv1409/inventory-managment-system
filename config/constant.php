<?php
/**
 * Created by PhpStorm.
 * User: vunq
 * Date: 2019-03-08
 * Time: 9:29 AM
 */

return [
    'DOCUMENT_TYPE' => [
        'IN' => ['key' => 'IN', 'value' => 'Nhập'],
        'OUT' => ['key' => 'OUT', 'value' => 'Xuất'],
        'CANCEL' => ['key' => 'DESTROY', 'value' => 'Hủy'],
        'REPAIR' => ['key' => 'REPAIR', 'value' => 'Sửa'],
//        'RENT' => ['key' => 'RENT', 'value' => 'Cho mượn thu hồi'],
    ],
    'MAX_RETURN_DATE' => '2038-01-19',
    'DOCUMENT_STATUS' => [
        'PENDING' => 'PENDING',
        'REJECTED' => 'REJECTED',
        'APPROVED' => 'APPROVED',
        '' => 'ALL',
    ],
    'MAX_EXCEL_EXPORT_ROW' => 1000,
    'ROW_NUM_IN_PAGE' => 10,
];


