<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('after-register','Auth\RegisterController@afterRegister')->name('register.after');


Route::group(['middleware' => ['auth']], function() {
    Route::get('/', 'HomeController@index')->name('home');
    Route::match(['post','get'],'/', 'HomeController@index')->name('homeTest');
    Route::resource('roles','RoleController');
    Route::resource('users','UserController');
    Route::patch('users/{user}/update-pass','UserController@updatePass')->name('users.updatePass');
    Route::get('users/{user}/edit-pass','UserController@editPass')->name('users.editPass');
    Route::get('me/user-profile','UserController@profile')->name('users.profile');
    Route::get('me/edit-profile','UserController@editProfile')->name('users.editProfile');
    Route::post('me/update-profile','UserController@updateProfile')->name('users.updateProfile');
    Route::get('me/edit-profile-password','UserController@editProfilePass')->name('users.editProfilePass');
    Route::post('me/update-profile-password','UserController@updateProfilePass')->name('users.updateProfilePass');
    Route::resource('products','ProductController');
    Route::resource('type2s','Type2Controller');

    Route::resource('models','ModelController');
    Route::get('models-export-excel', 'ModelController@exportExcel')->name('models.exportExcel');

    Route::resource('files','FileController')->only([
        'index', 'show', 'edit', 'update','destroy'
    ]);
    // File + Document Route
    Route::post('files/dropzone-store', ['as'=>'files.dropzoneStore','uses'=>'FileController@dropzoneStore']);
    Route::delete('files/dropzone-delete/{fileName}', ['as'=>'files.dropzoneDelete','uses'=>'FileController@dropzoneDelete']);
    Route::get('files/{file}/download', ['as'=>'files.download','uses'=>'FileController@download']);

    // Document Route
    Route::resource('documents', 'DocumentController');
    Route::get('documents/{document}/approve', 'DocumentController@approve')->name('documents.approve');
    Route::delete('documents/{document}/reject', 'DocumentController@reject')->name('documents.reject');
    Route::get('documents/{document}/preview', 'DocumentController@preview')->name('documents.preview');
    Route::get('documents/{document}/preview-part', 'DocumentController@previewPart')->name('documents.previewPart');
    Route::get('documents/{document}/pdf', 'DocumentController@printPdf')->name('documents.printPdf');
    Route::get('documents/{document}/pdf/download', 'DocumentController@downloadPdf')->name('documents.downloadPdf');
    Route::get('documents-export-excel', 'DocumentController@exportExcel')->name('documents.exportExcel');
    Route::get('documents/{document}/files/edit', 'DocumentController@editFiles')->name('documents.editFiles');
    Route::post('documents/{document}/files/update', 'DocumentController@updateFiles')->name('documents.updateFiles');

    // AJAX route
    Route::get('member/ajaxSearch', ['as' => 'member.ajaxSearch', 'uses' => 'MemberController@ajaxSearch']);
    Route::get('ajax-inventory','InventoryController@ajaxSearch')->name('ajax.inventorySearch');
    Route::get('ajax-category','ModelController@ajaxCategory')->name('ajax.category');
//    Route::get('ajax-member','MemberController@ajaxSearch')->name('ajax.member');

    // Inventory route
    Route::get('inventory/list-inventory', ['as' => 'inventory.index', 'uses' => 'InventoryController@index']);
    Route::get('inventory/create', ['as' => 'inventory.create', 'uses' => 'InventoryController@create']);
    Route::post('inventory/store', ['as' => 'inventory.store', 'uses' => 'InventoryController@store']);
    Route::put('inventory/update/{id}', ['as' => 'inventory.update', 'uses' => 'InventoryController@update']);
    Route::get('inventory/show/{id}', ['as' => 'inventory.show', 'uses' => 'InventoryController@show']);
    Route::get('inventory/edit/{id}', ['as' => 'inventory.edit', 'uses' => 'InventoryController@edit']);
    Route::delete('inventory/destroy/{id}', ['as' => 'inventory.destroy', 'uses' => 'InventoryController@destroy']);
    Route::get('inventory/import', ['as' => 'inventory.import', 'uses' => 'InventoryController@import']);
    Route::post('inventory/handleExcel', ['as' => 'inventory.handleExcel', 'uses' => 'InventoryController@handleExcel']);
    Route::post('inventory/saveDataExcel', ['as' => 'inventory.saveDataExcel', 'uses' => 'InventoryController@saveDataExcel']);
    Route::get('inventory/getType2s', ['as' => 'inventory.getType2s', 'uses' => 'InventoryController@getType2s']);
    Route::get('inventory/getModelName', ['as' => 'inventory.getModelName', 'uses' => 'InventoryController@getModelName']);
    Route::post('inventory/exportFile', ['as' => 'inventory.exportFile', 'uses' => 'InventoryController@exportFile']);
//inventory/import


    // Member route
    Route::get('member/list-member', ['as' => 'member.index', 'uses' => 'MemberController@index']);
    Route::get('member/create', ['as' => 'member.create', 'uses' => 'MemberController@create']);
    Route::post('member/store', ['as' => 'member.store', 'uses' => 'MemberController@store']);
    Route::put('member/update/{id}', ['as' => 'member.update', 'uses' => 'MemberController@update']);
    Route::get('member/show/{id}', ['as' => 'member.show', 'uses' => 'MemberController@show']);
    Route::get('member/edit/{id}', ['as' => 'member.edit', 'uses' => 'MemberController@edit']);
    Route::delete('member/destroy/{id}', ['as' => 'member.destroy', 'uses' => 'MemberController@destroy']);
    Route::get('member-export-excel', 'MemberController@exportExcel')->name('member.exportExcel');
    Route::get('member/import', 'MemberController@import')->name('member.import');
    Route::post('member/handleExcel', ['as' => 'member.handleExcel', 'uses' => 'MemberController@handleExcel']);
    Route::post('member/saveDataExcel', ['as' => 'member.saveDataExcel', 'uses' => 'MemberController@saveDataExcel']);
    Route::post('member/exportFile', ['as' => 'member.exportFile', 'uses' => 'MemberController@exportFile']);

    // Unit route
    Route::get('unit/list-unit', ['as' => 'unit.index', 'uses' => 'UnitController@index']);
    Route::get('unit/create', ['as' => 'unit.create', 'uses' => 'UnitController@create']);
    Route::post('unit/store', ['as' => 'unit.store', 'uses' => 'UnitController@store']);
    Route::put('unit/update/{id}', ['as' => 'unit.update', 'uses' => 'UnitController@update']);
    Route::get('unit/show/{id}', ['as' => 'unit.show', 'uses' => 'UnitController@show']);
    Route::get('unit/edit/{id}', ['as' => 'unit.edit', 'uses' => 'UnitController@edit']);
    Route::delete('unit/destroy/{id}', ['as' => 'unit.destroy', 'uses' => 'UnitController@destroy']);

});


