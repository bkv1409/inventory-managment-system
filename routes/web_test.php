<?php
/**
 * Created by PhpStorm.
 * User: FR_PL
 * Date: 2019-03-04
 * Time: 10:54 AM
 */

Route::get('/welcome', function () {
    return view('welcome');
});

Route::get('select2-autocomplete', 'Select2AutocompleteController@layout');
Route::get('select2-autocomplete-ajax', 'Select2AutocompleteController@dataAjax');

Route::get('dropzone', 'HomeController@dropzone');
Route::post('dropzone/store', ['as'=>'dropzone.store','uses'=>'HomeController@dropzoneStore']);

Route::get('/detect-face', 'SampleController@detectFaces');

Route::get('lte/advance', function () {
   return view('examples.adminlte_advance');
});
Route::get('test-pdf', 'OrderController@testPdf');
Route::get('test-document-preview', 'DocumentController@testPreviewPdf');
Route::get('test-document-pdf', 'DocumentController@testDownloadPdf');
