var Import = {
    maxed: false,
    list_error_record: [],
    handsontable: null,
    getInitData: function () {
        this.dataLength = initData != null ? initData.length : 0;
        return initData;

    },

    trigger: function () {

        $("#btnSaveImport").click(function () {

            var data = hot.getSourceData();

            if (data.length === 0) {
                alert("No  record not found");
                return;
            }

            $("#resultTable_validate").hide();
            $("#resultTable").show();

            $("#totalRecords").html('0');
            $("#errRecords").html('0');
            $("#sucRecords").html('0');

            Import.currentSaveIndex = 0;
            initData = [];
            Import.list_error_record = [];
            Import.saveData(data);

        });

        $("#export_file").click(function () {
            if (Import.list_error_record.length === 0) {
                alert("No error record not found");
                return;
            }
            Import.exportData();

        });
    },
    exportData: function () {
        $("#lbl_import_success").hide();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: URL_EXPORT_MEMBER,
            data: {
                "list_member_error":JSON.stringify(Import.list_error_record),
            },
            dataType: 'json',
            type: 'POST',
            success: function (response) {
                var a = document.createElement("a");
                a.href = response.file;
                a.download = response.name;
                document.body.appendChild(a);
                a.click();
                a.remove();
            },
            error: function () {
                alert('Error export data');
                return false;

            }
        });
    },

    saveData: function (data) {
        if (this.currentSaveIndex >= data.length) {
            $("#export_file").show();
            $("#lbl_import_success").show();

            $("#btnSaveImport").hide();
            Import.loadData(initData);
            return;
        }

        var len = 4;
        var list = [];
        var current = this.currentSaveIndex;
        var i = current;


        var dataLength = data.length;
        for (i = current; i < current + len && i < dataLength; i++) {
            if (data[i].lg_id !== null) {
                list.push(data[i]);
            }
        }

        this.currentSaveIndex = i;
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: URL_IMPORT_MEMBER,
            data: {
                "list_member": JSON.stringify(list),
            },
            dataType: 'json',
            type: 'POST',
            success: function (res) {
                if (res === "Done") {
                    $("#export_file").show();
                    $("#lbl_import_success").show();

                    $("#btnSaveImport").hide();
                    Import.loadData(initData);
                    return;
                }


                var length = res.arrRes.length;
                var status = "";

                for (var i = 0; i < length; i++) {
                    status = res.arrRes[i].status_cp;

                    if (status === "OK") {
                        $("#sucRecords").html(parseInt($("#sucRecords").html()) + 1);
                        initData.push(res.arrRes[i]);
                    } else {
                        $("#errRecords").html(parseInt($("#errRecords").html()) + 1);
                        initData.push(res.arrRes[i]);
                        Import.list_error_record.push(res.arrRes[i])
                    }
                    $("#totalRecords").html(parseInt($("#totalRecords").html()) + 1);
                }

                Import.saveData(data);
            },
            error: function () {

                var list_lenght = list.length;
                for (var i = 0; i < list_lenght; i++) {
                    if (list[i].status_cp === 'OK') {
                        list[i].status_cp = "Insert failed";
                    }
                    initData.push(list[i]);
                }
                $("#lbl_import_err").show();
                $("#btnSaveImport").hide();
                $("#errRecords").html(parseInt($("#errRecords").html()) + list.length);

                Import.loadData(initData);
            }
        });

    },

    buildTable: function () {
        var $container = $("#example1");
        var $parent = $container.parent();
        var resizeTimeout, availableWidth, availableHeight, $window = $(window),
            $example1 = $('#example1');

        var calculateSize = function () {
            var offset = $example1.offset();
            availableWidth = $window.width() - offset.left + $window.scrollLeft();
            availableHeight = $window.height() - offset.top + $window.scrollTop();
        };
        $window.on('resize', calculateSize);


        $('.maximize').on('click', function () {
            Import.maxed = !Import.maxed;
            $example1.handsontable('render');
        });

        Import.loadData(Import.getInitData());
        Import.trigger();
    },

    buildColumns: function (data) {
        var columns = [
            {
                data: "employee",
            }, {
                data: "status",
            }, {
                data: "lastname",
            }, {
                data: "middle",
            }, {
                data: "firstname",
            },
            {
                data: "email_address",
            }, {
                data: "phone_number",
            },
            {
                data: "status_cp",
            }
        ];

        return columns;
    },

    loadData: function (data) {
        var columns = this.buildColumns(data);
        var length_data = data.length;
        for (var i = 0; i < length_data; i++) {
            status = data[i].status_cp;

            if (status === "OK") {
                //
                $("#validRecord").html(parseInt($("#validRecord").html()) + 1);

            } else {
                $("#invalidRecord").html(parseInt($("#invalidRecord").html()) + 1);

            }
            $("#totalRecordsValidate").html(parseInt($("#totalRecordsValidate").html()) + 1);
        }

        var hot,
            ex1 = document.getElementById("example1");

        window.hot = new Handsontable(ex1, {
            readOnly: true,
            data: data,
            autoWrapRow: true,
            contextMenu: true,
            rowHeaders: true,
            colHeaders: true,
            manualColumnResize: true,
            manualRowResize: true,
            colWidths: [120, 100, 100, 100, 100, 200, 150,100],
            startRows: 12,
            startCols: 15,
            minSpareRows: 25,
            scrollH: 'auto',
            scrollV: 'auto',
            colHeaders: [
                "EMPLOYEE",
                "STATUS",
                "LASTNAME",
                "MIDDLE",
                "FIRSTNAME",
                "EMAIL_ADDRESS",
                "PHONE_NUMBER",

                "Message"

            ],
            columnSorting: true,
            currentRowClassName: 'currentRow',
            currentColClassName: 'currentCol',
            columns: columns,
            width: function () {
                if (Import.maxed && availableWidth === void 0) {
                    //	calculateSize();
                }
                return Import.maxed ? availableWidth : 1300;
            },
            height: function () {
                if (Import.maxed && availableHeight === void 0) {
                    //	calculateSize();
                }
                return Import.maxed ? availableHeight : 300;
            },
            afterChange: function (change, source) {

            },
            cells: function (row, col, prop) {

                var cellProperties = {};
                var dataInstance = this.instance.getSourceData();
                var rowDataStatus = dataInstance[row].status_cp;
                if (
                    rowDataStatus != null && rowDataStatus != 'OK') {
                    cellProperties.renderer = highlightRow;
                } else {
                    cellProperties.renderer = unhighlightRow;
                }

                return cellProperties;
            }
        });

        this.handsontable = $("#example1").data('handsontable');

    },

};

function highlightRow(instance, td, row, col, prop, value, cellProperties) {
    Handsontable.renderers.TextRenderer.apply(this, arguments);
    td.style.fontWeight = 'bold';
    td.style.color = 'red';
    td.style.background = '#CEC';
}

function unhighlightRow(instance, td, row, col, prop, value, cellProperties) {
    Handsontable.renderers.TextRenderer.apply(this, arguments);
    td.style.fontWeight = 'normal';
    td.style.color = 'black';
    td.style.background = '#fff';
}