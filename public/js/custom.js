var CustomJs = {
    ConfirmDelete: function() {
        var x = confirm("Are you sure you want to delete this item ?");
        if (x)
            return true;
        else
            return false;
    },
    init: function() {
        $(document).on('click', '#btn_clear_all', function(e) {

            $("form#form_clear_all :input[type=text],select,checkbox,radio").each(function() {
                $(this).val('');
                window.location.reload();
            });
        });

        $(document).on('click', '#btn_export', function(e) {
            e.preventDefault();
            let href = '/member-export-excel' + '?' + $("#form_clear_all").serialize();
            window.location.href = href;

        });
    },
};