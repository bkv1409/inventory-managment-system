

$(document).ready(function() {
    $(".type2_id").prop("disabled", true);

    $(".it_tag").prop("readonly", true);
    var tempItTag='';

    if($(".it_tag").val()){
        tempItTag =$(".it_tag").val();
    }

    console.log("--gia tri khoi tao it tag" + tempItTag);

    if($(".type1_id").val()  == 1 || $(".type1_id").val() == 2){
        $(".type2_id").prop("disabled", false);
        $(".it_tag").prop("readonly", false);
    }else{
        var dateNow = new Date();
        var millisecondsVal = dateNow.getTime();

        $(".it_tag").val("Meterial_"+millisecondsVal);
        $(".it_tag").prop("readonly", true);

    }

    $('.type1_id').on('change', function() {
        if(this.value){
            $(".type2_id").prop('disabled', false);
        }else{
            $(".type2_id").prop('disabled', true);
        }
        // add set quantity >=1 with only type1!=3
        if (this.value == 3) {
            $('#quantity').prop('readonly', false);
        } else {
            $('#quantity').prop('readonly', true);
            $('#quantity').val(1);
        }
            console.log("=============" + this.value);

        if (this.value == 1 || this.value == 2) {
            if(tempItTag){
                $(".it_tag").val(tempItTag);
            }else{
                $(".it_tag").val('');
            }

            $(".it_tag").prop("readonly", false);
        }else{
            var nowDate = new Date();
            var millisecondsValue = nowDate.getTime();
           $(".it_tag").val("Meterial_"+millisecondsValue);
            $(".it_tag").prop("readonly", true);
        }
    });



    $("#note").prop('disabled', true);

    if($("#status").val()==2){
        $("#note").prop('disabled', false);
    }
    $('#status').on('change', function() {
        if(this.value==='NG'){
            $("#note").prop('disabled', false);
        }else{
            $("#note").prop('disabled', true);
        }
    });


    $("#import_date").datetimepicker({
        format:'YYYY-MM-DD',
        defaultDate: moment(),
        sideBySide: true,
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-arrow-up",
            down: "fa fa-arrow-down"
        }
    });

    $("#return_date").datetimepicker({
        format:'YYYY-MM-DD',
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-arrow-up",
            down: "fa fa-arrow-down"
        }
    });

});





