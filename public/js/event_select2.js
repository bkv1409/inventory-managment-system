$(document).ready(function () {

    $('.type2_id').select2({
        ajax: {
            url: URL_GET_TYPE2,
            dataType: 'json',
            delay: 250,
            data : function(params) {
                return {
                    q : params.term, // search term
                    type1_id: $(".type1_id").val()
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.name,
                            id: item.id
                        }
                    })
                };
            },
            cache: true
        }

    });

    $('.model_id').select2({
        ajax: {
            url: URL_GET_MODEL_NAME,
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.model_name,
                            id: item.id
                        }
                    })
                };
            },
            cache: true
        }

    });


    $('.current_user_id').select2({
        ajax: {
            url: URL_GET_MEMBER_NAME,
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.name,
                            id: item.id
                        }
                    })
                };
            },
            cache: true
        }

    });


})
