<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToInventoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inventories', function (Blueprint $table) {
            $table->index(['type1_id', 'type2_id','model_id','status','serial_number','deleted_at'],'idx_inventory_search_6');
            $table->index(['type2_id','model_id','status','serial_number','deleted_at'],'idx_inventory_search_5');
            $table->index(['model_id','status','serial_number','deleted_at'],'idx_inventory_search_4');
            $table->index(['status','serial_number','deleted_at'],'idx_inventory_search_3');
            $table->index(['serial_number','deleted_at'],'idx_inventory_search_2');
            $table->index(['deleted_at'],'idx_inventory_search_1');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inventories', function (Blueprint $table) {
            $table->dropIndex('idx_inventory_search_1');
            $table->dropIndex('idx_inventory_search_2');
            $table->dropIndex('idx_inventory_search_3');
            $table->dropIndex('idx_inventory_search_4');
            $table->dropIndex('idx_inventory_search_5');
            $table->dropIndex('idx_inventory_search_6');

        });
    }
}
