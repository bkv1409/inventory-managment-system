<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateModelDictionaryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('model_dictionary', function(Blueprint $table)
		{
//			$table->bigInteger('id')->primary();
            $table->bigInteger('id', true);
			$table->string('model_name')->nullable();
			$table->string('category')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('model_dictionary');
	}

}
