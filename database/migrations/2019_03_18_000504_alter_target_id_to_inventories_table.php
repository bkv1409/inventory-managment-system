<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTargetIdToInventoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inventories', function (Blueprint $table) {
            $table->bigInteger('target_id')->nullable()->comment('Trường để lưu trữ thiết bị đích mà thiết bị này dùng để sửa. Trường để tự join với chính bảng inventories');

            $table->foreign('target_id','fk_self_inventories')
                ->references('id')
                ->on('inventories')
//                ->onDelete('cascade');
                ->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inventories', function (Blueprint $table) {
            $table->dropForeign('fk_self_inventories');
        });
    }
}
