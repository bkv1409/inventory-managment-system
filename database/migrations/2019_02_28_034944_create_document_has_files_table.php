<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDocumentHasFilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('document_has_files', function(Blueprint $table)
		{
			$table->bigInteger('document_id');
			$table->bigInteger('file_id')->index('fk_file_id');
			$table->primary(['document_id','file_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('document_has_files');
	}

}
