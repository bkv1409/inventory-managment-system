<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAddColumnToDocumentHasInventoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('document_has_inventories', function (Blueprint $table) {
            $table->integer('delta_quantity')->default(0)->comment('Lưu trữ giá trị thay đổi quantity của mỗi inventory ứng với 1 documents');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('document_has_inventories', function (Blueprint $table) {
            //
        });
    }
}
