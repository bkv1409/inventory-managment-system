<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToDocumentHasFilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('document_has_files', function(Blueprint $table)
		{
			$table->foreign('document_id', 'fk_document_id')->references('id')->on('documents')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('file_id', 'fk_file_id')->references('id')->on('files')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('document_has_files', function(Blueprint $table)
		{
			$table->dropForeign('fk_document_id');
			$table->dropForeign('fk_file_id');
		});
	}

}
