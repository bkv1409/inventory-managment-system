<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToDocumentHasInventoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('document_has_inventories', function(Blueprint $table)
		{
			$table->foreign('document_id', 'fk_document_id2')->references('id')->on('documents')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('inventory_id', 'fk_inventory_id1')->references('id')->on('inventories')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('document_has_inventories', function(Blueprint $table)
		{
			$table->dropForeign('fk_document_id2');
			$table->dropForeign('fk_inventory_id1');
		});
	}

}
