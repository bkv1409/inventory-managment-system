<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDocumentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('documents', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->string('document_no')->nullable();
			$table->bigInteger('deliver_id')->nullable()->index('fk_deliver');
			$table->bigInteger('receiver_id')->nullable()->index('fk_receiver');
			$table->string('reason')->nullable()->comment('Lý do');
			$table->string('note')->nullable()->comment('Ghi chú');
			$table->bigInteger('approved_by')->nullable()->comment('Người duyệt, mapping sang bảng member');
			$table->string('status', 30)->nullable()->comment('Trạng thái document: APPROVED, PENDING, REJECT');
			$table->string('reject_reason')->nullable()->comment('Lý do từ chối');
			$table->string('type', 30)->nullable()->comment('Loại document: Nhập: IN, Xuất: OUT, Hủy: CANCEL, Sửa: REPAIR, Cho mượn/Thu hồi: RENT');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('documents');
	}

}
