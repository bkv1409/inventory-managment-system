<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToInventoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('inventories', function(Blueprint $table)
		{
			$table->foreign('model_id', 'fk_model_dic')->references('id')->on('model_dictionary')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('type1_id', 'fk_type1')->references('id')->on('type1s')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('type2_id', 'fk_type2')->references('id')->on('type2s')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('inventories', function(Blueprint $table)
		{
			$table->dropForeign('fk_model_dic');
			$table->dropForeign('fk_type1');
			$table->dropForeign('fk_type2');
		});
	}

}
