<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDocumentHasInventoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('document_has_inventories', function(Blueprint $table)
		{
			$table->bigInteger('document_id');
			$table->bigInteger('inventory_id')->index('fk_inventory_id1');
			$table->primary(['document_id','inventory_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('document_has_inventories');
	}

}
