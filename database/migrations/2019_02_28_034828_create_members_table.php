<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMembersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('members', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->string('name')->nullable();
			$table->string('lg_id')->nullable();
			$table->string('email')->nullable();
			$table->string('phone_no', 11)->nullable();
			$table->boolean('is_lg_member')->default(1)->comment('1: lg_member, 0: non_lg_member');
			$table->string('department')->nullable();
			$table->string('company')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('members');
	}

}
