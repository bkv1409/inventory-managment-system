<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('documents', function (Blueprint $table) {
            $table->index(['document_no', 'status','type','reason','note','deliver_id','receiver_id','created_at','updated_at','deleted_at'],'idx_document_search_10');
            $table->index(['status','type','reason','note','deliver_id','receiver_id','created_at','updated_at','deleted_at'],'idx_document_search_9');
            $table->index(['type','reason','note','deliver_id','receiver_id','created_at','updated_at','deleted_at'],'idx_document_search_8');
            $table->index(['reason','note','deliver_id','receiver_id','created_at','updated_at','deleted_at'],'idx_document_search_7');
            $table->index(['note','deliver_id','receiver_id','created_at','updated_at','deleted_at'],'idx_document_search_6');
            $table->index(['deliver_id','receiver_id','created_at','updated_at','deleted_at'],'idx_document_search_5');
            $table->index(['receiver_id','created_at','updated_at','deleted_at'],'idx_document_search_4');
            $table->index(['created_at','updated_at','deleted_at'],'idx_document_search_3');
            $table->index(['updated_at','deleted_at'],'idx_document_search_2');
            $table->index(['deleted_at'],'idx_document_search_1');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('documents', function (Blueprint $table) {
            $table->dropIndex('idx_document_search_10');
            $table->dropIndex('idx_document_search_9');
            $table->dropIndex('idx_document_search_8');
            $table->dropIndex('idx_document_search_7');
            $table->dropIndex('idx_document_search_6');
            $table->dropIndex('idx_document_search_5');
            $table->dropIndex('idx_document_search_4');
            $table->dropIndex('idx_document_search_3');
            $table->dropIndex('idx_document_search_2');
            $table->dropIndex('idx_document_search_1');
        });
    }
}
