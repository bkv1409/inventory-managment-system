<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToDocumentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('documents', function(Blueprint $table)
		{
			$table->foreign('deliver_id', 'fk_deliver')->references('id')->on('members')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('receiver_id', 'fk_receiver')->references('id')->on('members')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('documents', function(Blueprint $table)
		{
			$table->dropForeign('fk_deliver');
			$table->dropForeign('fk_receiver');
		});
	}

}
