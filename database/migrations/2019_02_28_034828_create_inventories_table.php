<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInventoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('inventories', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->integer('type1_id')->nullable()->index('fk_type1')->comment('Có 3 loại: Device, Tools, Material/part');
			$table->integer('type2_id')->nullable()->index('fk_type2');
			$table->bigInteger('model_id')->nullable()->index('fk_model_dic');
			$table->string('it_tag')->nullable()->comment('For device + tools');
			$table->string('fa_no')->nullable()->comment('Số FA, tham chiếu hệ thống quản lý tài sản');
			$table->string('serial_number')->nullable();
			$table->string('unit')->nullable()->comment('Định nghĩa trước');
			$table->text('specs', 65535)->nullable()->comment('Điền nhiều');
			$table->string('status', 20)->nullable()->comment('OK|NG|NOT');
			$table->text('note', 65535)->nullable()->comment('free text, khi status = NG');
			$table->dateTime('import_date')->nullable()->comment('Ngày nhập mới, Default là ngày hiện tại, cho chọn');
			$table->bigInteger('current_user_id')->nullable()->comment('mapping tới bảng member: LG Member, Non-LG Member');
			$table->string('deliver')->nullable()->comment('nhung.mai | congvan.doan /username');
			$table->dateTime('return_date')->nullable()->comment('Ngày hẹn trả');
			$table->string('internal_status', 30)->nullable()->comment('Trạng thái nội bộ, để hệ thống quản lý: IN, OUT, RENT, USED');
			$table->timestamps();
			$table->softDeletes();
			$table->integer('quantity')->default(1)->comment('Số lượng thiết bị, mặc định 1. >=1 với vật liệu tiêu hao');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('inventories');
	}

}
