<?php

use Illuminate\Database\Seeder;

class Type2sTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('type2s')->delete();
        
        \DB::table('type2s')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'PC',
                'type1_id' => 2,
                'created_at' => '2019-03-03 07:11:03',
                'updated_at' => '2019-03-03 08:03:35',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Laptop',
                'type1_id' => 1,
                'created_at' => '2019-03-03 08:04:35',
                'updated_at' => '2019-03-03 08:04:35',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Switch1',
                'type1_id' => 1,
                'created_at' => '2019-03-03 15:08:00',
                'updated_at' => '2019-03-03 15:08:00',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Switch 10 Port',
                'type1_id' => 1,
                'created_at' => '2019-03-03 15:09:32',
                'updated_at' => '2019-03-03 15:09:32',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Router',
                'type1_id' => 1,
                'created_at' => '2019-03-03 15:09:48',
                'updated_at' => '2019-03-03 15:09:48',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Cat10',
                'type1_id' => 3,
                'created_at' => '2019-03-03 15:10:06',
                'updated_at' => '2019-03-03 15:10:06',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Kìm Bấm Mạng',
                'type1_id' => 2,
                'created_at' => '2019-03-03 15:10:26',
                'updated_at' => '2019-03-03 15:10:51',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}