<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('permissions')->delete();
        
        \DB::table('permissions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'role-list',
                'guard_name' => 'web',
                'created_at' => '2019-03-02 19:10:13',
                'updated_at' => '2019-03-02 19:10:13',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'role-create',
                'guard_name' => 'web',
                'created_at' => '2019-03-02 19:10:13',
                'updated_at' => '2019-03-02 19:10:13',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'role-edit',
                'guard_name' => 'web',
                'created_at' => '2019-03-02 19:10:13',
                'updated_at' => '2019-03-02 19:10:13',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'role-delete',
                'guard_name' => 'web',
                'created_at' => '2019-03-02 19:10:13',
                'updated_at' => '2019-03-02 19:10:13',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'product-list',
                'guard_name' => 'web',
                'created_at' => '2019-03-02 19:10:13',
                'updated_at' => '2019-03-02 19:10:13',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'product-create',
                'guard_name' => 'web',
                'created_at' => '2019-03-02 19:10:13',
                'updated_at' => '2019-03-02 19:10:13',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'product-edit',
                'guard_name' => 'web',
                'created_at' => '2019-03-02 19:10:13',
                'updated_at' => '2019-03-02 19:10:13',
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'product-delete',
                'guard_name' => 'web',
                'created_at' => '2019-03-02 19:10:13',
                'updated_at' => '2019-03-02 19:10:13',
            ),
            8 => 
            array (
                'id' => 10,
                'name' => 'inventory-list',
                'guard_name' => 'web',
                'created_at' => '2019-03-02 19:16:50',
                'updated_at' => '2019-03-02 19:16:50',
            ),
            9 => 
            array (
                'id' => 11,
                'name' => 'inventory-create',
                'guard_name' => 'web',
                'created_at' => '2019-03-02 19:16:55',
                'updated_at' => '2019-03-02 19:16:55',
            ),
            10 => 
            array (
                'id' => 12,
                'name' => 'inventory-edit',
                'guard_name' => 'web',
                'created_at' => '2019-03-02 19:17:03',
                'updated_at' => '2019-03-02 19:17:03',
            ),
            11 => 
            array (
                'id' => 13,
                'name' => 'inventory-delete',
                'guard_name' => 'web',
                'created_at' => '2019-03-02 19:17:23',
                'updated_at' => '2019-03-02 19:17:23',
            ),
            12 => 
            array (
                'id' => 14,
                'name' => 'inventory-approve',
                'guard_name' => 'web',
                'created_at' => '2019-03-02 19:18:39',
                'updated_at' => '2019-03-02 19:18:39',
            ),
            13 => 
            array (
                'id' => 15,
                'name' => 'document-list',
                'guard_name' => 'web',
                'created_at' => '2019-03-02 19:19:01',
                'updated_at' => '2019-03-02 19:19:01',
            ),
            14 => 
            array (
                'id' => 16,
                'name' => 'document-create',
                'guard_name' => 'web',
                'created_at' => '2019-03-02 19:19:08',
                'updated_at' => '2019-03-02 19:19:08',
            ),
            15 => 
            array (
                'id' => 17,
                'name' => 'document-edit',
                'guard_name' => 'web',
                'created_at' => '2019-03-02 19:19:17',
                'updated_at' => '2019-03-02 19:19:17',
            ),
            16 => 
            array (
                'id' => 18,
                'name' => 'document-delete',
                'guard_name' => 'web',
                'created_at' => '2019-03-02 19:19:21',
                'updated_at' => '2019-03-02 19:19:21',
            ),
            17 => 
            array (
                'id' => 19,
                'name' => 'document-approve',
                'guard_name' => 'web',
                'created_at' => '2019-03-02 19:19:28',
                'updated_at' => '2019-03-02 19:19:28',
            ),
            18 => 
            array (
                'id' => 20,
                'name' => 'config-list',
                'guard_name' => 'web',
                'created_at' => '2019-03-03 15:15:06',
                'updated_at' => '2019-03-03 15:15:06',
            ),
            19 => 
            array (
                'id' => 21,
                'name' => 'config-edit',
                'guard_name' => 'web',
                'created_at' => '2019-03-03 15:15:11',
                'updated_at' => '2019-03-03 15:15:11',
            ),
            20 => 
            array (
                'id' => 22,
                'name' => 'config-create',
                'guard_name' => 'web',
                'created_at' => '2019-03-03 15:15:39',
                'updated_at' => '2019-03-03 15:15:39',
            ),
            21 => 
            array (
                'id' => 23,
                'name' => 'config-delete',
                'guard_name' => 'web',
                'created_at' => '2019-03-03 15:15:45',
                'updated_at' => '2019-03-03 15:15:45',
            ),
        ));
        
        
    }
}