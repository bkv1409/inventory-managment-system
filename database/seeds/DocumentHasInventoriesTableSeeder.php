<?php

use Illuminate\Database\Seeder;

class DocumentHasInventoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('document_has_inventories')->delete();
        
        \DB::table('document_has_inventories')->insert(array (
            0 => 
            array (
                'document_id' => 1,
                'inventory_id' => 3,
                'delta_quantity' => 0,
            ),
            1 => 
            array (
                'document_id' => 2,
                'inventory_id' => 1,
                'delta_quantity' => 0,
            ),
            2 => 
            array (
                'document_id' => 2,
                'inventory_id' => 2,
                'delta_quantity' => 0,
            ),
            3 => 
            array (
                'document_id' => 3,
                'inventory_id' => 3,
                'delta_quantity' => 100,
            ),
            4 => 
            array (
                'document_id' => 4,
                'inventory_id' => 2,
                'delta_quantity' => 1,
            ),
            5 => 
            array (
                'document_id' => 5,
                'inventory_id' => 3,
                'delta_quantity' => 50,
            ),
            6 => 
            array (
                'document_id' => 6,
                'inventory_id' => 3,
                'delta_quantity' => 30,
            ),
            7 => 
            array (
                'document_id' => 7,
                'inventory_id' => 3,
                'delta_quantity' => 30,
            ),
            8 => 
            array (
                'document_id' => 8,
                'inventory_id' => 3,
                'delta_quantity' => 30,
            ),
            9 => 
            array (
                'document_id' => 9,
                'inventory_id' => 3,
                'delta_quantity' => 30,
            ),
            10 => 
            array (
                'document_id' => 10,
                'inventory_id' => 3,
                'delta_quantity' => 30,
            ),
            11 => 
            array (
                'document_id' => 11,
                'inventory_id' => 3,
                'delta_quantity' => 70,
            ),
            12 => 
            array (
                'document_id' => 13,
                'inventory_id' => 3,
                'delta_quantity' => 140,
            ),
            13 => 
            array (
                'document_id' => 14,
                'inventory_id' => 1,
                'delta_quantity' => 1,
            ),
            14 => 
            array (
                'document_id' => 15,
                'inventory_id' => 3,
                'delta_quantity' => 0,
            ),
        ));
        
        
    }
}