<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('roles')->delete();
        
        \DB::table('roles')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Admin',
                'guard_name' => 'web',
                'created_at' => '2019-03-02 19:20:45',
                'updated_at' => '2019-03-02 19:20:45',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Normal',
                'guard_name' => 'web',
                'created_at' => '2019-03-02 19:22:43',
                'updated_at' => '2019-03-02 19:22:43',
            ),
        ));
        
        
    }
}