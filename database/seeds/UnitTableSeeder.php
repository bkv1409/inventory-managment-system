<?php

use Illuminate\Database\Seeder;

class UnitTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('unit')->delete();
        
        \DB::table('unit')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Chiếc',
                'created_at' => '2019-03-14 23:16:38',
                'updated_at' => '2019-03-14 23:16:38',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Bộ',
                'created_at' => '2019-03-14 23:16:48',
                'updated_at' => '2019-03-14 23:16:48',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'm',
                'created_at' => '2019-03-14 23:17:02',
                'updated_at' => '2019-03-14 23:17:02',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'cuộn',
                'created_at' => '2019-03-14 23:17:12',
                'updated_at' => '2019-03-14 23:17:12',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}