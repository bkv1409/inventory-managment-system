<?php

use Illuminate\Database\Seeder;

class InventoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('inventories')->delete();
        
        \DB::table('inventories')->insert(array (
            0 => 
            array (
                'id' => 1,
                'type1_id' => 1,
                'type2_id' => 2,
                'model_id' => 1,
                'it_tag' => 'ittag1',
                'fa_no' => 'fanumber1',
                'serial_number' => 'AABC-11111',
                'unit' => 1,
                'specs' => 'zzzzzzzzzzzzz',
                'status' => 'OK',
                'note' => '',
                'import_date' => '2019-03-14 00:00:00',
                'current_user_id' => 1,
                'deliver' => 'BKV',
                'return_date' => '2019-03-15 00:00:00',
                'internal_status' => 'IN',
                'created_at' => '2019-03-14 23:56:34',
                'updated_at' => '2019-03-15 03:14:45',
                'deleted_at' => NULL,
                'quantity' => 1,
                'is_pending' => 0,
            ),
            1 => 
            array (
                'id' => 2,
                'type1_id' => 1,
                'type2_id' => 1,
                'model_id' => 2,
                'it_tag' => 'ittag2',
                'fa_no' => 'fanumber2',
                'serial_number' => 'NMNM345',
                'unit' => 1,
                'specs' => 'sssss',
                'status' => 'OK',
                'note' => '',
                'import_date' => '2019-03-15 00:00:00',
                'current_user_id' => 1,
                'deliver' => 'BKV',
                'return_date' => '2019-03-31 00:00:00',
                'internal_status' => 'INIT',
                'created_at' => '2019-03-15 00:03:10',
                'updated_at' => '2019-03-15 01:50:24',
                'deleted_at' => NULL,
                'quantity' => 1,
                'is_pending' => 0,
            ),
            2 => 
            array (
                'id' => 3,
                'type1_id' => 3,
                'type2_id' => 6,
                'model_id' => 4,
                'it_tag' => 'ittag3',
                'fa_no' => 'fanumber3',
                'serial_number' => 'ZYX-789',
                'unit' => 3,
                'specs' => 'aaaa',
                'status' => 'OK',
                'note' => '',
                'import_date' => '2019-03-15 00:00:00',
                'current_user_id' => 1,
                'deliver' => 'BKV',
                'return_date' => '2019-03-15 00:00:00',
                'internal_status' => 'IN',
                'created_at' => '2019-03-15 00:04:31',
                'updated_at' => '2019-03-15 03:23:48',
                'deleted_at' => NULL,
                'quantity' => 0,
                'is_pending' => 0,
            ),
        ));
        
        
    }
}