<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Nguyen Quang Vu',
                'email' => 'admin@admin.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$M5hOYpIfFmIPKUQQ06QFgu4H08Z8H8UbyS9kwujQRuBJbvACy/Qi6',
                'remember_token' => 'aXZ1MLaNftZj6LzkkHKspsZ7e7742SnRooqr1ysS5sDj8kuW8PUCOS1qFoTk',
                'created_at' => '2019-02-28 22:06:43',
                'updated_at' => '2019-02-28 22:06:43',
                'status' => 0,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Nguyen VU3',
                'email' => 'admin3@admin.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$Yo9cH17OtChIe/U/POkNv.ZQIFRqKSloQt4YFo5S60o3vkSCdQfgu',
                'remember_token' => 'cadHeyJ0RGXURrupjsZrR8gfwdEx1vwVvxMZMtm9eOJ3dB8mPSmlbWvFEIku',
                'created_at' => '2019-03-02 16:33:48',
                'updated_at' => '2019-03-15 03:50:01',
                'status' => 1,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'BKV',
                'email' => 'bkv1409@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$ZloQzI4KBpNPGxjAmPo4Uu3aoX5L2GY22As2zIoDlQDFv4SpYDRdW',
                'remember_token' => 'O2YTRFNwEuAW8epvGsmfCMy18bSDOt01hMoIfGqcU78FACdI6fveDm3mWRMA',
                'created_at' => '2019-03-02 16:36:13',
                'updated_at' => '2019-03-04 11:20:36',
                'status' => 1,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Vu4 123',
                'email' => 'admin4@admin.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$fagP1gAN56/9erfQqNjvdekQyYdujId8yv7jpRRMjgKG/l4sYDNGK',
                'remember_token' => 'PwokMwxDyID6BnFa4styqefx7TjWoQE9GrOaien7UzeTn34A2xCiuIHNLBl9',
                'created_at' => '2019-03-02 16:46:53',
                'updated_at' => '2019-03-15 03:32:46',
                'status' => 1,
            ),
        ));
        
        
    }
}