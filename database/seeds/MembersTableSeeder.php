<?php

use Illuminate\Database\Seeder;

class MembersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('members')->delete();
        
        \DB::table('members')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Nguyễn Quang Vũ',
                'lg_id' => 'LG11111',
                'email' => 'vunq@gmail.com',
                'phone_no' => '0914826121',
                'is_lg_member' => 1,
                'department' => 'IT',
                'company' => 'LGE',
                'created_at' => '2019-03-14 23:19:57',
                'updated_at' => '2019-03-14 23:19:57',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Phạm Công Uyển',
                'lg_id' => NULL,
                'email' => 'uyenpc@gmail.com',
                'phone_no' => '09111123456',
                'is_lg_member' => 0,
                'department' => 'HR',
                'company' => 'NextMedia',
                'created_at' => '2019-03-14 23:21:03',
                'updated_at' => '2019-03-14 23:21:03',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Phạm Ngọc Chiến',
                'lg_id' => 'LG222222222222222',
                'email' => 'chienpn@gmail.com',
                'phone_no' => '0913243434',
                'is_lg_member' => 1,
                'department' => 'IT',
                'company' => 'LGE',
                'created_at' => '2019-03-14 23:54:55',
                'updated_at' => '2019-03-14 23:54:55',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}