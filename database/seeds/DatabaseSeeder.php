<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(ModelHasRolesTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(RoleHasPermissionsTableSeeder::class);
        $this->call(ModelHasPermissionsTableSeeder::class);
        $this->call(PasswordResetsTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
        $this->call(UnitTableSeeder::class);
        $this->call(Type1sTableSeeder::class);
        $this->call(Type2sTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(FilesTableSeeder::class);

        $this->call(MembersTableSeeder::class);
        $this->call(ModelDictionaryTableSeeder::class);
        $this->call(InventoriesTableSeeder::class);
        $this->call(DocumentsTableSeeder::class);

        $this->call(DocumentHasFilesTableSeeder::class);
        $this->call(DocumentHasInventoriesTableSeeder::class);
    }
}
