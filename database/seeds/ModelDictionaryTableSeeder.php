<?php

use Illuminate\Database\Seeder;

class ModelDictionaryTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('model_dictionary')->delete();
        
        \DB::table('model_dictionary')->insert(array (
            0 => 
            array (
                'id' => 1,
                'model_name' => 'Dell 7250',
                'category' => 'Dell',
                'created_at' => '2019-03-14 23:14:23',
                'updated_at' => '2019-03-14 23:14:23',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'model_name' => 'IBM 123',
                'category' => 'IBM',
                'created_at' => '2019-03-14 23:14:38',
                'updated_at' => '2019-03-14 23:14:38',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'model_name' => 'Dell 8500',
                'category' => 'Dell',
                'created_at' => '2019-03-14 23:15:00',
                'updated_at' => '2019-03-14 23:15:00',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'model_name' => 'Cat10-11-11',
                'category' => 'Cable',
                'created_at' => '2019-03-14 23:15:27',
                'updated_at' => '2019-03-14 23:15:27',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}