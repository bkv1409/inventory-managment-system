/*
 Navicat Premium Data Transfer

 Source Server         : aThanhPC_MariaDB_192.168.100.113_3306
 Source Server Type    : MariaDB
 Source Server Version : 100128
 Source Host           : 192.168.100.113:3306
 Source Schema         : lar57-admin-lte3

 Target Server Type    : MariaDB
 Target Server Version : 100128
 File Encoding         : 65001

 Date: 01/03/2019 19:16:21
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES (1, 'Ha Noi', NULL, NULL);
INSERT INTO `categories` VALUES (2, 'Viet Nam', NULL, NULL);
INSERT INTO `categories` VALUES (3, 'Ho Chi Minh', NULL, NULL);
INSERT INTO `categories` VALUES (4, 'Lang Son', NULL, NULL);
INSERT INTO `categories` VALUES (5, 'Gia Lai', NULL, NULL);
INSERT INTO `categories` VALUES (6, 'Kom Tum', NULL, NULL);

-- ----------------------------
-- Table structure for document_has_files
-- ----------------------------
DROP TABLE IF EXISTS `document_has_files`;
CREATE TABLE `document_has_files`  (
  `document_id` bigint(20) NOT NULL,
  `file_id` bigint(20) NOT NULL,
  PRIMARY KEY (`document_id`, `file_id`) USING BTREE,
  INDEX `fk_file_id`(`file_id`) USING BTREE,
  CONSTRAINT `fk_document_id` FOREIGN KEY (`document_id`) REFERENCES `documents` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_file_id` FOREIGN KEY (`file_id`) REFERENCES `files` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for document_has_inventories
-- ----------------------------
DROP TABLE IF EXISTS `document_has_inventories`;
CREATE TABLE `document_has_inventories`  (
  `document_id` bigint(20) NOT NULL,
  `inventory_id` bigint(20) NOT NULL,
  PRIMARY KEY (`document_id`, `inventory_id`) USING BTREE,
  INDEX `fk_inventory_id1`(`inventory_id`) USING BTREE,
  CONSTRAINT `fk_document_id2` FOREIGN KEY (`document_id`) REFERENCES `documents` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_inventory_id1` FOREIGN KEY (`inventory_id`) REFERENCES `inventories` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for documents
-- ----------------------------
DROP TABLE IF EXISTS `documents`;
CREATE TABLE `documents`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `document_no` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `deliver_id` bigint(20) NULL DEFAULT NULL,
  `receiver_id` bigint(20) NULL DEFAULT NULL,
  `reason` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT 'Lý do',
  `note` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT 'Ghi chú',
  `approved_by` bigint(20) NULL DEFAULT NULL COMMENT 'Người duyệt, mapping sang bảng member',
  `status` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT 'Trạng thái document: APPROVED, PENDING, REJECT',
  `reject_reason` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT 'Lý do từ chối',
  `type` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT 'Loại document: Nhập: IN, Xuất: OUT, Hủy: CANCEL, Sửa: REPAIR, Cho mượn/Thu hồi: RENT',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_deliver`(`deliver_id`) USING BTREE,
  INDEX `fk_receiver`(`receiver_id`) USING BTREE,
  CONSTRAINT `fk_deliver` FOREIGN KEY (`deliver_id`) REFERENCES `members` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_receiver` FOREIGN KEY (`receiver_id`) REFERENCES `members` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for files
-- ----------------------------
DROP TABLE IF EXISTS `files`;
CREATE TABLE `files`  (
  `id` bigint(20) NOT NULL,
  `file_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `path` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for inventories
-- ----------------------------
DROP TABLE IF EXISTS `inventories`;
CREATE TABLE `inventories`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type1_id` int(11) NULL DEFAULT NULL COMMENT 'Có 3 loại: Device, Tools, Material/part',
  `type2_id` int(11) NULL DEFAULT NULL,
  `model_id` bigint(20) NULL DEFAULT NULL,
  `it_tag` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT 'For device + tools',
  `fa_no` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT 'Số FA, tham chiếu hệ thống quản lý tài sản',
  `serial_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `unit` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT 'Định nghĩa trước',
  `specs` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL COMMENT 'Điền nhiều',
  `status` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT 'OK|NG|NOT',
  `note` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL COMMENT 'free text, khi status = NG',
  `import_date` datetime(0) NULL DEFAULT NULL COMMENT 'Ngày nhập mới, Default là ngày hiện tại, cho chọn',
  `current_user_id` bigint(20) NULL DEFAULT NULL COMMENT 'mapping tới bảng member: LG Member, Non-LG Member',
  `deliver` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT 'nhung.mai | congvan.doan',
  `return_date` datetime(0) NULL DEFAULT NULL COMMENT 'Ngày hẹn trả',
  `internal_status` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT 'Trạng thái nội bộ, để hệ thống quản lý: IN, OUT, RENT, USED',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `quantity` int(11) NOT NULL DEFAULT 1 COMMENT 'Số lượng thiết bị, mặc định 1. >=1 với vật liệu tiêu hao',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_type1`(`type1_id`) USING BTREE,
  INDEX `fk_type2`(`type2_id`) USING BTREE,
  INDEX `fk_model_dic`(`model_id`) USING BTREE,
  CONSTRAINT `fk_model_dic` FOREIGN KEY (`model_id`) REFERENCES `model_dictionary` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_type1` FOREIGN KEY (`type1_id`) REFERENCES `type1s` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_type2` FOREIGN KEY (`type2_id`) REFERENCES `type2s` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for members
-- ----------------------------
DROP TABLE IF EXISTS `members`;
CREATE TABLE `members`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `lg_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `phone_no` varchar(11) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `is_lg_member` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1: lg_member, 0: non_lg_member',
  `department` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `company` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (3, '2019_02_26_022012_create_permission_tables', 1);
INSERT INTO `migrations` VALUES (4, '2019_02_26_083708_create_category_table', 2);
INSERT INTO `migrations` VALUES (5, '2019_02_26_101051_create_products_table', 3);
INSERT INTO `migrations` VALUES (6, '2019_02_28_034828_create_documents_table', 4);
INSERT INTO `migrations` VALUES (7, '2019_02_28_034828_create_files_table', 4);
INSERT INTO `migrations` VALUES (8, '2019_02_28_034828_create_inventories_table', 4);
INSERT INTO `migrations` VALUES (9, '2019_02_28_034828_create_members_table', 4);
INSERT INTO `migrations` VALUES (10, '2019_02_28_034828_create_type1s_table', 4);
INSERT INTO `migrations` VALUES (11, '2019_02_28_034828_create_type2s_table', 4);
INSERT INTO `migrations` VALUES (12, '2019_02_28_034831_add_foreign_keys_to_documents_table', 4);
INSERT INTO `migrations` VALUES (13, '2019_02_28_034828_create_model_dictionary_table', 5);
INSERT INTO `migrations` VALUES (14, '2019_02_28_034831_add_foreign_keys_to_inventories_table', 5);
INSERT INTO `migrations` VALUES (15, '2019_02_28_034944_create_document_has_files_table', 5);
INSERT INTO `migrations` VALUES (16, '2019_02_28_034944_create_document_has_inventories_table', 5);
INSERT INTO `migrations` VALUES (17, '2019_02_28_034946_add_foreign_keys_to_document_has_files_table', 5);
INSERT INTO `migrations` VALUES (18, '2019_02_28_034946_add_foreign_keys_to_document_has_inventories_table', 5);

-- ----------------------------
-- Table structure for model_dictionary
-- ----------------------------
DROP TABLE IF EXISTS `model_dictionary`;
CREATE TABLE `model_dictionary`  (
  `id` bigint(20) NOT NULL,
  `model_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `category` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for model_has_permissions
-- ----------------------------
DROP TABLE IF EXISTS `model_has_permissions`;
CREATE TABLE `model_has_permissions`  (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`permission_id`, `model_id`, `model_type`) USING BTREE,
  INDEX `model_has_permissions_model_id_model_type_index`(`model_id`, `model_type`) USING BTREE,
  CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for model_has_roles
-- ----------------------------
DROP TABLE IF EXISTS `model_has_roles`;
CREATE TABLE `model_has_roles`  (
  `role_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`role_id`, `model_id`, `model_type`) USING BTREE,
  INDEX `model_has_roles_model_id_model_type_index`(`model_id`, `model_type`) USING BTREE,
  CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of model_has_roles
-- ----------------------------
INSERT INTO `model_has_roles` VALUES (1, 'App\\User', 1);
INSERT INTO `model_has_roles` VALUES (1, 'App\\User', 2);

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for permissions
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `guard_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of permissions
-- ----------------------------
INSERT INTO `permissions` VALUES (1, 'role-list', 'web', '2019-02-26 09:17:41', '2019-02-26 09:17:41');
INSERT INTO `permissions` VALUES (2, 'role-create', 'web', '2019-02-26 09:17:41', '2019-02-26 09:17:41');
INSERT INTO `permissions` VALUES (3, 'role-edit', 'web', '2019-02-26 09:17:41', '2019-02-26 09:17:41');
INSERT INTO `permissions` VALUES (4, 'role-delete', 'web', '2019-02-26 09:17:41', '2019-02-26 09:17:41');
INSERT INTO `permissions` VALUES (5, 'product-list', 'web', '2019-02-26 09:17:41', '2019-02-26 09:17:41');
INSERT INTO `permissions` VALUES (6, 'product-create', 'web', '2019-02-26 09:17:41', '2019-02-26 09:17:41');
INSERT INTO `permissions` VALUES (7, 'product-edit', 'web', '2019-02-26 09:17:41', '2019-02-26 09:17:41');
INSERT INTO `permissions` VALUES (8, 'product-delete', 'web', '2019-02-26 09:17:41', '2019-02-26 09:17:41');

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES (1, 'Oto', 'Car abc', '2019-02-26 10:29:28', '2019-02-26 10:29:28');

-- ----------------------------
-- Table structure for role_has_permissions
-- ----------------------------
DROP TABLE IF EXISTS `role_has_permissions`;
CREATE TABLE `role_has_permissions`  (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`permission_id`, `role_id`) USING BTREE,
  INDEX `role_has_permissions_role_id_foreign`(`role_id`) USING BTREE,
  CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of role_has_permissions
-- ----------------------------
INSERT INTO `role_has_permissions` VALUES (1, 1);
INSERT INTO `role_has_permissions` VALUES (2, 1);
INSERT INTO `role_has_permissions` VALUES (3, 1);
INSERT INTO `role_has_permissions` VALUES (4, 1);
INSERT INTO `role_has_permissions` VALUES (5, 1);
INSERT INTO `role_has_permissions` VALUES (6, 1);
INSERT INTO `role_has_permissions` VALUES (7, 1);
INSERT INTO `role_has_permissions` VALUES (8, 1);

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `guard_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES (1, 'Role1', 'web', '2019-02-26 10:29:00', '2019-02-26 10:29:00');

-- ----------------------------
-- Table structure for type1s
-- ----------------------------
DROP TABLE IF EXISTS `type1s`;
CREATE TABLE `type1s`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for type2s
-- ----------------------------
DROP TABLE IF EXISTS `type2s`;
CREATE TABLE `type2s`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `type1_id` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email_verified_at` timestamp(0) NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'Nguyen Vu', 'admin@admin.com', NULL, '$2y$10$8712OFnAZD1U8Sd3JI1/qeihKJD2mY0eh9ct1YICbWVPiEyJZXpkm', NULL, '2019-02-26 08:53:32', '2019-02-26 10:31:00');
INSERT INTO `users` VALUES (2, 'Admin2', 'admin2@admin.com', NULL, '$2y$10$0/ch.tZ0Sj.nTuPnJ6Mlk.4wbKXDJ1vUCd1mGdLnX7zHDlo2KqWJG', NULL, '2019-02-26 10:30:43', '2019-02-26 10:30:43');

SET FOREIGN_KEY_CHECKS = 1;
