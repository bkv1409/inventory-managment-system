const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

let prod = mix.inProduction();
prod = false;
if (prod) {
    console.log('--------------production-----------')
    // mix.js('resources/assets/js/app.js', 'public/js').version();
    // mix.sass('resources/assets/sass/app.scss', 'public/css').version();
    mix.version(['public/js/app.js', 'public/css/app.css']);

} else {
    console.log('----------dev build-------------')
    mix.js('resources/js/app.js', 'public/js')
        .sass('resources/sass/app.scss', 'public/css');
}

