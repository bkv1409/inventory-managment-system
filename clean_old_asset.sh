#!/bin/bash
rm -rf public/js/app*.js
rm -rf public/js/vendor*.js
rm -rf public/js/chunks/*.js
rm -rf public/js/lang/*.js
rm -rf public/css/app.css
rm -rf public/css/vendor.css
rm -rf public/js/admin/*.js
