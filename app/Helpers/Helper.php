<?php
/**
 * Created by PhpStorm.
 * User: vunq
 * Date: 2019-03-06
 * Time: 4:27 PM
 */

namespace App\Helpers;


use Excel;

class Helper
{
    public static function homePageURL()
    {
        return url('/');
    }

    public static function getNewFilenameTimestamp($prefix = 'ims', $isTimestamp = true) {
        return $prefix. '_' . ($isTimestamp ? time() : date('Ymd_His_') . gettimeofday()['usec']);

    }

    public static function maxDate()
    {
        return config('constant.MAX_RETURN_DATE');
    }

    public static function getNewDocumentNo($type) {
        $seq = rand(1,99);
        return date('Ymd-His-') . ($seq < 10 ? '0'.$seq : $seq) .$type;
    }

    public static function flatArrayConfig($configs){
        foreach ($configs as $key => &$value) {
            $value = $value['value'];
        }
        return $configs;
    }

    public static function complexArrayConfig($configs) {
        foreach ($configs as $key => &$config) {
            $val = $config;
            $config['key'] = $key;
            $config['value'] = $val;
        }
        return $configs;
    }

    /**
     * @param $fileName
     * @param $modelsExcel
     * @param bool $isTimestamp
     */
    public static function exportExcel($fileName, $modelsExcel, $isTimestamp = false){
        Excel::create(Helper::getNewFilenameTimestamp($fileName,$isTimestamp), function($excel) use ($modelsExcel) {
            $excel->sheet('Sheetname', function($sheet) use ($modelsExcel) {
                $sheet->fromArray($modelsExcel);
            });
        })->download('xlsx');
    }

    /**
     * @param $fileName
     * @param $models
     * @param $fields
     * @param bool $isTimestamp
     */
    public static function exportExcelWithFilter($fileName, $models, $fields, $isTimestamp = false){
        $modelsExcel = array();
        foreach ($models as $model){
            $element = array();
            foreach ($fields as $field){
                $element[$field] = $model[$field];
            }
            array_push($modelsExcel, $element);
        }
        return Helper::exportExcel($fileName, $modelsExcel, $isTimestamp);
    }
}
