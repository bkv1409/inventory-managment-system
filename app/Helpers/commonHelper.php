<?php
/**
 * Created by PhpStorm.
 * User: conguyen
 * Date: 2019-03-03
 * Time: 21:24
 */

if (! function_exists('get_value_by_key')) {
    function get_value_by_key($key,$arr)
    {
        if(!$key){
            return '';
        }else{
            return $arr[$key];
        }
    }
}