<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type1 extends Model
{
    const TYPE1_DEVICE = 'Device';
    const TYPE1_TOOLS = 'Tools';
    const TYPE1_MATERIAL = 'Material';
    const TYPE1_PARTS = 'Parts';
    const TYPE1_MATERIAL_ID = 3;
    const TYPE1_PARTS_ID = 4;
    const TYPE1_DEVICE_ID = 1;

    const TYPE_NAME = [
        "DEVICE"=>1,
        "TOOL"=>2,
        "MATERIAL"=>3
    ];
}
