<?php

namespace App\Http\Controllers;

use App;
use App\Document;
use App\Inventory;
use App\Repositories\MemberRespository;
use App\Repositories\ModelDictionaryRepository;
use App\Repositories\Type1sRepository;
use App\Repositories\Type2SRepository;
use App\Repositories\UnitRespository;
use App\Services\DocumentService;
use DB;
use Helper;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;

class DocumentController extends Controller
{
    /**
     * DocumentController constructor.
     * DI Repository and Service
     * Append permission middleware foreach function
     * @param Type1sRepository $type1sRepository
     * @param Type2SRepository $type2sRepository
     * @param UnitRespository $unitRespository
     * @param MemberRespository $memberRespository
     * @param ModelDictionaryRepository $modelDictionaryRepository
     * @param DocumentService $documentService
     */
    public function __construct(
        Type1sRepository $type1sRepository,
        Type2sRepository $type2sRepository,
        UnitRespository $unitRespository,
        MemberRespository $memberRespository,
        ModelDictionaryRepository $modelDictionaryRepository,
        DocumentService $documentService
    )
    {
        $this->type2sRepository = $type2sRepository;
        $this->type1sRepository = $type1sRepository;
        $this->modelDictionaryRepository = $modelDictionaryRepository;
        $this->memberRespository = $memberRespository;
        $this->unitRespository = $unitRespository;
        $this->documentService = $documentService;

        $this->middleware('permission:document-list');
        $this->middleware('permission:document-create', ['only' => ['create','store']]);
        $this->middleware('permission:document-show', ['only' => ['preview','previewPart','printPdf','downloadPdf']]);
        $this->middleware('permission:document-delete', ['only' => ['destroy']]);
        $this->middleware('permission:document-edit', ['only' => ['editFiles','updateFiles']]);
        $this->middleware('permission:document-approve', ['only' => ['approve','reject']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $rowNum = config('constant.ROW_NUM_IN_PAGE');
        $query = $this->documentService->processWithParams($request);

//        dd($query->toSql(),$query->getBindings());
        $documents = $query->latest()->paginate($rowNum);

        $inputs = Input::query();
        $documentTypes = Helper::flatArrayConfig(config('constant.DOCUMENT_TYPE'));
        $documentStatus = config('constant.DOCUMENT_STATUS');
        $documentTypes[''] = 'Tất cả';
        return view('documents.index',compact('documents','inputs','documentTypes','documentStatus'))
            ->with('i', (request()->input('page', 1) - 1) * $rowNum);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $listType1s = $this->type1sRepository->list();
        $listType2s = $this->type2sRepository->list();
        $listModelDictionary = $this->modelDictionaryRepository->list();
        $listUnit = $this->unitRespository->list();
        $listMember = $this->memberRespository->list();
        $listStatus = Inventory::STATUS;
        //Loại document: Nhập: IN, Xuất: OUT, Hủy: CANCEL, Sửa: REPAIR, Cho mượn/Thu hồi: RENT
        $documentTypes = config('constant.DOCUMENT_TYPE');
//        dd($documentTypes,json_encode($documentTypes));
        return view('documents.create',
            compact('documentTypes',
                'listType1s',
                'listType2s',
                'listModelDictionary',
                'listUnit',
                'listMember',
                'listStatus'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function store(Request $request)
    {
        request()->validate([
            'type' => 'required',
            'deliver' => 'required',
            'inventories' => 'required',
            'return_date' => 'required',
        ]);
//        return response()->json(['data'=>$request->dataRepairInventories],Response::HTTP_EXPECTATION_FAILED);
        try {
            DB::beginTransaction();
            $this->documentService->storeProcess($request);
            DB::commit();
            return response()->json(['success'=>1,'message'=> 'Create Document Done']);
        } catch (\Throwable $exception) {
            DB::rollback();
            return response()->json(['success'=>0,'message'=> $exception->getMessage(),'errors'=>$exception],400);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function show(Document $document)
    {
        //Loại document: Nhập: IN, Xuất: OUT, Hủy: CANCEL, Sửa: REPAIR, Cho mượn/Thu hồi: RENT
        $documentTypes = config('constant.DOCUMENT_TYPE');
        $data = $this->documentService->buildInventoriesAndRepairs($document->inventories);
        $inventories = $data['inventories'];
        $repairInventories = $data['repairInventories'];
//        dd(compact('documentTypes','document','inventories','repairInventories'));
        return view('documents.show', compact('documentTypes','document','inventories','repairInventories'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function edit(Document $document)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Document $document)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function destroy(Document $document)
    {
        //
    }

    /**
     * @param Request $request
     * @param Document $document
     * @return \Illuminate\Http\RedirectResponse
     */
    public function approve(Request $request, Document $document){
        $APPROVED = config('constant.DOCUMENT_STATUS.APPROVED');
        $rowNum = config('constant.ROW_NUM_IN_PAGE');
        if ($document->status == $APPROVED) {
            return redirect()->route('documents.index')
                ->with('error','Document '.$document->id.' already been approved!')
                ->with('i', (request()->input('page', 1) - 1) * $rowNum);
        }
        // check over quantity first
        if ($document->inventories) {
            foreach ($document->inventories as $inventory) {
                if ($inventory->internal_status == Inventory::INTERNAL_STATUS['IN'] && $document->type == config('constant.DOCUMENT_TYPE.OUT.key')) {
                    if ($inventory->quantity - $inventory->pivot->delta_quantity < 0) {
                        return redirect()->route('documents.index')
                            ->with('error','Document '.$document->id.' cannot approved because other inventory has over current quantity!')
                            ->with('i', (request()->input('page', 1) - 1) * $rowNum);
                    }
                }
            }
        }

        // update inventories
        $this->documentService->approve($document, $request);

        return redirect()->route('documents.index')
            ->with('success','Document ID '.$document->id.' Document No '.$document->document_no.' approved!')
            ->with('i', (request()->input('page', 1) - 1) * $rowNum);
    }

    /**
     * @param Request $request
     * @param Document $document
     * @return \Illuminate\Http\RedirectResponse
     */
    public function reject(Request $request, Document $document){
        $REJECTED = config('constant.DOCUMENT_STATUS.REJECTED');
        $rowNum = (int) config('constant.ROW_NUM_IN_PAGE');
        if ($document->status == $REJECTED) {
            return redirect()->route('documents.index')
                ->with('error','Document '.$document->id.' already been rejected!')
                ->with('i', (request()->input('page', 1) - 1) * $rowNum);
        }

//        $request->validate([
//           'reject_reason' =>'required|min:20|max:150'
//        ]);

        $this->documentService->reject($document, $request);

        return redirect()->route('documents.index')
            ->with('success','Document ID '.$document->id.' Document No '.$document->document_no.' rejected!')
            ->with('i', (request()->input('page', 1) - 1) * $rowNum);
    }

    /**
     * @param Request $request
     * @param Document $document
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     */
    public function preview(Request $request, Document $document){
        if ($document->status != config('constant.DOCUMENT_STATUS.APPROVED'))
            return abort(Response::HTTP_NOT_ACCEPTABLE);
        $isWeb = true;
        $data = $this->documentService->buildInventoriesAndRepairs($document->inventories);
        $repairInventories = $data['repairInventories'];
        return view('documents.preview',compact('document','isWeb','repairInventories'));
    }

    /**
     * @param Request $request
     * @param Document $document
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     */
    public function previewPart(Request $request, Document $document){
        if ($document->status != config('constant.DOCUMENT_STATUS.APPROVED'))
            return abort(Response::HTTP_NOT_ACCEPTABLE);
        $isWeb = true;
        $data = $this->documentService->buildInventoriesAndRepairs($document->inventories);
        $repairInventories = $data['repairInventories'];
        return view('documents.preview-part',compact('document','isWeb','repairInventories'));
    }

    /**
     * @param Request $request
     * @param Document $document
     * @return Response|void
     */
    public function printPdf(Request $request, Document $document){
        if ($document->status != config('constant.DOCUMENT_STATUS.APPROVED'))
            return abort(Response::HTTP_NOT_ACCEPTABLE);
        $data = $this->documentService->buildInventoriesAndRepairs($document->inventories);
        $repairInventories = $data['repairInventories'];
        return $document->getPdf('stream', $repairInventories);
    }

    /**
     * @param Request $request
     * @param Document $document
     * @return Response|void
     */
    public function downloadPdf(Request $request, Document $document){
        if ($document->status != config('constant.DOCUMENT_STATUS.APPROVED'))
            return abort(Response::HTTP_NOT_ACCEPTABLE);
        $data = $this->documentService->buildInventoriesAndRepairs($document->inventories);
        $repairInventories = $data['repairInventories'];
        return $document->getPdf('download', $repairInventories);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function testPreviewPdf(Request $request){
        $document = Document::query()->latest()->first();
        return view('documents.preview',compact('document'));
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function testDownloadPdf(Request $request){
        ini_set('max_execution_time', 300); //300 seconds = 5 minutes

        $document = Document::query()->latest()->first();
        $pdf = App::make('dompdf.wrapper');

        $pdf = $pdf->loadView('documents.preview', compact('document'));

        return $pdf->download('invoice.pdf');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function exportExcel(Request $request){
        $rowNum = config('constant.ROW_NUM_IN_PAGE');
        $query = $this->documentService->processWithParams($request);

        $count = $query->count();
        if ($count > config('constant.MAX_EXCEL_EXPORT_ROW')) {
            return redirect()->route('documents.index')
                ->with('error','Export Excel failed because over maximum row ('.config('constant.MAX_EXCEL_EXPORT_ROW').' rows) ! Please export with some condition!')
                ->with('i', (request()->input('page', 1) - 1) * $rowNum);
        }
        $fields =['reason','note'
            ,'status','reject_reason','type','created_at'
            ,'return_date','approved_date','domain_account'];
        $documents = $query->get()->toArray();
        $documentsExcel = array();
        foreach ($documents as $document){
            $element = array();
            $element['id'] = $document['id'];
            $element['document_no'] = $document['document_no'];
            $element['deliver'] = $document['deliver']['name'];
            $element['receiver'] = $document['receiver']['name'];
            $element['approved_by'] = $document['approved_by']['name'] . ' (' .$document['approved_by']['email'] .')';
            foreach ($fields as $field){
                $element[$field] = $document[$field];
            }
            array_push($documentsExcel, $element);
        }

        return Helper::exportExcel('IMS_DocumentsExport', $documentsExcel);
    }

    /**
     * @param Request $request
     * @param Document $document
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editFiles(Request $request, Document $document){
        $files = $document->files;
        return view('documents.edit-files',compact('document','files'));
    }

    /**
     * @param Request $request
     * @param Document $document
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function updateFiles(Request $request, Document $document){
        request()->validate([
            'attachedFiles' => 'required',
        ]);
        try {
            DB::beginTransaction();

            $files = array();
            foreach ($request->attachedFiles as $f) {
                array_push($files,$f['id']);
            }
            $document->files()->attach($files);

            DB::commit();
            return response()->json(['success'=>1,'message'=> 'Update Document Files Done']);
        } catch (\Throwable $exception) {
            DB::rollback();
            return response()->json(['success'=>0,'message'=> $exception->getMessage(),'errors'=>$exception],400);
        }

    }
}
