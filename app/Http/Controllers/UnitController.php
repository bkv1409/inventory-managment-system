<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Unit;
use Log;

use App\Repositories\UnitRespository;
use App\Http\Requests\Unit\UpdateRequest;
use App\Http\Requests\Unit\StoreRequest;
class UnitController extends Controller
{

    public function __construct(UnitRespository $unitRespository)
    {

        $this->unitRespository = $unitRespository;
        $this->middleware('permission:config-list',['only' => ['index','show']]);
        $this->middleware('permission:config-create', ['only' => ['create','store']]);
        $this->middleware('permission:config-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:config-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $input = $request->query();
        $per_page = config('common.per_page');

        $listUnit = $this->unitRespository->getAll($input);

        return view('unit.index', array(
                'list_unit' => $listUnit->paginate($per_page),
                'i'=>($request->input('page', 1) - 1) * $per_page,
                'input'=>$input
            )
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('unit.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        try {
            $input = $request->all();
            $validated = $request->validated();
            $member_save = $this->unitRespository->store($input);
            if ($member_save) {
                return redirect()->route('unit.index')->with('unit_save_success', 'Save successfully');
            }
        } catch (\Exception $e) {
            Log::error('UnitController@store', [$e]);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        try {
            $unit = $this->unitRespository->findOne($id);
            return view('unit.show', array(
                'row' => $unit));
        } catch (\Exception $e) {
            Log::error('UnitController@show', [$e]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        try {

            $unit = $this->unitRespository->findOne($id);

            return view('unit.edit', array(
                'row' => $unit
            ));
        } catch (\Exception $e) {
            Log::error('UnitController@edit', [$e]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

      public function update(UpdateRequest $request, $id)
      {
          try {
              $input = $request->all();


              $validated = $request->validated();

              $this->unitRespository->update($id, $input);
              return redirect()->route('unit.index')->with('unit_save_success', 'Save successfully');

          } catch (\Exception $e) {
              Log::error('UnitController@update', [$e]);
          }
      }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->unitRespository->softDelete($id);
            return redirect()->route('unit.index')->with('unit_delete_success', 'Delete successfully');
        } catch (\Exception $e) {
            Log::error('\'UnitController@destroy', [$e]);
            return redirect()->route('unit.index')->with('unit_delete_error', 'System error');
        }
    }
}
