<?php

namespace App\Http\Controllers;

use Excel;
use Helper;
use Illuminate\Http\Request;
use App\Member;
use Log;
use DB;
use App\Repositories\MemberRespository;
use App\Http\Requests\Member\UpdateRequest;
use App\Http\Requests\Member\StoreRequest;
use Illuminate\Support\Facades\Storage;


class MemberController extends Controller
{

    public function __construct(MemberRespository $memberRespository)
    {

        $this->memberRespository = $memberRespository;
        $this->middleware('permission:config-list', ['only' => ['index', 'show', 'ajaxSearch']]);
        $this->middleware('permission:config-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:config-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:config-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $input = $request->query();
        $per_page = config('common.per_page');

        if ($request->btnsmt == 'Export') {
            $listInventory = $this->memberRespository->getAll($input)->get();
            $this->exportListMember($listInventory);
        }

        $listMember = $this->memberRespository->getAll($input);

        return view('member.index', array(
                'list_member' => $listMember->paginate($per_page),
                'is_member' => Member::IS_MEMBER,
                'i' => ($request->input('page', 1) - 1) * $per_page,
                'input' => $input
            )
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('member.create', array(
            'is_member' => Member::IS_MEMBER,
            'status_member' => Member::STATUS_ARRAY
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        try {
            $input = $request->all();
            $validated = $request->validated();
            $member_save = $this->memberRespository->store($input);
            if ($member_save) {
                return redirect()->route('member.index')->with('member_save_success', 'Save successfully');
            }
        } catch (\Exception $e) {
            Log::error('MemberController@store', [$e]);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        try {
            $member = $this->memberRespository->findOne($id);
            return view('member.show', array(
                'row' => $member,
                'status_member' => Member::STATUS_ARRAY,
                'is_member' => Member::IS_MEMBER));
        } catch (\Exception $e) {
            Log::error('MemberController@show', [$e]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        try {

            $member = $this->memberRespository->findOne($id);

            return view('member.edit', array(
                'row' => $member,
                'status_member' => Member::STATUS_ARRAY,
                'is_member' => Member::IS_MEMBER
            ));
        } catch (\Exception $e) {
            Log::error('MemberController@edit', [$e]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        try {
            $input = $request->all();


            $validated = $request->validated();

            $this->memberRespository->update($id, $input);
            return redirect()->route('member.index')->with('member_save_success', 'Save successfully');

        } catch (\Exception $e) {
            Log::error('MemberController@update', [$e]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        try {
            $this->memberRespository->softDelete($id);
            return redirect()->route('member.index')->with('member_delete_success', 'Delete successfully');
        } catch (\Exception $e) {
            Log::error('MemberController@destroy', [$e]);
            return redirect()->route('member.index')->with('member_delete_error', 'System error');
        }
    }

    public function ajaxSearch(Request $request)
    {
        $data = [];
        if ($request->has('q')) {
            $search = $request->q;
            $data = Member::query()
                ->where('name', 'LIKE', "%$search%")
                ->get();
        }

        return response()->json($data);
    }

    public function exportListMember($listMember)
    {

        return \Excel::create('list_member', function ($excel) use ($listMember) {

            $excel->sheet('sheet name', function ($sheet) use ($listMember) {
                $sheet->cell('A1', function ($cell) {
                    $cell->setValue('Member Name');
                });
                $sheet->cell('B1', function ($cell) {
                    $cell->setValue('LG ID');
                });
                $sheet->cell('C1', function ($cell) {
                    $cell->setValue('Email');
                });
                $sheet->cell('D1', function ($cell) {
                    $cell->setValue('Phone Number');
                });
                $sheet->cell('E1', function ($cell) {
                    $cell->setValue('Is Lg Member');
                });
                $sheet->cell('F1', function ($cell) {
                    $cell->setValue('Department');
                });
                $sheet->cell('G1', function ($cell) {
                    $cell->setValue('Company');
                });
                $sheet->cell('H1', function ($cell) {
                    $cell->setValue('Status');
                });
                $sheet->cell('I1', function ($cell) {
                    $cell->setValue('Domain Account');
                });

                $sheet->cell('J1', function ($cell) {
                    $cell->setValue('Create At');
                });

                $sheet->cell('K1', function ($cell) {
                    $cell->setValue('Update At');
                });

                foreach ($listMember as $key => $value) {
                    $i = $key + 2;
                    $sheet->cell('A' . $i, $value['name']);
                    $sheet->cell('B' . $i, $value['lg_id']);
                    $sheet->cell('C' . $i, $value['email']);
                    $sheet->cell('D' . $i, $value['phone_no']);
                    $sheet->cell('E' . $i, ($value['is_lg_member'] == 1) ? "lg_member" : "no_lg_member");
                    $sheet->cell('F' . $i, $value['department']);
                    $sheet->cell('G' . $i, $value['company']);
                    $sheet->cell('H' . $i, $value['status']);
                    $sheet->cell('I' . $i, $value['domain_account']);
                    $sheet->cell('J' . $i, $value['created_at']);
                    $sheet->cell('K' . $i, $value['updated_at']);

                }

            });

        })->download('xlsx');

    }

    public function exportExcel(Request $request)
    {
        $listMember = $this->memberRespository->getAll($request->query());
        $count = $listMember->count();
        if ($count > config('constant.MAX_EXCEL_EXPORT_ROW')) {
            return redirect()->route('member.index')
                ->with('error', 'Export Excel failed because over maximum row (' . config('constant.MAX_EXCEL_EXPORT_ROW') . ' rows) ! Please export with some condition!')
                ->with('i', (request()->input('page', 1) - 1) * 5);
        }
        // field show in excel
        $fields = ['id', 'name', 'lg_id', 'email', 'phone_no', 'department', 'company', 'created_at', 'domain_account'];
        $models = $listMember->get()->toArray();

        return Helper::exportExcelWithFilter('IMS_MemberExport', $models, $fields);
    }

    public function import(Request $request)
    {
        return view('member.import');
    }

    public function handleExcel(Request $request)
    {
        ini_set('memory_limit', -1);


        $destination_path = storage_path('uploads');

        $_arr_excel_ext = array('xls', 'xlsx');

        if (!$request->file('importExcel')) {
            return redirect()->route('member.import')
                ->with('file_invalid', 'There is no file attached on the form!');
        }

        $file = $request->file('importExcel');
        if (!in_array($file->getClientOriginalExtension(), $_arr_excel_ext)) {
            return redirect()->route('member.import')
                ->with('file_format_invalid', 'Please use .xls or .xlsx extension!');
        }

        if (!$file->move($destination_path, $file->getClientOriginalName())) {
            return redirect()->route('member.import')
                ->with('loading_error', 'Sorry, error on loading excel!');
        }
        $pathUpload = $destination_path . "/" . $file->getClientOriginalName();


        $data = Excel::load($pathUpload)->get();

        $allData = array();
        if ($data->count()) {
            $dataRow = array();
            foreach ($data as $key => $value) {
                $dataRow['lg_id'] = trim($value["employee"]);
                $dataRow['status'] = trim($value["status"]);
                $dataRow['employee'] = trim($value["employee"]);
                $dataRow['lastname'] = trim($value["lastname"]);
                $dataRow['middle'] = trim($value["middle"]);
                $dataRow['firstname'] = trim($value["firstname"]);
                $dataRow['email'] = trim($value["email_address"]);
                $dataRow['email_address'] = trim($value["email_address"]);
                $dataRow['phone_no'] = trim($value["phone_number"]);
                $dataRow['phone_number'] = trim($value["phone_number"]);
                $allData[] = $dataRow;
            }
        }

        $email_arr = array();
        $lg_id_arr = array();

        foreach ($allData as $key => $value) {

            if (!$value["email"]) {
                $value["status_cp"] = "Email is required";
                $allData[$key] = $value;
                continue;
            }

            if (in_array($value["email"], $email_arr)) {
                $value["status_cp"] = "Email existed in file import";
                $allData[$key] = $value;
                continue;
            } else {
                array_push($email_arr, $value["email"]);
            }

            $dataFindEmail = $this->memberRespository->findByEmail($value["email"]);
            if ($dataFindEmail) {
                $value["status_cp"] = "Email exist.";
                $allData[$key] = $value;
                continue;
            }

            if (!$value["lg_id"]) {
                $value["status_cp"] = "LG ID is required";
                $allData[$key] = $value;
                continue;
            }
            if (in_array($value["lg_id"], $lg_id_arr)) {
                $value["status_cp"] = "LG ID in file import";
                $allData[$key] = $value;
                continue;
            } else {
                array_push($lg_id_arr, $value["lg_id"]);
            }

            $dataFindLgId = $this->memberRespository->findByLgId($value["lg_id"]);
            if ($dataFindLgId) {
                $value["status_cp"] = "LG ID exist.";
                $allData[$key] = $value;
                continue;
            }


            if (!$value["lastname"]) {
                $value["status_cp"] = "Lastname is required";
                $allData[$key] = $value;
                continue;
            }

            if (!$value["middle"]) {
                $value["status_cp"] = "Middle is required";
                $allData[$key] = $value;
                continue;
            }

            if (!$value["firstname"]) {
                $value["status_cp"] = "Firstname is required";
                $allData[$key] = $value;
                continue;
            }
            $value["name"] = $value["firstname"].' '.$value["middle"].' '.$value["lastname"];
            $value["status_cp"] = "OK";

            $allData[$key] = $value;

        }
        unlink($pathUpload);
        return view('member.import', array('data' => $allData, "showbtn" => true));

    }

    public function saveDataExcel(Request $request)
    {

        try {
            DB::beginTransaction();
            $inputData = $request->input('list_member');

            Log::info($inputData);
            $dataConvert = json_decode($inputData,true) ;
            $dataInsert = array();
            $dataSuccess = array();
            $dataError = array();
            $totalRecord = count($inputData);
            if ($totalRecord > 0) {
                foreach ($dataConvert as $key => $value) {
                    if ($value["status_cp"] =="OK") {
                        $dataInsert[] = [
                            'lg_id' => $value["lg_id"],
                            'status' => $value["status"],
                            'name' => $value["name"],
                            'email' => $value["email"],
                            'phone_no' => $value["phone_no"],
                            'domain_account'=>explode("@", $value["email"])[0],
                            'company' => Member::COMPANY_DEFAULT
                        ];
                        $value["status_cp"] = "OK";
                        $dataSuccess[] = $value;
                    } else {
                        $dataError[] = [
                            'lg_id' => $value["lg_id"],
                            'status' => $value["status"],
                            'email' => $value["email"],
                            'lastname' => $value["lastname"],
                            'middle' => $value["middle"],
                            'firstname' => $value["firstname"],
                            'employee' => $value["employee"],
                            'phone_no' => $value["phone_no"],
                            'email_address' => $value["email_address"],
                            'phone_number' => $value["phone_number"],
                            'status_cp' => $value["status_cp"]
                        ];
                    }
                }
                Member::insert($dataInsert);

            }
            DB::commit();

            if ($inputData == null || $totalRecord <= 0) {
                $res = "Done";
            } else {
                $totalError = count($dataError);
                $totalSave = count($dataInsert);
                $arrRes = array_merge($dataError, $dataSuccess);
                $res = array("arrRes" => $arrRes, "totalError" => $totalError, "totalSave" => $totalSave);

            }

            echo json_encode($res);
        } catch (\Exception $e) {
            Log::error('MemberController@saveDataExcel', [$e]);
            DB::rollback();

            foreach ($dataSuccess as $key => $value) {
                $value["status_cp"] = "Insert failed";
                $dataSuccess[$key] = $value;
            }

            $arrRes = array_merge($dataError, $dataSuccess);
            $res = array("arrRes" => $arrRes);

            echo json_encode($res);
            Log::error('MemberController@saveDataExcel', [$e]);
        }
    }



    public function exportFile(Request $request)
    {
        $input = $request->input('list_member_error');
        $list_member_error = json_decode($input,true);
        $myFile = Excel::create('List_Error_Record', function ($excel) use ($list_member_error) {
            $excel->sheet('mySheet', function ($sheet) use ($list_member_error) {
                $sheet->cell('A1', function ($cell) {
                    $cell->setValue('EMPLOYEE');
                });
                $sheet->cell('B1', function ($cell) {
                    $cell->setValue('STATUS');
                });
                $sheet->cell('C1', function ($cell) {
                    $cell->setValue('LASTNAME');
                });
                $sheet->cell('D1', function ($cell) {
                    $cell->setValue('MIDDLE');
                });
                $sheet->cell('E1', function ($cell) {
                    $cell->setValue('FIRSTNAME');
                });
                $sheet->cell('F1', function ($cell) {
                    $cell->setValue('EMAIL_ADDRESS');
                });
                $sheet->cell('G1', function ($cell) {
                    $cell->setValue('PHONE_NUMBER');
                });
                $sheet->cell('H1', function ($cell) {
                    $cell->setValue('MESSAGE');
                });

                foreach ($list_member_error as $key => $value) {
                    $i = $key + 2;
                    $sheet->cell('A' . $i, $value['lg_id']);
                    $sheet->cell('B' . $i, $value['status']);
                    $sheet->cell('C' . $i, $value['lastname']);
                    $sheet->cell('D' . $i, $value['middle']);
                    $sheet->cell('E' . $i, $value['firstname']);
                    $sheet->cell('F' . $i, $value['email_address']);
                    $sheet->cell('G' . $i, $value['phone_number']);
                    $sheet->cell('H' . $i, $value['status_cp']);

                }

            });
        });
        $myFile = $myFile->string('xlsx');
        $response = array(
            'name' => "List_Error_Record",
            'file' => "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," . base64_encode($myFile)
        );
        return response()->json($response);

    }



}
