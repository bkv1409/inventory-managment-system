<?php
/**
 * Created by PhpStorm.
 * User: conguyen
 * Date: 2019-03-02
 * Time: 20:01
 */

namespace App\Http\Controllers;

use App\Type1;
use Illuminate\Http\Request;
use App\Repositories\InventoryRepository;
use App\Repositories\Type1sRepository;
use App\Repositories\Type2sRepository;
use App\Repositories\ModelDictionaryRepository;
use App\Repositories\UnitRespository;
use App\Repositories\MemberRespository;
use App\Inventory;
use Illuminate\Http\Response;
use Log;
use App\Http\Requests\Inventory\StoreRequest;
use App\Http\Requests\Inventory\UpdateRequest;
use Excel;
use DB;
use App\Jobs\SendEmailJob;
use App\Helpers\Helper;
use App\Member;


class InventoryController extends Controller
{

    public function __construct(
        Excel $excel,
        InventoryRepository $inventoryRepository,
        Type1sRepository $type1sRepository,
        Type2sRepository $type2sRepository,
        UnitRespository $unitRespository,
        MemberRespository $memberRespository,
        ModelDictionaryRepository $modelDictionaryRepository)
    {
        $this->inventoryRepository = $inventoryRepository;
        $this->type2sRepository = $type2sRepository;
        $this->type1sRepository = $type1sRepository;
        $this->modelDictionaryRepository = $modelDictionaryRepository;
        $this->memberRespository = $memberRespository;
        $this->unitRespository = $unitRespository;
        $this->excel = $excel;
        $this->dataExel = [];
        $this->inventory = [];

        $this->middleware('permission:inventory-list');
        $this->middleware('permission:inventory-create', ['only' => ['create','store']]);
        $this->middleware('permission:inventory-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:inventory-show', ['only' => ['show','ajaxSearch']]);
        $this->middleware('permission:inventory-import', ['only' => ['handleExcel','import','saveDataExcel','exportFile']]);
        $this->middleware('permission:inventory-delete', ['only' => ['destroy']]);
    }

    public function index(Request $request)
    {
        $input = $request->query();
        if ($request->btnsmt == 'Export') {
            if (!$input["type1_id"] && !$input["status"] && !$input["serial_number"]) {
                return redirect()->route('inventory.index')->with('invalid_param', 'Search Parameter is required');
            }
            $listInventory = $this->inventoryRepository->list($input)->get();
            $this->exportListIventory($listInventory);
        }
        $modelName = array();
        $type2s = array();
        $listType1s = $this->type1sRepository->list();
        $listInventory = $this->inventoryRepository->list($input);

        $per_page = config('common.per_page');

        return view('inventory.index', array(
                'list_inventory' => $listInventory->paginate($per_page),
                'list_type1s' => $listType1s,
                'type2s' => $type2s,
                'model_name' => $modelName,
                'inventory_status' => Inventory::STATUS,
                'internal_status' => Inventory::INTERNAL_STATUS,
                'is_member_arr'=>Member::IS_MEMBER,
                'i' => ($request->input('page', 1) - 1) * $per_page,
                'input' => $input
            )
        );
    }


    public function create()
    {
        $listType1s = $this->type1sRepository->list();

        $listUnit = $this->unitRespository->list();


        return view('inventory.create', array(
            'list_type1s' => $listType1s,
            'inventory_status' => Inventory::STATUS,
            'internal_status' => Inventory::INTERNAL_STATUS,
            'list_unit' => $listUnit
        ));
    }

    public function store(StoreRequest $request)
    {
        try {

            $input = $request->all();
            $validated = $request->validated();
            if ($input["type1_id"] === Type1::TYPE_NAME["MATERIAL"]) {
                $input["it_tag"] = "Material_" . round(microtime(true) * 1000);
            }
            $invetory_save = $this->inventoryRepository->store($input);
            if ($invetory_save) {
                if ($request->isXmlHttpRequest()) {
                    $invetory_save->type1_name = $invetory_save->type1->name;
                    $invetory_save->type2_name = $invetory_save->type2->name;
                    $invetory_save->model_name = $invetory_save->model->model_name;
                    $invetory_save->unit_name = $invetory_save->unitData->name;
                    return response()->json(['error'=>0,'message'=>'Save successfully','errors'=>[],'inventory' => $invetory_save]);
                }
                $new_inventory = $this->inventoryRepository->findOne($invetory_save->id);
                // send email
                try {
                    $mailData = [
                        "TYPE" => Inventory::ACTION["CREATE"],
                        "user_name" => \Auth::user()->name,
                        "new_data" => $new_inventory
                    ];
                    dispatch(new SendEmailJob($mailData));
                } catch (\Throwable $exception) {
                    Log::error('InventoryController@store SendEmailJob ', [$exception]);
                }

                return redirect()->route('inventory.index')->with('inventory_save_success', 'Save successfully');
            }
        } catch (\Exception $e) {
            if ($request->isXmlHttpRequest()) {
                return response()->json(['error'=>1,'message'=>$e->getMessage(),'errors'=>$e->getTrace(),'inventory' => null],Response::HTTP_BAD_REQUEST);
            }
            return redirect()->route('inventory.index')->with('inventory_save_error', 'System error');
            Log::error('InventoryController@store', [$e]);

        }
    }

    public function destroy(Request $request, $id)
    {
        try {
            $detele_data = $this->inventoryRepository->softDeleteInventory($id);
            if ($detele_data) {
                $delete_inventory = $this->inventoryRepository->findWithTrash($id);

                try {
                    $mailData = [
                        "TYPE" => Inventory::ACTION["DELETE"],
                        "user_name" => \Auth::user()->name,
                        "new_data" => $delete_inventory
                    ];
                    dispatch(new SendEmailJob($mailData));
                } catch (\Throwable $exception) {
                    Log::error('InventoryController@store SendEmailJob ', [$exception]);
                }
                return redirect()->route('inventory.index')->with('inventory_delete_success', 'Delete successfully');
            }

        } catch (\Exception $e) {
            Log::error('InventoryController@destroy', [$e]);
            return redirect()->route('inventory.index')->with('inventory_delete_error', 'System error');
        }
    }

    public function edit(Request $request, $id)
    {
        try {
            $listType1s = $this->type1sRepository->list();

            $listUnit = $this->unitRespository->list();

            $inventory = $this->inventoryRepository->findOne($id);
            $modelName = $this->modelDictionaryRepository->findOne($inventory->model_id);
            $memberName = $this->memberRespository->findOne($inventory->current_user_id);

            $type2s = $this->type2sRepository->findOne($inventory->type2_id);
            return view('inventory.edit', array(
                'row' => $inventory,
                'list_type1s' => $listType1s,
                'inventory_status' => Inventory::STATUS,
                'type2s_name' => $type2s->toArray(),
                'model_name' => $modelName->toArray(),
                'member_name' => $memberName->toArray(),
                'list_unit' => $listUnit
            ));
        } catch (\Exception $e) {
            Log::error('InventoryController@destroy', [$e]);
        }
    }

    public function show(Request $request, $id)
    {
        try {
            $inventory = $this->inventoryRepository->findOne($id);
            return view('inventory.show', array(
                'row' => $inventory,
                'is_member_arr'=>Member::IS_MEMBER,
                'inventory_status' => Inventory::STATUS,
                'internal_status' => Inventory::INTERNAL_STATUS));
        } catch (\Exception $e) {
            Log::error('InventoryController@show', [$e]);
        }
    }

    public function update(UpdateRequest $request, $id)
    {
        try {

            $input = $request->all();
            $input_hiden = json_decode($input["inventory_hidden"], true);
            if ($input["type1_id"] === Type1::TYPE_NAME["MATERIAL"]) {
                $input["it_tag"] = "Meterial_" . round(microtime(true) * 1000);
            }
            $validated = $request->validated();
            $data_update = $this->inventoryRepository->update($id, $input);
            if ($data_update == 1) {
                $new_inventory = $this->inventoryRepository->findOne($id);
                try {
                    $mailData = [
                        "TYPE" => Inventory::ACTION["EDIT"],
                        "user_name" => \Auth::user()->name,
                        "old_data" => $input_hiden,
                        "new_data" => $new_inventory
                    ];
                    dispatch(new SendEmailJob($mailData));
                } catch (\Throwable $exception) {
                    Log::error('InventoryController@store SendEmailJob ', [$exception]);
                }

            }

            return redirect()->route('inventory.index')->with('inventory_save_success', 'Save successfully');

        } catch (\Exception $e) {
            Log::error('InventoryController@update', [$e]);
        }
    }

    public function import(Request $request)
    {
        return view('inventory.import');
    }

    function check_type2s($type2s, $name, $type1_id)
    {

        $record = array();
        foreach ($type2s as $key => $type2) {
            if ($type2['name'] === $name && $type2['type1_id'] === $type1_id) {
                $record = $type2;
            }
        }
        return $record;
    }


    public function handleExcel(Request $request)
    {
        ini_set('memory_limit', -1);

        $listType1s = $this->type1sRepository->list();
        $listType2s = $this->type2sRepository->list();
        $listModelDictionary = $this->modelDictionaryRepository->list();
        $listUnit = $this->unitRespository->list();

        $destination_path = storage_path('uploads');

        $_arr_excel_ext = array('xls', 'xlsx');

        if (!$request->file('importExcel')) {
            return redirect()->route('inventory.import')
                ->with('file_invalid', 'There is no file attached on the form!');
        }

        $file = $request->file('importExcel');
        if (!in_array($file->getClientOriginalExtension(), $_arr_excel_ext)) {
            return redirect()->route('inventory.import')
                ->with('file_format_invalid', 'Please use .xls or .xlsx extension!');
        }

        if (!$file->move($destination_path, $file->getClientOriginalName())) {
            return redirect()->route('inventory.import')
                ->with('loading_error', 'Sorry, error on loading excel!');
        }
        $pathUpload = $destination_path . "/" . $file->getClientOriginalName();
        $data = Excel::load($pathUpload)->get();


        $allData = array();
        if ($data->count()) {
            $dataRow = array();
            foreach ($data as $key => $value) {
                $dataRow['serial_number'] = trim($value["serial_number"]);
                $dataRow['type1'] = trim($value["type1"]);
                $dataRow['type2'] = trim($value["type2"]);
                $dataRow['model_name'] = trim($value["model_name"]);
                $dataRow['status_inventory'] = trim($value["status"]);
                $dataRow['fa_no'] = trim($value["fa_no"]);
                $dataRow['it_tag'] = trim($value["it_tag"]);
                $dataRow['quantity'] = trim($value["quantity"]) ? trim($value["quantity"]) : 1;
                $dataRow['unit'] = trim($value["unit"]);
                $dataRow['is_lg'] = trim($value["is_lg"]);
                $dataRow['lg_id'] = trim($value["lg_id"]);
                $dataRow['member_name'] = trim($value["member_name"]);
                $dataRow['specs'] = trim($value["specs"]);
                $dataRow['note'] = trim($value["note"]);
                $dataRow['domain_account'] = trim($value["domain_account"]);
                $allData[] = $dataRow;
            }
        }

        $serial_number_arr = array();
        $it_tag_arr = array();

        foreach ($allData as $key => $value) {
            $value["return_date"] = Helper::maxDate();
            if (!$value["serial_number"]) {
                $value["status_cp"] = "Serial number is required";
                $allData[$key] = $value;
                continue;
            }

            if (in_array($value["serial_number"], $serial_number_arr)) {
                $value["status_cp"] = "Serial number existed in file import";
                $allData[$key] = $value;
                continue;
            } else {
                array_push($serial_number_arr, $value["serial_number"]);
            }

            $dataFind = $this->inventoryRepository->findBySerialNumber($value["serial_number"]);
            if ($dataFind) {
                $value["status_cp"] = "Serial number exist.";
                $allData[$key] = $value;
                continue;
            }

            if (!$value["type1"]) {
                $value["status_cp"] = "Types1 name is required";
                $allData[$key] = $value;
                continue;
            } else {

                $type1_id = array_search($value["type1"], $listType1s);

                if ($type1_id == null) {
                    $value["status_cp"] = "Types1 name not exist.";
                    $allData[$key] = $value;
                    continue;
                } else {
                    $value["type1_id"] = $type1_id;
                    $allData[$key] = $value;
                }

                if (!$value["type2"]) {
                    $value["status_cp"] = "Types2 name is required";
                    $allData[$key] = $value;
                    continue;
                }

                $type2_search = $this->check_type2s($listType2s, $value["type2"], $type1_id);
                if (!$type2_search) {
                    $value["status_cp"] = "Types2 name not match type1";
                    $allData[$key] = $value;
                    continue;
                } else {
                    $value["type2_id"] = $type2_search['id'];
                    $allData[$key] = $value;
                }

            }

            if (!$value["it_tag"]) {
                if ($value["type1"] === Type1::TYPE1_DEVICE || $value["type1"] === Type1::TYPE1_TOOLS) {
                    $value["status_cp"] = "IT tag is required.";
                    $allData[$key] = $value;
                    continue;
                } elseif ($value["type1"] === Type1::TYPE1_MATERIAL) {
                    $value["it_tag"] = "Meterial_" . round(microtime(true) * 1000);

                }
            }else{
                if (in_array($value["it_tag"], $it_tag_arr)) {
                    $value["status_cp"] = "It tag existed in file import.";
                    $allData[$key] = $value;
                    continue;
                } else {
                    array_push($it_tag_arr, $value["it_tag"]);
                    $findItTag = $this->inventoryRepository->findByItTag($value["it_tag"]);
                    if ($findItTag) {
                        $value["status_cp"] = "It tag exist.";
                        $allData[$key] = $value;
                        continue;
                    }
                }
            }

            if (!$value["model_name"]) {
                $value["status_cp"] = "Model name is required";
                $allData[$key] = $value;
                continue;
            }

            $model_id = array_search($value["model_name"], $listModelDictionary);
            if ($model_id == null) {
                $value["status_cp"] = "Model name not exist.";
                $allData[$key] = $value;
                continue;
            } else {
                $value["model_id"] = $model_id;
                $allData[$key] = $value;
            }

            if (!$value["status_inventory"]) {
                $value["status_cp"] = "Status is required";
                $allData[$key] = $value;
                continue;
            }

            $status_id = array_search($value["status_inventory"], Inventory::STATUS);
            if ($status_id == null) {
                $value["status_cp"] = "Status not exist.";
                $allData[$key] = $value;
                continue;
            } else {
                $value["status_id"] = $status_id;
                $allData[$key] = $value;
            }


            if (!is_numeric($value["quantity"])) {
                $value["status_cp"] = "Quantity must be number.";
                $allData[$key] = $value;
                continue;
            }

            if (!$value["is_lg"]) {
                $value["status_cp"] = "Is Lg is required";
                $allData[$key] = $value;
                continue;
            }
            if ($value["is_lg"] !== "Yes" && $value["is_lg"] !== "No") {
                $value["status_cp"] = "Is Lg must be Yes or No";
                $allData[$key] = $value;
                continue;
            }

            if ($value["is_lg"] == "Yes") {
                if (!$value["lg_id"]) {
                    $value["status_cp"] = "Lg Id is required";
                    $allData[$key] = $value;
                    continue;
                }else{
                    $find_member_by_lgID = $this->memberRespository->findByLgId($value["lg_id"]);
                    if (!$find_member_by_lgID) {
                        $value["status_cp"] = "Member not exist.";
                        $allData[$key] = $value;
                        continue;
                    } else {
                        $value["member_id"] = $find_member_by_lgID->id;
                        $allData[$key] = $value;
                    }

                }
            }


            if ($value["is_lg"] == "No") {
                if (!$value["member_name"]) {
                    $value["status_cp"] = "Member is required";
                    $allData[$key] = $value;
                    continue;
                }else{
                    $find_member_by_name = $this->memberRespository->findByMemberName($value["member_name"]);
                    if (!$find_member_by_name) {
                        $value["status_cp"] = "Member not exist.";
                        $allData[$key] = $value;
                        continue;
                    } else {
                        $value["member_id"] = $find_member_by_name->id;
                        $allData[$key] = $value;
                    }

                }
            }

            if (!$value["unit"]) {
                $value["status_cp"] = "Unit is required";
                $allData[$key] = $value;
                continue;
            }

            $unit_id = array_search($value["unit"], $listUnit);
            if ($unit_id == null) {
                $value["status_cp"] = "Unit  not exist.";
                $allData[$key] = $value;
                continue;
            } else {
                $value["unit_id"] = $unit_id;
                $allData[$key] = $value;
            }

            $value["note"] = $value["status_inventory"] == Inventory::STATUS['NG'] ? $value["note"] : '';
            $value["domain_account"] = $value["domain_account"] ? $value["domain_account"] : '';

            $value["status_cp"] = "OK";
            $allData[$key] = $value;

        }
        unlink($pathUpload);
        return view('inventory.import', array('data' => $allData, "showbtn" => true));

    }

    public function getType2s(Request $request)
    {
        $data = $this->type2sRepository->findByType1ID($request->type1_id, $request->q);

        return response()->json($data);
    }

    public function getModelName(Request $request)
    {
        $data = $this->modelDictionaryRepository->findByName($request->q);

        return response()->json($data);
    }


    public function saveDataExcel(Request $request)
    {

        try {
            DB::beginTransaction();
            $inputData = $request->input('list_inventory');
            $dataConvert = json_decode($inputData,true) ;
            $dataInsert = array();
            $dataSuccess = array();
            $dataError = array();
            $totalRecord = count($inputData);
            if ($totalRecord > 0) {
                foreach ($dataConvert as $key => $value) {
                    if ($value["status_cp"] =="OK") {
                        $dataInsert[] = [
                            'type1_id' => $value["type1_id"],
                            'type2_id' => $value["type2_id"],
                            'model_id' => $value["model_id"],
                            'it_tag' => $value["it_tag"],
                            'fa_no' => $value["fa_no"],
                            'serial_number' => $value["serial_number"],
                            'unit' => $value["unit_id"],
                            'specs' => $value["specs"],
                            'note' => $value["note"],
                            'current_user_id' => $value["member_id"],
                            'status' => $value['status_id'],
                            'internal_status' => Inventory::INTERNAL_STATUS["INIT"],
                            'quantity' => $value["quantity"],
                            'return_date' => $value["return_date"],
                            'import_date' => date("Y-m-d"),
                            'domain_account' => $value["domain_account"],
                            'create_manual' => 0,
                            'deliver' => \Auth::user()->name
                        ];
                        $value["status_cp"] = "OK";
                        $dataSuccess[] = $value;
                    } else {
                        $dataError[] = [
                            'serial_number' => $value["serial_number"],
                            'type1' => $value["type1"],
                            'type2' => $value["type2"],
                            'model_name' => $value["model_name"],
                            'status_inventory' => $value["status_inventory"],
                            'fa_no' => $value["fa_no"],
                            'it_tag' => $value["it_tag"],
                            'quantity' => $value["quantity"],
                            'unit' => $value["unit"],
                            'return_date' => $value['return_date'],
                            'quantity' => $value["quantity"],
                            'is_lg' => $value["is_lg"],
                            'lg_id' => $value["lg_id"],
                            'member_name' => $value["member_name"],
                            'specs' => $value["specs"],
                            'note' => $value["note"],
                            'domain_account' => $value["domain_account"],
                            'status_cp' => $value["status_cp"]
                        ];
                    }
                }
                Inventory::insert($dataInsert);

            }
            DB::commit();

            if ($inputData == null || $totalRecord <= 0) {
                $res = "Done";
            } else {
                $totalError = count($dataError);
                $totalSave = count($dataInsert);
                $arrRes = array_merge($dataError, $dataSuccess);
                $res = array("arrRes" => $arrRes, "totalError" => $totalError, "totalSave" => $totalSave);

            }

            echo json_encode($res);
        } catch (\Exception $e) {
            Log::error('InventoryController@saveDataExcel', [$e]);
            DB::rollback();

            foreach ($dataSuccess as $key => $value) {
                $value["status_cp"] = "Insert failed";
                $dataSuccess[$key] = $value;
            }

            $arrRes = array_merge($dataError, $dataSuccess);
            $res = array("arrRes" => $arrRes);

            echo json_encode($res);
            Log::error('InventoryController@saveDataExcel', [$e]);
        }
    }

    public function ajaxSearch(Request $request) {
        $data = [];
        if($request->has('q')){
            $search = $request->q;
            $type = $request->type;
            $inventories = $request->inventories;
            $isEquipement = $request->is_equipment;
            $query = Inventory::query()
                ->select('inventories.*','m.model_name','t1.name as type1_name','t2.name as type2_name','u.name as unit_name')
                ->join('model_dictionary as m', 'm.id','=','inventories.model_id')
                ->join('type1s as t1','t1.id','=','inventories.type1_id')
                ->join('type2s as t2','t2.id','=','inventories.type2_id')
                ->join('unit as u','u.id','=','inventories.unit');

            switch ($type) {
                case config('constant.DOCUMENT_TYPE.IN.key'):

                    $query->where('internal_status','!=','USED');
                    // fix code for CANCEL and DESTROY
                    $query->where('internal_status','!=','CANCEL');
                    $query->where('internal_status','!=','DESTROY');
                    $query->where(function($query){
                        $query->where('internal_status','!=','IN');
                        $query->orWhere('inventories.type1_id','=',Type1::TYPE1_MATERIAL_ID);
                    });
                    break;
                case config('constant.DOCUMENT_TYPE.OUT.key'):
                    $query->where('quantity','>',0);
                    $query->where('internal_status','=',Inventory::INTERNAL_STATUS['IN']);
                    break;
                case config('constant.DOCUMENT_TYPE.REPAIR.key'):
                    $query->where('internal_status','=',Inventory::INTERNAL_STATUS['IN']);
                    if ($isEquipement!=null && $isEquipement == 1) {
                        $query->where('inventories.type1_id', '=',Type1::TYPE1_PARTS_ID);
                    } else {
                        $query->where('inventories.type1_id', '=',Type1::TYPE1_DEVICE_ID);
                    }

                    if ($inventories && count($inventories) > 0) {
                        $query->whereNotIn('inventories.id',$inventories);
                    }
                    break;
                case config('constant.DOCUMENT_TYPE.CANCEL.key'):
                    $query->whereIn('internal_status',[Inventory::INTERNAL_STATUS['IN']]);
                    $query->where('inventories.type1_id','!=',Type1::TYPE1_MATERIAL_ID);

                    break;
                default:
                    Log::error("type is not correct");

            }
            $query->where(function ($query) use ($search) {
                $query->where('serial_number','LIKE',"%$search%")
                    ->orWhere('it_tag','LIKE',"%$search%")
                    ->orWhere('fa_no','LIKE',"%$search%")
                    ->orWhere('m.model_name','LIKE',"%$search%");
            });
            $query->where('is_pending','!=',1);
            $data = $query->get()->take(config('constant.ROW_NUM_IN_PAGE'));
        }
        return response()->json($data);
    }

    public function exportFile(Request $request)
    {

        $list_inventory_error= $request->input('list_inventory_error');

        $myFile = Excel::create('List_Error_Record', function ($excel) use ($list_inventory_error) {
            $excel->sheet('mySheet', function ($sheet) use ($list_inventory_error) {
                $sheet->cell('A1', function ($cell) {
                    $cell->setValue('Serial Number');
                });
                $sheet->cell('B1', function ($cell) {
                    $cell->setValue('Fa No');
                });
                $sheet->cell('C1', function ($cell) {
                    $cell->setValue('IT Tag');
                });
                $sheet->cell('D1', function ($cell) {
                    $cell->setValue('Type1');
                });
                $sheet->cell('E1', function ($cell) {
                    $cell->setValue('Type2');
                });
                $sheet->cell('F1', function ($cell) {
                    $cell->setValue('Model name');
                });
                $sheet->cell('G1', function ($cell) {
                    $cell->setValue('Status');
                });
                $sheet->cell('H1', function ($cell) {
                    $cell->setValue('Quantity');
                });
                $sheet->cell('I1', function ($cell) {
                    $cell->setValue('Unit');
                });
                $sheet->cell('J1', function ($cell) {
                    $cell->setValue('Is Lg');
                });
                $sheet->cell('K1', function ($cell) {
                    $cell->setValue('Lg Id');
                });
                $sheet->cell('L1', function ($cell) {
                    $cell->setValue('Member Name');
                });
                $sheet->cell('M1', function ($cell) {
                    $cell->setValue('Specs');
                });

                $sheet->cell('N1', function ($cell) {
                    $cell->setValue('Note');
                });

                $sheet->cell('O1', function ($cell) {
                    $cell->setValue('Domain account');
                });

                $sheet->cell('P1', function ($cell) {
                    $cell->setValue('Message');
                });

                foreach ($list_inventory_error as $key => $value) {
                    $i = $key + 2;
                    $sheet->cell('A' . $i, $value['serial_number']);
                    $sheet->cell('B' . $i, $value['fa_no']);
                    $sheet->cell('C' . $i, $value['it_tag']);
                    $sheet->cell('D' . $i, $value['type1']);
                    $sheet->cell('E' . $i, $value['type2']);
                    $sheet->cell('F' . $i, $value['model_name']);
                    $sheet->cell('G' . $i, $value['status_inventory']);
                    $sheet->cell('H' . $i, $value['quantity']);
                    $sheet->cell('I' . $i, $value['unit']);
                    $sheet->cell('J' . $i, $value['is_lg']);
                    $sheet->cell('K' . $i, $value['lg_id']);
                    $sheet->cell('L' . $i, $value['member_name']);
                    $sheet->cell('M' . $i, $value['specs']);
                    $sheet->cell('N' . $i, $value['note']);
                    $sheet->cell('O' . $i, $value['domain_account']);
                    $sheet->cell('P' . $i, $value['status_cp']);

                }

            });
        });
        $myFile = $myFile->string('xlsx');
        $response = array(
            'name' => "List_Error_Record",
            'file' => "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," . base64_encode($myFile)
        );
        return response()->json($response);

    }


    public function exportListIventory($listInventory)
    {

        return Excel::create('list_inventory', function ($excel) use ($listInventory) {
            $excel->sheet('list', function ($sheet) use ($listInventory) {

                $sheet->cell('A1', function ($cell) {
                    $cell->setValue('Serial Number');
                });
                $sheet->cell('B1', function ($cell) {
                    $cell->setValue('Fa No');
                });
                $sheet->cell('C1', function ($cell) {
                    $cell->setValue('IT Tag');
                });
                $sheet->cell('D1', function ($cell) {
                    $cell->setValue('Type1');
                });
                $sheet->cell('E1', function ($cell) {
                    $cell->setValue('Type2');
                });
                $sheet->cell('F1', function ($cell) {
                    $cell->setValue('Model name');
                });
                $sheet->cell('G1', function ($cell) {
                    $cell->setValue('Status');
                });
                $sheet->cell('H1', function ($cell) {
                    $cell->setValue('Internal status');
                });
                $sheet->cell('I1', function ($cell) {
                    $cell->setValue('Quantity');
                });
                $sheet->cell('J1', function ($cell) {
                    $cell->setValue('Unit');
                });
                $sheet->cell('K1', function ($cell) {
                    $cell->setValue('Import date');
                });
                $sheet->cell('L1', function ($cell) {
                    $cell->setValue('Return date');
                });
                $sheet->cell('M1', function ($cell) {
                    $cell->setValue('Note');
                });
                $sheet->cell('N1', function ($cell) {
                    $cell->setValue('Domain account');
                });
                $sheet->cell('O1', function ($cell) {
                    $cell->setValue('Specs');
                });

                $sheet->cell('P1', function ($cell) {
                    $cell->setValue('Member');
                });

                $sheet->cell('Q1', function ($cell) {
                    $cell->setValue('Member email');
                });

                $sheet->cell('R1', function ($cell) {
                    $cell->setValue('Member Lg Id');
                });

                $sheet->cell('S1', function ($cell) {
                    $cell->setValue('IS LG Member');
                });

                $sheet->cell('T1', function ($cell) {
                    $cell->setValue('Department');
                });

                $sheet->cell('U1', function ($cell) {
                    $cell->setValue('Company');
                });

                foreach ($listInventory as $key => $value) {
                    $i = $key + 2;
                    $sheet->cell('A' . $i, $value->serial_number);
                    $sheet->cell('B' . $i, $value->fa_no);
                    $sheet->cell('C' . $i, $value->it_tag);
                    $sheet->cell('D' . $i, $value->type1_name);
                    $sheet->cell('E' . $i, $value->type2_name);
                    $sheet->cell('F' . $i, $value->model_name);
                    $sheet->cell('G' . $i, $value->status);
                    $sheet->cell('H' . $i, $value->internal_status);
                    $sheet->cell('I' . $i, $value->quantity);
                    $sheet->cell('J' . $i, $value->unit_name);
                    $sheet->cell('K' . $i, $value->import_date);
                    $sheet->cell('L' . $i, $value->return_date);
                    $sheet->cell('M' . $i, $value->note);
                    $sheet->cell('N' . $i, $value->domain_account);
                    $sheet->cell('O' . $i, $value->specs);
                    $sheet->cell('P' . $i, $value->username);
                    $sheet->cell('Q' . $i, $value->member_email);
                    $sheet->cell('R' . $i, $value->lg_id);
                    $sheet->cell('S' . $i, get_value_by_key($value->is_lg_member,Member::IS_MEMBER));
                    $sheet->cell('T' . $i, $value->department_member);
                    $sheet->cell('U' . $i, $value->company_member);
                }

            });

        })->download("xlsx");

    }

}


