<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function testPdf(Request $request)
    {
        $order = new Order();
        if ($request->type) {
            return $order->getPdf($request->type);
        }
        return $order->getPdf();
    }
}
