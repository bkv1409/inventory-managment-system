<?php

namespace App\Http\Controllers;

use App\Type1;
use App\Type2;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class Type2Controller extends Controller
{

    function __construct()
    {
        $this->middleware('permission:config-list',['only' => ['index','show']]);
        $this->middleware('permission:config-create', ['only' => ['create','store']]);
        $this->middleware('permission:config-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:config-delete', ['only' => ['destroy']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $rowNum = config('constant.ROW_NUM_IN_PAGE');
        $query = Type2::query();
        if (!empty($request->name)) {
            $query->where('name','like', '%'.$request->name.'%');
        }
        if (!empty($request->type1_id) && $request->type1_id != -1) {
            $query->where('type1_id', $request->type1_id);
        }
        $type2s = $query->latest()->paginate($rowNum);

        $type1s = Type1::pluck('name','id')->all();
        $type1s[-1] = 'All';
        $inputs = Input::query();
        return view('type2s.index',compact('type2s','type1s','inputs'))
            ->with('i', (request()->input('page', 1) - 1) * $rowNum);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $type1s = Type1::pluck('name','id')->all();
        return view('type2s.create', compact('type1s'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name' => 'required',
            'type1_id' => 'required',
        ]);


        Type2::create($request->all());


        return redirect()->route('type2s.index')
            ->with('success','Type2 created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Type2  $type2
     * @return \Illuminate\Http\Response
     */
    public function show(Type2 $type2)
    {
        return view('type2s.show',compact('type2'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Type2  $type2
     * @return \Illuminate\Http\Response
     */
    public function edit(Type2 $type2)
    {
        $type1s = Type1::pluck('name','id')->all();
        return view('type2s.edit',compact('type2','type1s'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Type2 $type2
     * @return void
     */
    public function update(Request $request, Type2 $type2)
    {
        request()->validate([
            'name' => 'required',
            'type1_id' => 'required',
        ]);


        $type2->update($request->all());


        return redirect()->route('type2s.index')
            ->with('success','Type2s updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Type2 $type2
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Type2 $type2)
    {
        $type2->delete();

        return redirect()->route('type2s.index')
            ->with('success','Type2 deleted successfully');
    }
}
