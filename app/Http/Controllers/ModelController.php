<?php

namespace App\Http\Controllers;

use App\ModelDictionary;
use Excel;
use Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class ModelController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:config-list',['only' => ['index','show']]);
        $this->middleware('permission:config-create', ['only' => ['create','store']]);
        $this->middleware('permission:config-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:config-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $rowNum = config('constant.ROW_NUM_IN_PAGE');
        $query = ModelDictionary::query();
        if (!empty($request->model_name)) {
            $query->where('model_name','like', '%'.$request->model_name.'%');
        }
        if (!empty($request->category)) {
            $query->where('category', $request->category);
        }
        $models = $query->latest()->paginate($rowNum);

        $inputs = Input::query();
        return view('model_dictionaries.index',compact('models','inputs'))
            ->with('i', (request()->input('page', 1) - 1) * $rowNum);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('model_dictionaries.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'model_name' => 'required',
        ]);


        ModelDictionary::create($request->all());


        return redirect()->route('models.index')
            ->with('success','Model dictionary created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param ModelDictionary $model
     * @return \Illuminate\Http\Response
     */
    public function show(ModelDictionary $model)
    {
        return view('model_dictionaries.show',compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param ModelDictionary $model
     * @return \Illuminate\Http\Response
     */
    public function edit(ModelDictionary $model)
    {
        return view('model_dictionaries.edit', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param ModelDictionary $model
     * @return void
     */
    public function update(Request $request, ModelDictionary $model)
    {
        request()->validate([
            'model_name' => 'required',
        ]);


        $model->update($request->all());


        return redirect()->route('models.index')
            ->with('success','Model Dictionary updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\ModelDictionary $model
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(ModelDictionary $model)
    {
        $model->delete();

        return redirect()->route('models.index')
            ->with('success','Model Dictionary deleted successfully');
    }


    /**
     * Show the application dataAjax.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function ajaxCategory(Request $request)
    {
        $data = [];
        if($request->has('q')){
            $search = $request->q;
            $data = ModelDictionary::query()->select('category', 'category')
                ->where('category','LIKE',"%$search%")
                ->get();
        }

        return response()->json($data);
    }

    public function exportExcel(Request $request){
        $query = ModelDictionary::query();
        if (!empty($request->model_name)) {
            $query->where('model_name','like', '%'.$request->model_name.'%');
        }
        if (!empty($request->category)) {
            $query->where('category', $request->category);
        }
        $count = $query->count();
        if ($count > config('constant.MAX_EXCEL_EXPORT_ROW')) {
            return redirect()->route('models.index')
                ->with('error','Export Excel failed because over maximum row ('.config('constant.MAX_EXCEL_EXPORT_ROW').' rows) ! Please export with some condition!')
                ->with('i', (request()->input('page', 1) - 1) * config('constant.ROW_NUM_IN_PAGE'));
        }
        $fields =['id','model_name','category','created_at'];
        $models = $query->get()->toArray();

        return Helper::exportExcelWithFilter('IMS_ModelsExport', $models, $fields);


    }
}
