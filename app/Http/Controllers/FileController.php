<?php

namespace App\Http\Controllers;

use App\File;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Storage;

class FileController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:config-list',['only' => ['index','show']]);
        $this->middleware('permission:config-create', ['only' => ['create','store']]);
        $this->middleware('permission:config-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:config-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $rowNum = config('constant.ROW_NUM_IN_PAGE');
        $query = File::query();
        if (!empty($request->file_name)) {
            $query->where('file_name','like', '%'.$request->file_name.'%');
        }
        if (!empty($request->type) ) {
            $query->where('type','like', '%'.$request->type.'%');
        }
        if (!empty($request->start_date) ) {
            $query->whereDate('created_at','>=', $request->start_date);
        }
        if (!empty($request->end_date) ) {
            $query->whereDate('created_at','<=', $request->end_date);
        }
//        dd($query->toSql(),$query->getBindings());
        $files = $query->latest()->paginate($rowNum);

        $inputs = Input::query();
        return view('files.index',compact('files','inputs'))
            ->with('i', (request()->input('page', 1) - 1) * $rowNum);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\File  $file
     * @return \Illuminate\Http\Response
     */
    public function show(File $file)
    {
        return view('files.show',compact('file'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\File  $file
     * @return \Illuminate\Http\Response
     */
    public function edit(File $file)
    {
        return view('files.edit',compact('file'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\File  $file
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, File $file)
    {
        request()->validate([
            'file_name' => 'required',
            'file_path' => 'file|max:5120'
        ]);
        $data = $request->all();

        if ($request->hasFile('file_path')) {
            // delete old file
            try {
                Storage::delete($file->path);
            } catch (\Throwable $exception) {
            }
            $uploadFile = $request->file('file_path');
            $type = $uploadFile->getClientOriginalExtension();
            $data['type'] = $type; // overwrite type input

            // save new file
//            $path = $uploadFile->store('files');//automatic file name
            // manual  file name
            $fileName = Helper::getNewFilenameTimestamp().'-'.$uploadFile->getClientOriginalName();
            $path = Storage::putFileAs(
                'files', $uploadFile, $fileName
            );
            $data['path'] = $path;


        }

        $file->update($data);


        return redirect()->route('files.index')
            ->with('success','Files updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\File $file
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(File $file)
    {
        // delete old file
        try {
            Storage::delete($file->path);
        } catch (\Throwable $exception) {
        }
        // delete file model on db
        $file->delete();
        return redirect()->route('files.index')
            ->with('success','Type2 deleted successfully');
    }

    /**
     * Upload Code
     *
     * @return void
     */
    public function dropzoneStore(Request $request)
    {
        request()->validate([
            'file' => 'file|max:8120'
        ]);
        $data = array();

        if ($request->hasFile('file')) {
            $uploadFile = $request->file('file');
            $type = $uploadFile->getClientOriginalExtension();
            $data['type'] = $type; // overwrite type input
            $fileName = Helper::getNewFilenameTimestamp().'-'.$uploadFile->getClientOriginalName();
            $data['file_name'] = $fileName;
            $path = Storage::putFileAs(
                'files', $uploadFile, $fileName
            );
            $data['path'] = $path;
            $data['public_path'] = Storage::url($path);

            $newFile = File::create($data);
            return response()->json(['success'=>$fileName, 'id' => $newFile->id]);
        }
        return response()->json(['error' => 'no has file','id' => -1],500);
    }

    public function dropzoneDelete(Request $request, $fileName) {
        try {
            $file = File::query()->where('file_name','=',$fileName)->first();
            Storage::delete($file->path);
            $file->delete();
            return response()->json(['success'=>'Delete file '.$fileName. ' Done!']);
        } catch (\Throwable $exception) {
            return response()->json(['error'=>'Has error: '.$exception->getMessage()],500);
        }
    }

    public function download(Request $request, File $file){
//        dd($file);
        try {
            return Storage::download($file->path);
        } catch (\Throwable $exception) {
            \Log::error($exception);
            abort(404, "File not found!!!");
        }

    }
}
