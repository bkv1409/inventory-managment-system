<?php

namespace App\Http\Requests\Member;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'lg_id'=> "required|unique:members,lg_id",
            'email' => "required|unique:members,email",
            'phone_no' => "required",
            'company' => "required",

        ];
    }

    public function messages()
    {
        return [
            'name.required' =>'Member name is required.',
            'email.unique' =>'Email existed.',
            'email.required' =>'Email is required.',
            'lg_id.unique' =>'LG ID existed.',
            'lg_id.required' =>'LG ID is required.',
            'company.required' =>'Company is required.',
        ];

    }
}
