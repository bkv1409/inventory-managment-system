<?php

namespace App\Http\Requests\Unit;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function rules()
    {
        $id = $this->route('id');

        return [
            'name' => 'required|unique:unit,name,'.$id,
        ];
    }

    public function messages()
    {
        return [
            'name.required' =>'Unit name is required.',
            'name.unique' =>'Unit name exist.',

        ];

    }
}
