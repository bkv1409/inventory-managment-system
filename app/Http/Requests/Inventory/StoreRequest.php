<?php

namespace App\Http\Requests\Inventory;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'serial_number' => 'required|unique:inventories,serial_number',
            'it_tag' => 'required|unique:inventories,it_tag',
            'quantity' => "integer",
            'type1_id' => "required",
            'type2_id' => "required",
            'model_id' => "required",
            'status' => "required",
            'current_user_id' => "required",
        ];
    }

    public function messages()
    {
        return [
            'serial_number.required' =>'Serial number is required.',
            'serial_number.unique' =>'Serial number exist.',
            'it_tag.required' =>'It tag is required.',
            'it_tag.unique' =>'It tag exist.',

            'quantity.integer' =>'Quantity  must be number.',
            'type1_id.required' =>'Type1 name is required.',
            'type2_id.required' =>'Type2 name is required.',
            'model_id.required' =>'Model name is required.',
            'status.required' =>'Status is required.',
            'current_user_id.required' =>'User is required.',
        ];

    }
}
