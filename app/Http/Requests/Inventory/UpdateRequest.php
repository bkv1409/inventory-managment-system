<?php

namespace App\Http\Requests\Inventory;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('id');

        return [
            'serial_number' => 'required|unique:inventories,serial_number,'.$id,
            'it_tag' => 'required|unique:inventories,it_tag,'.$id,
            'quantity' => "integer",


        ];
    }

    public function messages()
    {
        return [
            'serial_number.required' =>'Serial number is required.',
            'serial_number.unique' =>'Serial number exist.',
            'it_tag.required' =>'It tag is required.',
            'it_tag.unique' =>'It tag exist.',
            'quantity.integer' =>'Quantity  must be number.'

        ];

    }
}
