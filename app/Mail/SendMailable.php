<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMailable extends Mailable
{
    use Queueable, SerializesModels;
   // public $list_inventory;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
      //  $this->list_inventory = $list_inventory;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->view('inventory.mailTemplate')->subject("List inventory expired");
    }
}
