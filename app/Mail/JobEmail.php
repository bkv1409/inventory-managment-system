<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;

class JobEmail extends Mailable
{
    use Queueable, SerializesModels;
    public $mailData;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($mailData)
    {
        $this->mailData = $mailData;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        Log::info("================".json_encode($this->mailData));
        switch ($this->mailData["TYPE"]) {
            case "EDIT":
                return $this->view('inventory.mailEdit')->subject("Edit Inventory");
                break;
            case "CREATE":
                return $this->view('inventory.mailCreate')->subject("Create Inventory");
                break;
            default:
                return $this->view('inventory.mailDelete')->subject("Delete Inventory");
        }

    }

}
