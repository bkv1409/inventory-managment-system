<?php

namespace App;

use App;
use App\Services\DocumentService;
use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    public function files(){
        return $this->belongsToMany('App\File', 'document_has_files', 'document_id', 'file_id');

    }

    public function inventories(){
        return $this->belongsToMany('App\Inventory', 'document_has_inventories', 'document_id', 'inventory_id')
            ->withPivot('delta_quantity');

    }


    public function deliver()
    {
        return $this->belongsTo('App\Member','deliver_id');

    }
    public function receiver()
    {
        return $this->belongsTo('App\Member','receiver_id');

    }

    public function approvedBy(){
        return $this->belongsTo('App\User','approved_by','id');
    }

    public function getPdf($type = 'stream', $repairInventories = array())
    {

        $pdf = App::make('dompdf.wrapper');

        $pdf = $pdf->loadView('documents.preview',
            [
                'document' => $this,
                'repairInventories' => $repairInventories
            ]);


        if ($type == 'stream') {
            return $pdf->stream('document_'.$this->document_no.'.pdf');
        }

        if ($type == 'download') {
            return $pdf->download('document_'.$this->document_no.'.pdf');
        }
    }
}
