<?php
/**
 * Created by PhpStorm.
 * User: conguyen
 * Date: 2019-03-02
 * Time: 20:07
 */

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Inventory extends Model
{
    use SoftDeletes;
    protected $table = 'inventories';
    protected $fillable = ['type1_id','import_date','internal_status','is_pending','target_id'];
    protected $guarded = ['id'];
    const STATUS = [
        "OK"=>"OK",
        "NG"=>"NG",
    ];
    const INTERNAL_STATUS = [
        "IN"=>"IN",
        "OUT"=>"OUT",
//        "RENT"=>"RENT",
        "USED"=>"USED",
        "INIT"=>"INIT",
//        "CANCEL"=>"CANCEL",
        "DESTROY"=>"DESTROY",
    ];
    const ACTION = [
        "CREATE"=>"CREATE",
        "EDIT"=>"EDIT",
        "DELETE"=>"DELETE"
    ];
    public function type1(){
        return $this->belongsTo(Type1::class);
    }

    public function type2(){
        return $this->belongsTo(Type2::class);
    }

    public function unitData(){
        return $this->belongsTo(Unit::class,'unit','id');
    }

    public function model(){
        return $this->belongsTo(ModelDictionary::class,'model_id','id');
    }

    public function target(){
        return $this->belongsTo(Inventory::class,'target_id','id');
    }

    public function equipments(){
        return $this->hasMany(Inventory::class, 'target_id', 'id');
    }
}
