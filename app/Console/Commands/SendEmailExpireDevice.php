<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Log;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;
use App\Repositories\InventoryRepository;
use Excel;
class SendEmailExpireDevice extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sendmail:exprireDevice';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct( InventoryRepository $inventoryRepository)
    {
        $this->inventoryRepository = $inventoryRepository;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $list_inventory =$this->inventoryRepository->findDeviceExpire();
        Log::info("============send mail================list iventory");
        if(count($list_inventory) > 0){

        $excel_file = Excel::create('List_expire_device', function($excel) use ($list_inventory) {
            $excel->sheet('mySheet', function($sheet) use ($list_inventory)
            {
                $sheet->cell('A1', function($cell) {$cell->setValue('Serial Number');   });
                $sheet->cell('B1', function($cell) {$cell->setValue('Return date');   });
                $sheet->cell('C1', function($cell) {$cell->setValue('Model name');   });
                $sheet->cell('D1', function($cell) {$cell->setValue('Receiver');   });
                $sheet->cell('E1', function($cell) {$cell->setValue('Reason');   });
                foreach ($list_inventory as $key => $value) {
                    $i= $key+2;
                    $sheet->cell('A'.$i, $value->serial_number);
                    $sheet->cell('B'.$i, $value->return_date);
                    $sheet->cell('C'.$i, $value->model_name);
                    $sheet->cell('D'.$i, $value->full_name);
                    $sheet->cell('E'.$i, $value->reason);
                }

            });
        });
        $mail_to = config('common.mail_to');
        $message = new SendMailable();
        $message->attach($excel_file->store("xls",false,true)['full']);
        Mail::to($mail_to)->send($message);
        }else{
            Log::info("Record Not found");
        }
     //   Mail::to('phamconguyen@gmail.com')->send(new SendMailable($list_inventory))->attach($excel_file->store("xls",false,true)['full'], 'excel.xls');
    }
}
