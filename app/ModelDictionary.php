<?php
/**
 * Created by PhpStorm.
 * User: conguyen
 * Date: 2019-03-03
 * Time: 10:12
 */

namespace App;
use Illuminate\Database\Eloquent\Model;

class ModelDictionary extends Model
{
    protected $table = 'model_dictionary';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'model_name','category'
    ];
}