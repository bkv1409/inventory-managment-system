<?php
/**
 * Created by PhpStorm.
 * User: vunq
 * Date: 2019-03-19
 * Time: 10:51 AM
 */

namespace App\Services;


use App\Document;
use App\Helpers\Helper;
use App\Inventory;
use App\Type1;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DocumentService
{
    /**
     * @param $documentInventories
     * @return array
     */
    public function buildInventoriesAndRepairs($documentInventories) {
        $inventories = array();
        $repairInventories = array();
        foreach ($documentInventories as $inventory) {
            $inventory->type1_name = $inventory->type1->name;
            $inventory->type2_name = $inventory->type2->name;
            $inventory->model_name = $inventory->model->model_name;
            $inventory->unit_name = $inventory->unitData->name;
            array_push($inventories, $inventory);
            $obj = new \stdClass();
            $obj->id = $inventory->id;
            $obj->obj = $inventory;
            $obj->data = array();
            if ($inventory->equipments) {
                foreach ($inventory->equipments as $equipment){
                    $equipment->type1_name = $equipment->type1->name;
                    $equipment->type2_name = $equipment->type2->name;
                    $equipment->model_name = $equipment->model->model_name;
                    $equipment->unit_name = $equipment->unitData->name;
                    array_push($obj->data, $equipment);
                }
            }
            array_push($repairInventories, $obj);
        }
        return compact('inventories','repairInventories');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function processWithParams(Request $request){
        $query = Document::query()->with(['deliver','receiver','files','inventories','approvedBy']);
        if (!empty($request->document_no)) {
            $query->where('document_no','like', '%'.$request->document_no.'%');
        }
        if (!empty($request->type) ) {
            $query->where('type','=', $request->type);
        }
        if (!empty($request->status) ) {
            $query->where('status','=', $request->status);
        }
        if (!empty($request->member_name) ) {
            $memberName = $request->member_name;
            $query->where(function ($query) use ($memberName) {
                $query->whereHas('deliver', function ($query) use ($memberName){
                    $query->where('name', 'like', '%'.$memberName.'%');
                });
                $query->orWhereHas('receiver', function ($query) use ($memberName){
                    $query->where('name', 'like', '%'.$memberName.'%');
                });
            });
        }
        if (!empty($request->reason) ) {
            $query->where('reason','like', '%'.$request->reason.'%');
        }
        if (!empty($request->note) ) {
            $query->where('note','like', '%'.$request->note.'%');
        }

        if (!empty($request->start_date) ) {
            $query->whereDate('created_at','>=', $request->start_date);
        }
        if (!empty($request->end_date) ) {
            $query->whereDate('created_at','<=', $request->end_date);
        }
        if (!empty($request->approved_start_date) ) {
            $query->whereDate('approved_date','>=', $request->approved_start_date);
        }
        if (!empty($request->approved_end_date) ) {
            $query->whereDate('approved_date','<=', $request->approved_end_date);
        }
        return $query;
    }

    /**
     * store document process
     * @param $request
     */
    public function storeProcess($request)
    {
        $document = new Document();
        $document->document_no = Helper::getNewDocumentNo($request->type);
        $deliver = $request->deliver;
        $document->deliver_id = $deliver['id'];
        $receiver = $request->receiver;
        if ($request->type != config('constant.DOCUMENT_TYPE.CANCEL.key')) $document->receiver_id = $receiver['id'];
        $document->reason = $request->reason;
        $document->note = $request->note;
        $document->status = 'PENDING';
        $document->type = $request->type;
        $document->return_date = $request->return_date;
        $document->save();

        $files = array();
        foreach ($request->attachedFiles as $f) {
            array_push($files,$f['id']);
        }
        $document->files()->sync($files);
        $inventories = array();
        foreach ($request->inventories as $i) {
//            array_push($inventories, $i['id']);
            $inventories[$i['id']] = ['delta_quantity' => $i['quantity']];
        }
        $document->inventories()->sync($inventories);
        foreach ($request->inventories as $i) {
            $inventoryModel = Inventory::query()->find($i['id']);
            $inventoryModel->update([
                'is_pending' => 1
            ]);
        }
        if ($request->dataRepairInventories) {
            foreach ($request->dataRepairInventories as $repairInventory) {
                if ($repairInventory['data'] && count($repairInventory['data']) > 0 ) {
                    foreach ($repairInventory['data'] as $repair){
                        $inventoryModel = Inventory::query()->find($repair['id']);
                        $inventoryModel->update([
                            'is_pending' => 1,
                            'target_id' => $repairInventory['id']
                        ]);
                    }
                }
            }
        }
    }

    /**
     * approve document process
     * @param $document
     * @param $request
     */
    public function approve($document, $request)
    {
        $APPROVED = config('constant.DOCUMENT_STATUS.APPROVED');
        if ($document->inventories) {
            foreach ($document->inventories as $inventory) {

                if ($inventory->type1_id == Type1::TYPE1_MATERIAL_ID) {
                    if ($inventory->internal_status == Inventory::INTERNAL_STATUS['INIT'] && $document->type == config('constant.DOCUMENT_TYPE.IN.key')) {
                        // TODO keep quantity
//                        $inventory->quantity = $request->quantity;
                        $inventory->internal_status = $this->convertInventoryStatus($document->type);
                    } elseif ($inventory->internal_status == Inventory::INTERNAL_STATUS['IN'] && $document->type == config('constant.DOCUMENT_TYPE.IN.key')){
                        // TODO add quantity
                        $inventory->quantity = $inventory->quantity + $inventory->pivot->delta_quantity;
                        $inventory->internal_status = $this->convertInventoryStatus($document->type);
                    } elseif ($inventory->internal_status == Inventory::INTERNAL_STATUS['IN'] && $document->type == config('constant.DOCUMENT_TYPE.OUT.key')){
                        // TODO minus quantity
                        $inventory->quantity = $inventory->quantity - $inventory->pivot->delta_quantity;
                        $inventory->internal_status = $document->type == config('constant.DOCUMENT_TYPE.CANCEL.key') ? $document->type : Inventory::INTERNAL_STATUS['IN'];
                    } else
                        $inventory->internal_status = $this->convertInventoryStatus($document->type);

                } else {
                    $inventory->internal_status = $this->convertInventoryStatus($document->type);
                }
                $inventory->return_date = $document->return_date;
                if ($document->type == config('constant.DOCUMENT_TYPE.IN.key')) $inventory->import_date = Carbon::now();
                $inventory->is_pending = 0;
                $inventory->save();

                if ($document->type == config('constant.DOCUMENT_TYPE.REPAIR.key')) {
                    // save inventories
                    if ($inventory->equipments) {
                        foreach ($inventory->equipments as $equipment) {
                            $equipment->update([
                                'is_pending' => 0,
                                'internal_status' => Inventory::INTERNAL_STATUS['USED']
                            ]);
                        }
                    }
                }
            }
        }
        $document->status = $APPROVED;
        $document->reject_reason = $request->reject_reason;
        $document->approved_by = \Auth::user()->id;
        $document->approved_date = Carbon::now();
        $document->save();
    }

    /**
     * reject document process
     * @param $document
     * @param $request
     *
     */
    public function reject($document, $request)
    {
        $document->status = config('constant.DOCUMENT_STATUS.REJECTED');
        $document->approved_by = \Auth::user()->id;
        $document->approved_date = Carbon::now();
        $document->reject_reason = $request->reject_reason;
        $document->save();
        // update inventories
        if ($document->inventories) {
            foreach ($document->inventories as $inventory) {
                $inventory->update([
                    'is_pending' => 0
                ]);
                if ($document->type == config('constant.DOCUMENT_TYPE.REPAIR.key')) {
                    // save inventories
                    if ($inventory->equipments) {
                        foreach ($inventory->equipments as $equipment) {
                            $equipment->update([
                                'is_pending' => 0,
                                'target_id' => null,
                            ]);
                        }
//                        $inventory->equipments()->detach();
                    }
                }
            }
        }
    }

    /**
     * @param $type
     * @return string
     */
    public function convertInventoryStatus($type) {
        if ($type == config('constant.DOCUMENT_TYPE.REPAIR.key')) {
            return "USED";
        } else {
            return $type;
        }
    }
}