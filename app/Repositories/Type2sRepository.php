<?php
/**
 * Created by PhpStorm.
 * User: conguyen
 * Date: 2019-03-03
 * Time: 09:17
 */

namespace App\Repositories;
use App\Type2s;
use Log;
use DB;


class Type2SRepository
{
    public function __construct(Type2s $type2s)
    {
        $this->type2s = $type2s;

    }

    public function list()
    {
        $listType2= $this->type2s->get()->toArray();
        return $listType2;
    }

    public function findByType1ID($type1Id,$name)
    {
        return $this->type2s::where('type1_id', '=', $type1Id)->where('name', 'LIKE',"%$name%")
            ->get();

    }

    public function findOne($id)
    {
        $unit = $this->type2s::where('id', $id)->first();
        return $unit;
    }

}