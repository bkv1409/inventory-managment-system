<?php
/**
 * Created by PhpStorm.
 * User: conguyen
 * Date: 2019-03-03
 * Time: 20:58
 */

namespace App\Repositories;
use App\Member;
use Log;
use DB;


class MemberRespository
{
    public function __construct(Member $member)
    {
        $this->member = $member;

    }

    public function list()
    {
        $listMember =  $this->member::pluck('name', 'id')->toArray();
        return $listMember;
    }

    public function getAll(array $options = [])
    {
        $query = $this->member::query();

        if (isset($options['name'])) {
            $query = $query->where('name',
                'like', '%'.$options['name'].'%');
        }

        if (isset($options['email'])) {
            $query = $query->where('email',
                'like', '%'.$options['email'].'%');
        }

        if (isset($options['phone_no'])) {
            $query = $query->where('phone_no',
                'like', '%'.$options['phone_no'].'%');
         }


        if (isset($options['company'])) {
            $query = $query->where('company',
                'like', '%'.$options['company'].'%');
        }


       return $query->orderBy('id', 'desc');
    }


    public function store(array $attrs = [])
    {
        $this->member->lg_id = $attrs['lg_id'];
        $this->member->name = $attrs['name'];
        $this->member->is_lg_member = $attrs['is_lg_member'];
        $this->member->email = $attrs['email'];
        $this->member->phone_no = $attrs['phone_no'] ? $attrs['phone_no']: '' ;
        $this->member->department = $attrs['department'];
        $this->member->status = $attrs['status'];
        $this->member->company = $attrs['company'];
        $this->member->domain_account = empty($attrs['domain_account']) ? '' : $attrs['domain_account'];

        $this->member->save();

        return $this->member;
    }

    public function findOne($id)
    {
        $member = $this->member::where('id', $id)->first();
        return $member;
    }

    public function softDelete($id)
    {
        return $this->member->where('id', $id)->delete();
    }

    public function update($id,$input)
    {
        unset($input["_token"]);
        unset($input["_method"]);
        $this->member::whereId($id)->update($input);
    }

    public function findByMemberName($member)
    {
        $member = $this->member::where('name', $member)->first();
        return $member;
    }

    public function findByLgId($lg_id)
    {
        $member = $this->member::where('lg_id', $lg_id)->first();
        return $member;
    }

    public function findByEmail($email)
    {
        $member = $this->member::where('email', $email)->first();
        return $member;
    }

}