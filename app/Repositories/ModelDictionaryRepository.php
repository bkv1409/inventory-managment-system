<?php
/**
 * Created by PhpStorm.
 * User: conguyen
 * Date: 2019-03-03
 * Time: 10:14
 */

namespace App\Repositories;
use App\ModelDictionary;
use Log;
use DB;

class ModelDictionaryRepository
{
    public function __construct(ModelDictionary $modelDictionary)
    {
        $this->modelDictionary = $modelDictionary;

    }

    public function list()
    {
        $listType1 = $this->modelDictionary::pluck('model_name', 'id')->toArray();
        return $listType1;
    }

    public function findByName($name)
    {
        return $this->modelDictionary::where('model_name', 'LIKE',"%$name%")->get();

    }

    public function findOne($id)
    {
        $unit = $this->modelDictionary::where('id', $id)->first();
        return $unit;
    }
}