<?php
/**
 * Created by PhpStorm.
 * User: conguyen
 * Date: 2019-03-03
 * Time: 09:17
 */

namespace App\Repositories;
use App\Type1s;
use Log;
use DB;

class Type1sRepository
{
    public function __construct(Type1s $type1s)
    {
        $this->type1s = $type1s;

    }

    public function list()
    {
        $listType1 =  $this->type1s::pluck('name', 'id')->toArray();
        return $listType1;
    }
}