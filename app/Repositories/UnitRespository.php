<?php
/**
 * Created by PhpStorm.
 * User: conguyen
 * Date: 2019-03-03
 * Time: 20:57
 */

namespace App\Repositories;
use App\Unit;
use Log;
use DB;


class UnitRespository
{
    public function __construct(Unit $unit)
    {
        $this->unit = $unit;

    }

    public function list()
    {
        $listUnit = $this->unit::pluck('name', 'id')->toArray();
        return $listUnit;
    }

    public function getAll(array $options = [])
    {
        $query = $this->unit::query();

        if (isset($options['name'])) {
            $query = $query->where('name',
                'like', '%'.$options['name'].'%');
        }

        return $query;
    }

    public function findOne($id)
    {
        $unit = $this->unit::where('id', $id)->first();
        return $unit;
    }


    public function store(array $attrs = [])
    {

        $this->unit->name = $attrs['name'];
        $this->unit->save();

        return $this->unit;
    }

    public function softDelete($id)
    {
        return $this->unit->where('id', $id)->delete();
    }

    public function update($id,$input)
    {
        unset($input["_token"]);
        unset($input["_method"]);
        $this->unit::whereId($id)->update($input);
    }
}