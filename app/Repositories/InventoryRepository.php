<?php
/**
 * Created by PhpStorm.
 * User: conguyen
 * Date: 2019-03-02
 * Time: 20:14
 */

namespace App\Repositories;

use App\Inventory;
use Log;
use DB;
use App\Repositories\Auth;

class InventoryRepository
{
    public function __construct(Inventory $inventory)
    {
        $this->inventory = $inventory;

    }

    public function list(array $options = [])
    {
        $searchQuery = DB::table('inventories')
            ->join('type1s as type1', 'inventories.type1_id', '=', 'type1.id')
            ->join('type2s as type2', 'inventories.type2_id', '=', 'type2.id')
            ->join('model_dictionary as model', 'inventories.model_id', '=', 'model.id')
            ->join('members as member', 'inventories.current_user_id', '=', 'member.id')
            ->join('unit as u', 'inventories.unit', '=', 'u.id')
            ->orderBy('inventories.id', 'desc')
            ->select(
                'inventories.*', 'type1.name as type1_name',
                'type2.name as type2_name', 'model.model_name as model_name',
                'member.name as username','member.email as member_email','member.lg_id as lg_id',
                'member.is_lg_member as is_lg_member','member.department as department_member',
                'member.company as company_member',
                'u.name as unit_name'
            );

        if (isset($options['type1_id'])) {
            $searchQuery = $searchQuery->where('inventories.type1_id', '=', $options['type1_id']);
        }
        if (isset($options['type2_id'])) {
            $searchQuery = $searchQuery->where('inventories.type2_id', '=', $options['type2_id']);
        }

        if (isset($options['model_id'])) {
            $searchQuery = $searchQuery->where('inventories.model_id', '=', $options['model_id']);
        }
        if (isset($options['status'])) {
            $searchQuery = $searchQuery->where('inventories.status', '=', $options['status']);
        }
        if (isset($options['serial_number'])) {
            $searchQuery = $searchQuery->where('inventories.serial_number',
                'like', '%' . $options['serial_number'] . '%');
        }

        $searchQuery = $searchQuery->where('inventories.deleted_at', '=', NULL);


        return $searchQuery->orderBy('id', 'desc');;
    }


    public function store(array $attrs = [])
    {

        $this->inventory->type1_id = $attrs['type1_id'];
        $this->inventory->type2_id = $attrs['type2_id'];
        $this->inventory->model_id = $attrs['model_id'];
        $this->inventory->it_tag = $attrs['it_tag'] ? $attrs['it_tag'] : '';
        $this->inventory->fa_no = $attrs['fa_no'];
        $this->inventory->serial_number = $attrs['serial_number'];
        $this->inventory->unit = $attrs['unit'];
        $this->inventory->specs = $attrs['specs'] ? $attrs['specs'] : '';
        $this->inventory->status = $attrs['status'];
        $this->inventory->note = array_key_exists("note", $attrs) ? $attrs['note'] : '';
        $this->inventory->import_date = date("Y-m-d");
        $this->inventory->current_user_id = $attrs['current_user_id'];
        $this->inventory->deliver = \Auth::user()->name;
        $this->inventory->return_date = $attrs['return_date'] ? $attrs['return_date'] : '';
        $this->inventory->quantity =  array_key_exists("quantity", $attrs) ? $attrs['quantity'] : 1;
        $this->inventory->internal_status = $this->inventory::INTERNAL_STATUS["INIT"];
        $this->inventory->domain_account = !empty($attrs['domain_account']) ? $attrs['domain_account'] : '';;
        $this->inventory->save();

        return $this->inventory;
    }

    public function softDeleteInventory($id)
    {
        return $this->inventory->where('id', $id)->delete();
    }

    public function findOne($id)
    {
        return $searchQuery = DB::table('inventories')
            ->join('type1s as type1', 'inventories.type1_id', '=', 'type1.id')
            ->join('type2s as type2', 'inventories.type2_id', '=', 'type2.id')
            ->join('model_dictionary as model', 'inventories.model_id', '=', 'model.id')
            ->join('members as member', 'inventories.current_user_id', '=', 'member.id')
            ->join('unit as u', 'inventories.unit', '=', 'u.id')
            ->orderBy('inventories.id', 'desc')
            ->select(
                'inventories.*', 'type1.name as type1_name', 'type1.id as type1_id',
                'type2.name as type2_name', 'type2.id as type2_id', 'model.model_name as model_name',
                'model.id as model_id',
                'member.name as username','member.email as member_email',
                'member.is_lg_member as is_lg_member',
                'member.lg_id as lg_id','member.department as department_member',
                'member.company as company_member',
                'u.name as unit_name', 'u.id as unit_id'
            )->where("inventories.id", $id)->where("inventories.deleted_at", NULL)->first();
    }

    public function findWithTrash($id)
    {
        return $searchQuery = DB::table('inventories')
            ->join('type1s as type1', 'inventories.type1_id', '=', 'type1.id')
            ->join('type2s as type2', 'inventories.type2_id', '=', 'type2.id')
            ->join('model_dictionary as model', 'inventories.model_id', '=', 'model.id')
            ->join('members as member', 'inventories.current_user_id', '=', 'member.id')
            ->join('unit as u', 'inventories.unit', '=', 'u.id')
            ->orderBy('inventories.id', 'desc')
            ->select(
                'inventories.*', 'type1.name as type1_name', 'type1.id as type1_id',
                'type2.name as type2_name', 'type2.id as type2_id', 'model.model_name as model_name',
                'model.id as model_id',
                'member.name as username', 'u.name as unit_name', 'u.id as unit_id'
            )->where("inventories.id", $id)->first();
    }

    public function update($id, $input)
    {
        unset($input["_token"]);
        unset($input["_method"]);
        unset($input["inventory_hidden"]);

        $input["quantity"] = $input["quantity"] ? $input["quantity"] : 1;
       return $this->inventory::whereId($id)->update($input);
    }

    public function findBySerialNumber($serial_number)
    {
        $inventory = $this->inventory::where('serial_number', $serial_number)->first();
        return $inventory;
    }

    public function findByItTag($itTag)
    {
        $inventory = $this->inventory::where('it_tag', $itTag)->first();
        return $inventory;
    }

    public function findDeviceExpire()
    {
        $status_out = $this->inventory::INTERNAL_STATUS["OUT"];
        $status_rent = $this->inventory::INTERNAL_STATUS["RENT"];
        $today = \Carbon\Carbon::now()->format('Y-m-d');
        $queryString = "
        SELECT
            iv.serial_number,iv.model_id,iv.return_date,doc.receiver_id,
            doc.reason,mem.name as full_name,mo.model_name
        FROM inventories as iv
        JOIN document_has_inventories as doHasIv ON iv.id = doHasIv.inventory_id
        JOIN documents as doc  ON doc.id = doHasIv.document_id
        JOIN members as mem  ON doc.receiver_id = mem.id
        JOIN model_dictionary as mo  ON mo.id = iv.model_id
        WHERE iv.return_date <='$today' and (internal_status = '$status_out' OR internal_status = '$status_rent')";
        $inventory = DB::select($queryString);
        return $inventory;
    }

}