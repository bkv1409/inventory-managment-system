<?php
/**
 * Created by PhpStorm.
 * User: conguyen
 * Date: 2019-03-03
 * Time: 20:47
 */

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Member extends Model
{
    use SoftDeletes;
    protected $table = 'members';
    protected $fillable = ['id,name'];
    protected $guarded = ['id'];
    const STATUS=["Emloyment","Resignation"];
    const COMPANY_DEFAULT="LGE";
    const IS_MEMBER = [
        1 => "LG_MEMBER",
        0 => "NON_MEMBER"
    ];

    const STATUS_ARRAY = [
        'Emloyment' => "Emloyment",
        'Resignation' => "Resignation"
    ];

}