<?php
/**
 * Created by PhpStorm.
 * User: vunq
 * Date: 2019-02-25
 * Time: 12:00 PM
 */

namespace App;

use JeroenNoten\LaravelAdminLte\Menu\Builder;
use JeroenNoten\LaravelAdminLte\Menu\Filters\FilterInterface;
use Laratrust;

class MyMenuFilter implements FilterInterface
{
    public function transform($item, Builder $builder)
    {
        if (isset($item['permission'])
//            && ! Laratrust::can($item['permission'])
        ) {
            return false;
        }

        return $item;
    }
}