<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;


class Product extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'detail'
    ];
}
