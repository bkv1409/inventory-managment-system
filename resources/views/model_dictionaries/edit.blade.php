@extends('layouts.master')


@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit Model Dictionary</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('models.index') }}"> Back</a>
            </div>
        </div>
    </div>


    @include('inc.input_alert')

    <form action="{{ route('models.update',$model->id) }}" method="POST">
        @csrf
        @method('PUT')
        <div class="row">
            <div class="col-md-6 col-12">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Name:</strong>
                            {!! Form::text('model_name', $model->model_name, array('placeholder' => 'Enter Name','class' => 'form-control')) !!}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Category:</strong>
                            <br/>
                            {{--{!! Form::text('category', $model->category, array('placeholder' => 'Enter Category','class' => 'form-control','id' => 'category-input')) !!}--}}
{{--                            {!! Form::select('category',array(), $model->category, array('placeholder' => 'Enter Category','class' => 'form-control','id' => 'category-input')) !!}--}}
                            <select class="form-control js-select2" name="category" id="category-input"></select>
                        </div>
                    </div>
                    {{--<div class="col-xs-12 col-sm-12 col-md-12">--}}
                    {{--<div class="form-group">--}}
                    {{--<strong>Created at:</strong>--}}
                    {{--{!! Form::text('create_at', $model->created_at, array('placeholder' => 'Created At','class' => 'form-control','disabled')) !!}--}}
                    {{--</div>--}}
                    {{--</div>--}}

                    {{--<div class="col-xs-12 col-sm-12 col-md-12">--}}
                    {{--<div class="form-group">--}}
                    {{--<strong>Updated At:</strong>--}}
                    {{--{!! Form::text('updated_at', $model->updated_at, array('placeholder' => 'Updated At','class' => 'form-control','disabled')) !!}--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('js')
    <script type="text/javascript">

        $(document).ready(function() {
            $('.js-select2').select2({
                placeholder: 'Select an Category',
                ajax: {
                    url: '/ajax-category',
                    dataType: 'json',
                    delay: 250,
                    processResults: function (data) {
                        return {
                            results:  $.map(data, function (item) {
                                return {
                                    text: item.category,
                                    id: item.category
                                }
                            })
                        };
                    },
                    cache: true
                }
            });
        });



    </script>
@endsection
