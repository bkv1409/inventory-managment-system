@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Model Dictionaries</h2>
            </div>
            <div class="pull-right">
                @can('config-create')
                    <a class="btn btn-success" href="{{ route('models.create') }}"> Create New Model</a>
                @endcan
            </div>
        </div>
    </div>
    <div class="row mb-3">
        <div class="col-md-12">
            <form id="models-search-id" action="{{ route('models.index') }}">
                <div class="row align-middle">
                    <div class="col-xs-12 col-sm-12 col-md-4">
                        <div class="form-group">
                            <strong>Name:</strong>
                            {!! Form::text('model_name', $inputs['model_name'] ?? '', array('placeholder' => 'Name','class' => 'form-control','id' => 'name_input')) !!}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4">
                        <div class="form-group">
                            <strong>Category:</strong>
                            <br/>
                            {!! Form::text('category', $inputs['category'] ?? '' , array('class' => 'form-control','id' => 'category_input')) !!}
                        </div>
                    </div>
                    <div>
                        {!! Form::input('hidden','page',null, ['id' => 'page_input_hidden']) !!}
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 text-center " style="vertical-align: middle;">
                        <button type="button" class="btn btn-primary models-search-ctrl" style="margin: 0 auto !important;	width: 100px;"><i class="fa fa-fw fa-search"></i>Search</button>
                        <button type="button" class="btn btn-light models-clear-ctrl" style="margin: 0 auto !important;	width: 100px;"><i class="fa fa-fw  fa-recycle"></i>Reset</button>
                        <button type="button" class="btn btn-info models-export-ctrl" style="margin: 0 auto !important;	width: 100px;"><i class="fa fa-fw  fa-table"></i>Export</button>
                    </div>
                    {{--<div class="col-xs-12 col-sm-12 col-md-2 text-center " style="vertical-align: middle;">--}}
                        {{--<button type="button" class="btn btn-light models-clear-ctrl" style="margin: 0 auto !important;	width: 100px;"><i class="fa fa-fw  fa-recycle"></i>Reset</button>--}}
                    {{--</div>--}}
                </div>
            </form>
        </div>
    </div>


    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif


    <table class="table table-bordered mt-3">
        {{--<tbody style=" overflow-y:auto;">--}}
        <tr>
            <th>No</th>
            <th>Model Name</th>
            <th>Category</th>
            <th>Created At</th>
            <th>Updated At</th>
            <th width="280px">Action</th>
        </tr>
        @php
            $i = 0;
        @endphp
        @foreach ($models as $model)

            <tr>
                <td>{{ ++$i }}</td>
                <td>{{ $model->model_name }}</td>
                <td>{{ $model->category }}</td>
                <td>{{ $model->created_at }}</td>
                <td>{{ $model->updated_at }}</td>
                <td>
                    <form class="delete-form-ctrl" action="{{ route('models.destroy',$model->id) }}" method="POST">
                        <a class="btn btn-info" href="{{ route('models.show',$model->id) }}">Show</a>
                        @can('config-edit')
                            <a class="btn btn-primary" href="{{ route('models.edit',$model->id) }}">Edit</a>
                        @endcan

                        @csrf
                        @method('DELETE')
                        @can('config-delete')
                            <button type="button" class="btn btn-danger btn-delete-ctrl">Delete</button>
                        @endcan
                    </form>
                </td>
            </tr>
        @endforeach
        {{--</tbody>--}}

    </table>


    {{--{!! $type2s->links() !!}--}}
    {!!  $models->appends($_GET)->links()  !!}



@endsection

@push('js')
    <script>
        $(function() {
            console.log( "ready!" );
            $('.models-search-ctrl').click(function(e) {
                e.preventDefault();
                $('#page_input_hidden').val($('.pagination li.active span').html());
                $( "#models-search-id" ).submit();

            })

            $('.models-clear-ctrl').click(function(e) {
                e.preventDefault();
                $( "#name_input" ).val('');
                $( "#category_input" ).val('');
                $('#page_input_hidden').val(1);
                $( "#models-search-id" ).submit();
            })

            $('.models-export-ctrl').click(function (e) {
                e.preventDefault();
                let href = '{{route('models.exportExcel')}}'+'?'+$( "#models-search-id" ).serialize();
                window.location.href = href;
                // window.open(href,'_blank');
            })

            $('.btn-delete-ctrl').click(function(e) {

                if (confirm('Confirm delete ?')) {
                    $(".delete-form-ctrl").submit();
                    $(this).attr("disabled", true);

                }
                e.preventDefault();
            })
        });

    </script>
@endpush
