@extends('layouts.master')


@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Add New Model Dictionary</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('models.index') }}"> Back</a>
            </div>
        </div>
    </div>


    @include('inc.input_alert')

    {!! Form::open(array('route' => 'models.store','method'=>'POST')) !!}
    @csrf
    <div class="row">
        <div class="col-md-6 col-12">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Name:</strong>
                        {!! Form::text('model_name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Category:</strong>
                        <br/>
                        {!! Form::text('category', null, array('placeholder' => 'Category','class' => 'form-control')) !!}
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}



@endsection
