@extends('layouts.master')


@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> Show Model Dictionary</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('models.index') }}"> Back</a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 col-md-6">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Name:</strong>
                        {!! Form::text('name', $model->model_name, array('placeholder' => 'Name','class' => 'form-control','disabled')) !!}
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Category:</strong>
                        <br/>
                        {!! Form::text('category', $model->category, array('placeholder' => 'Category','class' => 'form-control','disabled')) !!}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Created at:</strong>
                        {!! Form::text('create_at', $model->created_at, array('placeholder' => 'Created At','class' => 'form-control','disabled')) !!}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Updated At:</strong>
                        {!! Form::text('updated_at', $model->updated_at, array('placeholder' => 'Updated At','class' => 'form-control','disabled')) !!}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <a type="button" class="btn btn-primary" href="{{route('models.edit',$model->id)}}">Edit</a>
                </div>

            </div>
        </div>
    </div>

@endsection
