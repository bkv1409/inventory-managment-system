@extends('layouts.master')


@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit Type2</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('type2s.index') }}"> Back</a>
            </div>
        </div>
    </div>


    @include('inc.input_alert')

    <form action="{{ route('type2s.update',$type2->id) }}" method="POST">
        @csrf
        @method('PUT')
               <div class="row">
                   <div class="col-md-6 col-12">
                       <div class="row">
                           <div class="col-xs-12 col-sm-12 col-md-12">
                               <div class="form-group">
                                   <strong>Name:</strong>
                                   {!! Form::text('name', $type2->name, array('placeholder' => 'Name','class' => 'form-control')) !!}
                               </div>
                           </div>
                           <div class="col-xs-12 col-sm-12 col-md-12">
                               <div class="form-group">
                                   <strong>Type1:</strong>
                                   <br/>
                                   {!! Form::select('type1_id', $type1s,$type2->type1_id, array('class' => 'form-control')) !!}
                               </div>
                           </div>
                           {{--<div class="col-xs-12 col-sm-12 col-md-12">--}}
                               {{--<div class="form-group">--}}
                                   {{--<strong>Created at:</strong>--}}
                                   {{--{!! Form::text('create_at', $type2->created_at, array('placeholder' => 'Created At','class' => 'form-control','disabled')) !!}--}}
                               {{--</div>--}}
                           {{--</div>--}}

                           {{--<div class="col-xs-12 col-sm-12 col-md-12">--}}
                               {{--<div class="form-group">--}}
                                   {{--<strong>Updated At:</strong>--}}
                                   {{--{!! Form::text('updated_at', $type2->updated_at, array('placeholder' => 'Updated At','class' => 'form-control','disabled')) !!}--}}
                               {{--</div>--}}
                           {{--</div>--}}
                           <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                               <button type="submit" class="btn btn-primary">Submit</button>
                           </div>
                       </div>
                   </div>
               </div>
    </form>
@endsection
