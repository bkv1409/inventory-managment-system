@extends('layouts.master')


@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> Show Type2</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('type2s.index') }}"> Back</a>
            </div>
        </div>
    </div>


    {{--<div class="row">--}}
        {{--<div class="col-xs-12 col-sm-12 col-md-12">--}}
            {{--<div class="form-group">--}}
                {{--<strong>Name:</strong>--}}
                {{--{{ $type2->name }}--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="col-xs-12 col-sm-12 col-md-12">--}}
            {{--<div class="form-group">--}}
                {{--<strong>Type1:</strong>--}}
                {{--{{ $type2->type1_id }}--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="col-xs-12 col-sm-12 col-md-12">--}}
            {{--<div class="form-group">--}}
                {{--<strong>Created At:</strong>--}}
                {{--{{ $type2->created_at }}--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="col-xs-12 col-sm-12 col-md-12">--}}
            {{--<div class="form-group">--}}
                {{--<strong>Updated At:</strong>--}}
                {{--{{ $type2->updated_at }}--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}

    <div class="row">
        <div class="col-12 col-md-6">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Name:</strong>
                        {!! Form::text('name', $type2->name, array('placeholder' => 'Name','class' => 'form-control','disabled')) !!}
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Type1:</strong>
                        <br/>
                        {!! Form::text('type1_id', $type2->type1 == null ? '' : $type2->type1->name, array('placeholder' => 'Type1','class' => 'form-control','disabled')) !!}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Created at:</strong>
                        {!! Form::text('create_at', $type2->created_at, array('placeholder' => 'Created At','class' => 'form-control','disabled')) !!}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Updated At:</strong>
                        {!! Form::text('updated_at', $type2->updated_at, array('placeholder' => 'Updated At','class' => 'form-control','disabled')) !!}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <a type="button" class="btn btn-primary" href="{{route('type2s.edit',$type2->id)}}">Edit</a>
                </div>

            </div>
        </div>
    </div>

@endsection
