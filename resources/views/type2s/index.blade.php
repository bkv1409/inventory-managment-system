@extends('layouts.master')


@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Type2s</h2>
            </div>
            <div class="pull-right">
                @can('config-create')
                    <a class="btn btn-success" href="{{ route('type2s.create') }}"> Create New Type2</a>
                @endcan
            </div>
        </div>
    </div>
    <div class="row mb-3">
        <div class="col-md-12">
            <form id="type2s-search-id" action="{{ route('type2s.index') }}">
                <div class="row align-middle">
                    <div class="col-xs-12 col-sm-12 col-md-4">
                        <div class="form-group">
                            <strong>Name:</strong>
                            {!! Form::text('name', $inputs['name'] ?? '', array('placeholder' => 'Name','class' => 'form-control','id' => 'name_input')) !!}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4">
                        <div class="form-group">
                            <strong>Type1:</strong>
                            <br/>
                            {!! Form::select('type1_id', $type1s, $inputs['type1_id'] ?? -1 , array('class' => 'form-control','id' => 'type1_id_input')) !!}
                        </div>
                    </div>
                    <div>
                        {!! Form::input('hidden','page',null, ['id' => 'page_input_hidden']) !!}
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-2 text-center " style="vertical-align: middle;">
                        <button type="button" class="btn btn-primary type2s-search-ctrl" style="margin: 0 auto !important;	width: 100px;"><i class="fa fa-fw fa-search"></i>Search</button>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-2 text-center " style="vertical-align: middle;">
                        <button type="button" class="btn btn-light type2s-clear-ctrl" style="margin: 0 auto !important;	width: 100px;"><i class="fa fa-fw  fa-recycle"></i>Reset</button>
                    </div>
                </div>
            </form>
        </div>
    </div>


    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif


    <table class="table table-bordered mt-3">
        {{--<tbody style=" overflow-y:auto;">--}}
        <tr>
            <th>No</th>
            <th>Name</th>
            <th>Type1</th>
            <th>Created At</th>
            <th>Updated At</th>
            <th width="280px">Action</th>
        </tr>
        @php
            $i = 0;
        @endphp
        @foreach ($type2s as $type2)

            <tr>
                <td>{{ ++$i }}</td>
                <td>{{ $type2->name }}</td>
                <td>{{ $type2->type1 == null ? 'N/A' : $type2->type1->name }}</td>
                <td>{{ $type2->created_at }}</td>
                <td>{{ $type2->updated_at }}</td>
                <td>
                    {{--<form action="{{ route('type2s.destroy',$type2->id) }}" method="POST">--}}
                        <a class="btn btn-info" href="{{ route('type2s.show',$type2->id) }}">Show</a>
                        @can('config-edit')
                            <a class="btn btn-primary" href="{{ route('type2s.edit',$type2->id) }}">Edit</a>
                        @endcan


                        {{--@csrf--}}
                        {{--@method('DELETE')--}}
                        @can('config-delete')
                            {{--<button type="submit" class="btn btn-danger">Delete</button>--}}
                            {!! Form::open(['method' => 'DELETE','route' => ['type2s.destroy', $type2->id],'style'=>'display:inline']) !!}
                            {!! Form::submit('Delete', ['class' => 'btn btn-danger','onclick' => 'return CustomJs.ConfirmDelete()']) !!}
                            {!! Form::close() !!}
                        @endcan
                    {{--</form>--}}
                </td>
            </tr>
        @endforeach
        {{--</tbody>--}}

    </table>


    {{--{!! $type2s->links() !!}--}}
    {!!  $type2s->appends($_GET)->links()  !!}



@endsection

@push('js')
    <script>
        $(function() {
            console.log( "ready!" );
            $('.type2s-search-ctrl').click(function(e) {
                e.preventDefault();
                console.log('aa')
                $('#page_input_hidden').val($('.pagination li.active span').html());
                $( "#type2s-search-id" ).submit();

            })

            $('.type2s-clear-ctrl').click(function(e) {
                e.preventDefault();
                $( "#name_input" ).val('');
                $( "#type1_id_input" ).val('-1');
                $('#page_input_hidden').val(1);
                $( "#type2s-search-id" ).submit();
            })
        });

    </script>
@endpush
