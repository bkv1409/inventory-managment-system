@extends('layouts.master')


@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Files</h2>
            </div>
            <div class="pull-right">
                @can('config-create')
                    {{--<a class="btn btn-success" href="{{ route('files.create') }}"> Create New Type2</a>--}}
                @endcan
            </div>
        </div>
    </div>
    <div class="row mb-3">
        <div class="col-md-12">
            <form id="files-search-id" action="{{ route('files.index') }}">
                <div class="row align-middle">
                    <div class="col-xs-12 col-sm-12 col-md-2">
                        <div class="form-group">
                            <label>File Name: </label>
                            <div class="input-group">
                                {!! Form::text('file_name', $inputs['file_name'] ?? '', array('placeholder' => 'Enter file name','class' => 'form-control','id' => 'name_input')) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-2">
                        <div class="form-group">
                            <label>Type: </label>
                            <div class="input-group">
                                {!! Form::text('type', $inputs['type'] ?? '' , array('class' => 'form-control','id' => 'type_input')) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-2">
                        <div class="form-group">
                            <label>Created Date (Start):</label>

                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                {!! Form::text('start_date', $inputs['start_date'] ?? '', array('placeholder' => 'Enter start date','class' => 'form-control datepicker pull-right','id' => 'start_date_input')) !!}
                            </div>
                            <!-- /.input group -->
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-2">
                        <div class="form-group">
                            <label>Created Date (End):</label>
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                {!! Form::text('end_date', $inputs['end_date'] ?? '', array('placeholder' => 'Enter end date','class' => 'form-control datepicker pull-right','id' => 'end_date_input')) !!}
                            </div>
                            <!-- /.input group -->
                        </div>
                    </div>
                    <div>
                        {!! Form::input('hidden','page',null, ['id' => 'page_input_hidden']) !!}
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-2 text-center " style="vertical-align: middle;">
                        <button type="button" class="btn btn-primary files-search-ctrl" style="margin: 0 auto !important;	width: 100px;"><i class="fa fa-fw fa-search"></i>Search</button>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-2 text-center " style="vertical-align: middle;">
                        <button type="button" class="btn btn-light files-clear-ctrl" style="margin: 0 auto !important;	width: 100px;"><i class="fa fa-fw  fa-recycle"></i>Reset</button>
                    </div>
                </div>
            </form>
        </div>
    </div>


    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <div style="overflow-x:auto;">
    <table class="table table-bordered mt-3 table-responsive">
        {{--<tbody style=" overflow-y:auto;">--}}
        <tr>
            <th>No</th>
            <th>File Name</th>
            {{--<th>File Path</th>--}}
            <th>Type</th>
            <th>Created At</th>
            <th>Updated At</th>
            <th width="380px">Action</th>
        </tr>
        @php
            $i = 0;
        @endphp
        @foreach ($files as $file)

            <tr>
                <td>{{ ++$i }}</td>
                <td>{{ $file->file_name }}</td>
                {{--<td>{{ $file->path }}</td>--}}
                <td>{{ $file->type }}</td>
                <td>{{ $file->created_at }}</td>
                <td>{{ $file->updated_at }}</td>
                <td>
{{--                    <form action="{{ route('files.destroy',$file->id) }}" method="POST">--}}
                        <a class="btn btn-info" href="{{ route('files.show',$file->id) }}">Show</a>
                        @can('config-edit')
                            <a class="btn btn-primary" href="{{ route('files.edit',$file->id) }}">Edit</a>
                        @endcan

                        @csrf
                        @method('DELETE')
                        @can('config-delete')
                            {{--<button type="submit" class="btn btn-danger">Delete</button>--}}
                            {!! Form::open(['method' => 'DELETE','route' => ['files.destroy', $file->id],'style'=>'display:inline']) !!}
                            {!! Form::submit('Delete', ['class' => 'btn btn-danger','onclick' => 'return CustomJs.ConfirmDelete()']) !!}
                            {!! Form::close() !!}
                        @endcan
                        @can('config-edit')
                            <a class="btn btn-microsoft" href="{{ route('files.download',$file->id) }}">Download</a>
                        @endcan
                    {{--</form>--}}
                </td>
            </tr>
        @endforeach
        {{--</tbody>--}}

    </table>

    </div>
    {{--{!! $files->links() !!}--}}
    {!!  $files->appends($_GET)->links()  !!}



@endsection

@push('js')
    <script>
        $(function() {
            $('.files-search-ctrl').click(function(e) {
                e.preventDefault();
                console.log('eee')
                $('#page_input_hidden').val($('.pagination li.active span').html());
                $( "#files-search-id" ).submit();

            })

            $('.files-clear-ctrl').click(function(e) {
                e.preventDefault();
                $( "#name_input" ).val('');
                $( "#type_input" ).val('');
                $( "#start_date_input" ).val('');
                $( "#end_date_input" ).val('');
                $('#page_input_hidden').val(1);
                $( "#files-search-id" ).submit();
            })



        });

    </script>

@endpush
