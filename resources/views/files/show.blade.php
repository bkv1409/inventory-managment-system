@extends('layouts.master')


@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> Show File</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('files.index') }}"> Back</a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 col-md-6">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>File Name:</strong>
                        {!! Form::text('name', $file->file_name, array('placeholder' => 'Name','class' => 'form-control','disabled')) !!}
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Type:</strong>
                        <br/>
                        {!! Form::text('type', $file->type, array('placeholder' => '','class' => 'form-control','disabled')) !!}
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Path:</strong>
                        <br/>
                        {!! Form::text('path', $file->path, array('placeholder' => '','class' => 'form-control','disabled')) !!}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Created at:</strong>
                        {!! Form::text('create_at', $file->created_at, array('placeholder' => 'Created At','class' => 'form-control','disabled')) !!}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Updated At:</strong>
                        {!! Form::text('updated_at', $file->updated_at, array('placeholder' => 'Updated At','class' => 'form-control','disabled')) !!}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <a type="button" class="btn btn-primary" href="{{route('files.edit',$file->id)}}">Edit</a>
                </div>

            </div>
        </div>
    </div>

@endsection
