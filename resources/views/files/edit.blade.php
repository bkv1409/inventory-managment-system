@extends('layouts.master')


@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit File</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('files.index') }}"> Back</a>
            </div>
        </div>
    </div>


    @include('inc.input_alert')

    <form action="{{ route('files.update',$file->id) }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="row">
            <div class="col-md-6 col-12">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>File Name:</strong>
                            {!! Form::text('file_name', $file->file_name, array('placeholder' => 'Enter Name','class' => 'form-control')) !!}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Type:</strong>
                            <br/>
                            {!! Form::text('type', $file->type, array('placeholder' => 'Enter type','class' => 'form-control','id' => 'type-input')) !!}
                            {{--                            {!! Form::select('category',array(), $file->category, array('placeholder' => 'Enter Category','class' => 'form-control','id' => 'category-input')) !!}--}}
                            {{--<select class="form-control js-select2" style="width:500px;" name="type" id="category-input"></select>--}}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>File Path:</strong>
                            <br/>
                            {!! Form::text('path_view', $file->path, array('placeholder' => '','class' => 'form-control','disabled')) !!}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <label>Change file:</label>
                                {!! Form::file('file_path', array('placeholder' => '','class' => 'form-control')) !!}
                                <small id="fileHelp" class="form-text text-muted">Please upload a  file. Size of file should not be maximum 5MB.</small>
                            </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Created at:</strong>
                            {!! Form::text('create_at', $file->created_at, array('placeholder' => 'Created At','class' => 'form-control','disabled')) !!}
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Updated At:</strong>
                            {!! Form::text('updated_at', $file->updated_at, array('placeholder' => 'Updated At','class' => 'form-control','disabled')) !!}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('js')
    <script type="text/javascript">

        $(document).ready(function() {
            $('.js-select2').select2({
                placeholder: 'Select an Category',
                ajax: {
                    url: '/ajax-category',
                    dataType: 'json',
                    delay: 250,
                    processResults: function (data) {
                        return {
                            results:  $.map(data, function (item) {
                                return {
                                    text: item.category,
                                    id: item.category
                                }
                            })
                        };
                    },
                    cache: true
                }
            });
        });



    </script>
@endsection
