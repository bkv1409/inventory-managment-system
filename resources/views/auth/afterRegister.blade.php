@extends('layouts.init')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header text-bold">{{ __('Your registration is success') }}</div>

                    <div class="card-body text-red">

                        {{ __('Please request admin to enable your account!') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection