
@extends('adminlte::page')

@section('title', 'Inventory Management System')

@section('content_header')
    @yield('content_header')
@stop

@section('content')
    @yield('content')
@stop

@push('css')
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
@endpush

@push('js')
    <!-- Scripts -->
    <script src="{{ asset('js/custom.js') }}" defer></script>
    <script src="{{ asset('js/app.js') }}" defer></script>

    <script>
        $(function() {
            CustomJs.init();
        });
    </script>
@endpush
