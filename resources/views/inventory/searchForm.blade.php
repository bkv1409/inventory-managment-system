{!! Form::open(array('route' => 'inventory.index','method'=>'get','id'=>'form_clear_all')) !!}
<div class="row">
    <div class="col-md-12">
        <div class="col-md-2">
            <div class="form-group">
                <strong>Type 1</strong>
                @if (isset($input['type1_id']))
                    {!! Form::select('type1_id', array( '' => 'Select type 1') + $list_type1s, $input['type1_id'],
                     ['class' => 'type1_id form-control']) !!}
                @else
                    {!! Form::select('type1_id', array( '' => 'Select type 1') + $list_type1s, null,
                     ['class' => 'type1_id form-control']) !!}
                @endif
            </div>
        </div>
        <div class=" col-md-2">
            <div class="form-group">
                <strong>Type 2</strong>
                <select class="type2_id form-control" id="type2_id" name="type2_id"></select>

            </div>
        </div>
        <div class="form-group col-md-2">
            <div class="form-group">
                <strong>Model Name</strong>

                <select class="model_id form-control" id="model_id" name="model_id"></select>

            </div>
        </div>

        <div class="form-group col-md-2">
            <div class="form-group">
                <strong>Satus</strong>

                @if (isset($input['status']))
                    {!! Form::select('status', array( '' => 'Select status') + $inventory_status, $input['status'],
                     ['class' => 'form-control']) !!}
                @else
                    {!! Form::select('status', array( '' => 'Select status') + $inventory_status, null,
                     ['class' => 'form-control']) !!}
                @endif
            </div>
        </div>

        <div class="form-group col-md-4">
            <div class="form-group">
                <strong>Serial Number</strong>

                @if (isset($input['serial_number']))
                    {!! Form::text('serial_number',$input['serial_number'], ['class' => 'form-control','id' => 'serial_number']) !!}

                @else
                    {!! Form::text('serial_number',null, ['class' => 'form-control',
                'id' => 'serial_number','placeholder' =>'Serial Number']) !!}
                @endif



            </div>
        </div>
    </div>

</div>

<div class="row">
    <div class="form-group col-md-4 "></div>
    <div class="form-group col-md-4 "></div>
    <div class="form-group col-md-4">
        <div class="form-group col-md-5">
            {!! Form::submit('Export', array('class' => 'btn btn-info center-block','name'=>'btnsmt')) !!}
        </div>
        <div class="form-group col-md-5">
            {!! Form::submit('Search', array('class' => 'btn btn-info center-block','name'=>'btnsmt')) !!}
        </div>

        <div class="form-group col-md-2">
            <a href="{{ route('inventory.index') }}" class="btn btn-danger" role="button">Reset</a>
        </div>
    </div>

</div>
{!! Form::close() !!}
