@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> Create Inventory</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('inventory.index') }}"> Back</a>
            </div>
        </div>
    </div>

    <form action="{{ route('inventory.store') }}" method="POST">
        @csrf

        <div class="row">
            <div class=" col-md-6">

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Serial Number</strong>
                        <input type="text" name="serial_number" value ="{{old('serial_number')}}"class="form-control" placeholder="Serial Number">
                        @if ($errors->has('serial_number'))
                            <p class="help-block" style="color:red;font-weight:bold">
                                *{{ $errors->first('serial_number') }}</p>
                        @endif
                    </div>

                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Type1 Name</strong>
                        {!! Form::select('type1_id', array( '' => 'Select Type1 Name') + $list_type1s,old('type1_id'),['class' => 'type1_id form-control','id'=>'type1_id']) !!}
                        @if ($errors->has('type1_id'))
                            <p class="help-block" style="color:red;font-weight:bold">
                                *{{ $errors->first('type1_id') }}</p>
                        @endif
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>FA Number</strong>
                        <input type="text" name="fa_no"  value ="{{old('fa_no')}}" class="form-control" placeholder="FA Number">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>IT Tag</strong>
                        <input type="text" name="it_tag"  value ="{{old('it_tag')}}" class="form-control" placeholder="IT Tag">

                        @if ($errors->has('it_tag'))
                            <p class="help-block" style="color:red;font-weight:bold">
                                *{{ $errors->first('it_tag') }}</p>
                        @endif
                    </div>


                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Type2 Name</strong>
                        <select class="type2_id form-control" id="type2_id"  name="type2_id"></select>
                        @if ($errors->has('type2_id'))
                            <p class="help-block" style="color:red;font-weight:bold">
                                *{{ $errors->first('type2_id') }}</p>
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Model Name</strong>

                        <select class="model_id form-control" id="model_id" name="model_id">
                            {{--@if(old('model_id'))--}}
                                {{--<option value="{{old('model_id')}}">{{old('model_id')}}</option>--}}
                            {{--@endif--}}
                        </select>

                        @if ($errors->has('model_id'))
                            <p class="help-block" style="color:red;font-weight:bold">
                                *{{ $errors->first('model_id') }}</p>
                        @endif

                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Unit</strong>
                        {!! Form::select('unit', array( '' => 'Select Unit') + $list_unit,old('unit'),['class' => 'form-control']) !!}

                    </div>
                </div>
            </div>


            <div class=" col-md-6">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Status</strong>

                        {!! Form::select('status', array( '' => 'Select status') + $inventory_status, old('status'),['class' => 'form-control','id' => 'status']) !!}
                        @if ($errors->has('status'))
                            <p class="help-block" style="color:red;font-weight:bold">*{{ $errors->first('status') }}</p>
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                     
                        <input type="hidden" name="return_date" id="return_date"  value ="{{old('return_date') ?? App\Helpers\Helper::maxDate()}}" class="form-control" placeholder="Return Date">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Current Member</strong>
                        <select class="current_user_id form-control" id="current_user_id"  name="current_user_id"></select>
                        @if ($errors->has('current_user_id'))
                            <p class="help-block" style="color:red;font-weight:bold">
                                *{{ $errors->first('current_user_id') }}</p>
                        @endif

                        {{--{!! Form::select('current_user_id', array( '' => 'Select User') + $list_member, old('current_user_id'),['class' => 'form-control','id' => 'current_user_id']) !!}--}}
                        {{--@if ($errors->has('current_user_id'))--}}
                            {{--<p class="help-block" style="color:red;font-weight:bold">*{{ $errors->first('current_user_id') }}</p>--}}
                        {{--@endif--}}
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Quantity</strong>
                        <input value={{old('quantity',1)}} type="text" name="quantity" id="quantity" class="form-control" placeholder="Quantity" disabled>
                        @if ($errors->has('quantity'))
                            <p class="help-block" style="color:red;font-weight:bold">
                                *{{ $errors->first('quantity') }}</p>
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Note for NG</strong>
                        <textarea class="form-control" id="note" rows="2" name="note" value ="{{old('note')}}"
                                  placeholder="Note for NG"></textarea>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Specs</strong>
                        <textarea class="form-control" rows="2" name="specs" value ="{{old('specs')}}"
                                  placeholder="Specs"></textarea>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Domain Account</strong>
                        <textarea class="form-control" rows="2" name="domain_account" value ="{{old('domain_account')}}"
                                  placeholder="Domain Account"></textarea>
                    </div>
                </div>


            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </div>

    </form>


@stop


@section('css')
    <link rel="stylesheet" href="{{ URL::asset('css/bootstrap-datetimepicker.min.css') }}"/>
@stop

@section('js')
    <script type="text/javascript" src="{{ URL::asset('js/moment.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/bootstrap-datetimepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/inventory_create.js') }}"></script>

    <script type="text/javascript">
        var URL_GET_TYPE2 = '<?php echo url('/inventory/getType2s')?>';
        var URL_GET_MODEL_NAME = '<?php echo url('/inventory/getModelName')?>';
        var URL_GET_MEMBER_NAME = '<?php echo url('/member/ajaxSearch')?>';
    </script>

    <script type="text/javascript" src="{{ URL::asset('js/event_select2.js') }}"></script>
@stop






