@extends('layouts.master')



@section('content')

    @if(session('inventory_save_success'))
        <div class="alert alert-success">
            <strong>{!! session('inventory_save_success') !!}</strong>
        </div>
    @endif
    @if(session('inventory_delete_success'))
        <div class="alert alert-success">
            <strong>{!! session('inventory_delete_success') !!}</strong>
        </div>
    @endif

    @if(session('inventory_delete_error'))
        <div class="alert alert-warning alert-dismissable">
            <strong>{!! session('inventory_delete_error') !!}</strong>
        </div>
    @endif
    @if(session('inventory_save_error'))
        <div class="alert alert-warning alert-dismissable">
            <strong>{!! session('inventory_save_error') !!}</strong>
        </div>
    @endif


    @if(session('invalid_param'))
        <div class="alert alert-warning alert-dismissable">
            <strong>{!! session('invalid_param') !!}</strong>
        </div>
    @endif


    @include ('inventory.searchForm',
    array('list_type1s'=>$list_type1s,
    'type2s'=>$type2s,
    'model_name'=>$model_name,
    'inventory_status'=>$inventory_status,
    'input'=>$input
    ))


    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Inventory</h2>
            </div>
            <div class="pull-right">
                @can('inventory-create')
                <a class="btn btn-success" href="{{route('inventory.create') }}"> Create New Inventory</a>
                @endcan
            </div>
        </div>
    </div>


    <div style="overflow-x:auto;">
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Invt. ID</th>
            <th>Serial Number</th>
            <th>Type1 name</th>
            <th>Type2 name</th>
            <th>Model name</th>
            <th>Status</th>
            <th>Internal status</th>
            <th>Import Date</th>
            <th>Return  Date</th>
            <th>Quantity</th>
            <th>Unit</th>
            <th>Member name</th>
            <th>Member email</th>
            <th>Member Lg Id</th>
            <th>Is Lg Member</th>
            <th width="220px">Action</th>
        </tr>
        @foreach ($list_inventory as $inventory)
            <tr>
                <td>{{ ++$i }}</td>
                <td>{{ $inventory->id }}</td>
                <td>{{ $inventory->serial_number }}</td>
                <td>{{ $inventory->type1_name }}</td>
                <td>{{ $inventory->type2_name }}</td>
                <td>{{ $inventory->model_name }}</td>
                <td>{{ $inventory->status}}</td>
                <td>{{ $inventory->internal_status}}</td>
                <td>{{ $inventory->import_date }}</td>
                <td>{{ $inventory->return_date }}</td>
                <td>{{ $inventory->quantity }}</td>
                <td>{{ $inventory->unit_name }}</td>
                <td>{{ $inventory->username }}</td>
                <td>{{ $inventory->member_email }}</td>
                <td>{{ $inventory->lg_id }}</td>
                <td>{{get_value_by_key($inventory->is_lg_member,$is_member_arr)  }}</td>
                <td>
                    <a class="btn btn-info" href="{{ route('inventory.show',$inventory->id) }}">Show</a>
                    @can('inventory-edit')
                    <a class="btn btn-primary"
                       href="{{ route('inventory.edit',$inventory->id) }}">Edit</a>
                    @endcan
                    @can('inventory-delete')
                    {!! Form::open(['method' => 'DELETE','route' => ['inventory.destroy', $inventory->id],'style'=>'display:inline']) !!}
                    {!! Form::submit('Delete', ['class' => 'btn btn-danger','onclick' => 'return CustomJs.ConfirmDelete()']) !!}
                    {!! Form::close() !!}
                    @endcan

                </td>
            </tr>
        @endforeach
    </table>
    </div>
    {!! $list_inventory->links() !!}

@stop

@section('js')
    <script type="text/javascript" src="{{ URL::asset('js/moment.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/bootstrap-datetimepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/inventory_create.js') }}"></script>
    <script>
        var URL_GET_TYPE2 = '<?php echo url('/inventory/getType2s')?>';
        var URL_GET_MODEL_NAME = '<?php echo url('/inventory/getModelName')?>';

    </script>

    <script type="text/javascript" src="{{ URL::asset('js/event_select2.js') }}"></script>


@stop
