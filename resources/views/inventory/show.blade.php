@extends('layouts.master')

@section('content')

    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> Show Inventory</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('inventory.index') }}"> Back</a>
            </div>
        </div>
    </div>


    <div class="row">
        <div class=" col-md-4">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Serial Number:</strong>
                        {{$row->serial_number}}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>IT Tag:</strong>
                    {{$row->it_tag}}
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>FA Number:</strong>
                    {{$row->it_tag}}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Type1 Name:</strong>
                    {{$row->type1_name}}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Type2 Name:</strong>
                    {{$row->type2_name}}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Model Name:</strong>
                    {{$row->model_name}}

                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Unit:</strong>
                    {{$row->unit_name}}
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Quantity:</strong>
                   {{$row->quantity}}
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Deliver User:</strong>
                    {{$row->deliver}}
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Note for NG</strong>
                    {!! Form::textarea('note',$row->note, ['style'=>"height:65px",
                    'class' => 'form-control','id' => 'note','readonly'=>"readonly"]) !!}
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Specs</strong>
                    {!! Form::textarea('specs',$row->specs, ['style'=>"height:65px",
                    'class' => 'form-control','id' => 'specs','readonly'=>"readonly"]) !!}
                </div>
            </div>

        </div>


        <div class=" col-md-4">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Status:</strong>
                    {{$inventory_status[$row->status]}}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Internal Status:</strong>
                    {{$internal_status[$row->internal_status]}}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Import Date:</strong>
                    {{$row->import_date}}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Plan for Return Date:</strong>
                    {{$row->return_date}}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Current Member:</strong>
                    {{$row->username}}

                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong> Member email:</strong>
                    {{$row->member_email}}

                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Member lg Id:</strong>
                    {{$row->lg_id}}

                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Is Lg Member:</strong>
                    {{ get_value_by_key($row->is_lg_member,$is_member_arr)   }}

                </div>
            </div>



        </div>


    </div>




@stop



