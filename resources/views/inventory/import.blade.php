@extends('layouts.master')



@section('content')

    @if(session('file_format_invalid'))
        <div class="alert alert-warning alert-dismissable">
            <strong>{!! session('file_format_invalid') !!}</strong>
        </div>
    @endif
    @if(session('file_invalid'))
        <div class="alert alert-warning alert-dismissable">
            <strong>{!! session('file_invalid') !!}</strong>
        </div>
    @endif

    @if(session('loading_error'))
        <div class="alert alert-warning alert-dismissable">
            <strong>{!! session('loading_error') !!}</strong>
        </div>
    @endif

    <div id="lbl_import_success" class="alert alert-success">
        <strong>Import finished</strong>
    </div>

    <div id="lbl_import_err" class="alert alert-warning alert-dismissable">
        <strong>Import error please try again !</strong>
    </div>



    <div class="row" style="margin-bottom: 20px">

        <div class="col-xs-6">
            <a href="<?php echo url('/file/Import_Inventory.xlsx'); ?> ">
                {!! Form::button("Download file example", array('class' => 'btn btn-info pull-right','style'=>'margin-right:40px')) !!}
            </a>
            {!! Form::open( ['route' => 'inventory.handleExcel', 'id' => 'form_import_excel', 'files' => true] ) !!}
            <div class="pull-right">
                <button type="submit" class="btn btn-primary">Upload</button>
            </div>
            <div class="form-group">
                {!! Form::file('importExcel'); !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>



    <div class="row">
        <div id="example1" class="dataTable" style="float: left;width: 1500px"></div>
    </div>

    <div class="row">
        <center>
            <button name="save" id="btnSaveImport" class="btn btn-primary btn-sm" style="margin-top: 10px">
                Save Data
            </button>

            <button name="export_file" id="export_file" class="btn btn-primary btn-sm" style="margin-top: 10px">
                Export File Error
            </button>
        </center>
    </div>

    <div class="row">
        <center id="resultTable">
            <table class="table table-bordered" style="width: 300px">
                <thead>
                <tr>
                    <th colspan="2" style="color: red;">Result</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Total record</td>
                    <td><span id="totalRecords">0</span></td>
                </tr>
                <tr>
                    <td>Save Success</td>
                    <td id="sucRecords">0</td>
                </tr>
                <tr>
                    <td>Save Error</td>
                    <td id="errRecords">0</td>
                </tr>
                </tbody>
            </table>

        </center>


        <center id="resultTable_validate">
            <table class="table table-bordered" style="width: 300px">
                <thead>
                <tr>
                    <th colspan="2" style="color: red;">Result</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Total record</td>
                    <td><span id="totalRecordsValidate">0</span></td>
                </tr>
                <tr>
                    <td>Valid</td>
                    <td id="validRecord">0</td>
                </tr>
                <tr>
                    <td>Invalid</td>
                    <td id="invalidRecord">0</td>
                </tr>
                </tbody>
            </table>

        </center>
    </div>


@stop



@section('css')
    <link rel="stylesheet" href="{{ URL::asset('css/handsontable.full.css') }}"/>
@stop

@section('js')
    <script type="text/javascript" src="{{ URL::asset('js/handsontable.full.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery.blockUI.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/import.js') }}"></script>

    <script data-jsfiddle="example1">

        var initData = [{
            serial_number: "Serial 1",
            type1: "Device",
            type2: "Laptop",
            model_name: "Dell",
            status_inventory: "Ok",
            fa_no: "Fa no",
            it_tag: "Tag",
            quantity: "1",
            unit: "Unit",
            is_lg: "Yes",
            lg_id: "LGID",
            member: "VuNQ",
            specs: "",
            note: "",
            domain_account: "",
            status_cp: "OK",
        }];

        <?php if(isset($data) && $data != null):?>
            initData = <?php echo json_encode($data)?>;
        <?php endif;?>

    </script>


    <script data-jsfiddle="example1">

        $(document).ready(function () {
            $("#export_file").hide();

            $("#lbl_import_success").hide();
            $("#lbl_import_err").hide();
            $("#resultTable_validate").show();
            $("#resultTable").hide();
            <?php if (isset($showbtn)) { ?>
            $("#btnSaveImport").show();

            <?php } else { ?>

            $("#btnSaveImport").hide();
            <?php } ?>
            window.URL_IMPORT ='<?php echo url('/inventory/saveDataExcel')?>';
            window.URL_EXPORT ='<?php echo url('/inventory/exportFile')?>';
            Import.buildTable();

        });


    </script>

@stop





