@extends('layouts.master')

@section('content')

    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> Edit Inventory</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('inventory.index') }}"> Back</a>
            </div>
        </div>
    </div>

    {!! Form::Model($row,['route' => ['inventory.update', $row->id]]) !!}
        @csrf
        @method('PUT')



    <div class="row">
        <div class=" col-md-6">

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Serial Number</strong>
                    {!! Form::text('serial_number',null, ['class' => 'form-control','id' => 'serial_number','placeholder' =>'Serial Number']) !!}
                    @if ($errors->has('serial_number'))
                        <p class="help-block" style="color:red;font-weight:bold">*{{ $errors->first('serial_number') }}</p>
                    @endif
                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">

                <div class="form-group">
                    <strong>Type1 Name</strong>
                    {!! Form::select('type1_id', $list_type1s, null,['class' => 'type1_id form-control']) !!}
                    @if ($errors->has('type1_id'))
                        <p class="help-block" style="color:red;font-weight:bold">*{{ $errors->first('type1_id') }}</p>
                    @endif
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>FA Number</strong>
                    {!! Form::text('fa_no',null, ['class' => 'form-control','id' => 'fa_no','placeholder' =>'FA Number']) !!}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">

                <div class="form-group">
                    <strong>IT Tag</strong>
                    {!! Form::text('it_tag',null, ['class' => 'form-control it_tag','id' => 'it_tag','placeholder' =>'It tag']) !!}
                    @if ($errors->has('it_tag'))
                        <p class="help-block" style="color:red;font-weight:bold">
                            *{{ $errors->first('it_tag') }}</p>
                    @endif
                </div>

            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Type2 Name</strong>
                    <select class=" type2_id form-control" id="type2_id" name="type2_id"></select>
                    @if ($errors->has('type2_id'))
                        <p class="help-block" style="color:red;font-weight:bold">*{{ $errors->first('type2_id') }}</p>
                    @endif
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Model Name</strong>

                    <select class=" model_id form-control" id="model_id" name="model_id"></select>

                    @if ($errors->has('model_id'))
                        <p class="help-block" style="color:red;font-weight:bold">*{{ $errors->first('model_id') }}</p>
                    @endif

                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Unit</strong>
                    {!! Form::select('unit', $list_unit, null,['class' => 'form-control']) !!}

                </div>
            </div>



        </div>


        <div class=" col-md-6">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Status</strong>
                    {!! Form::select('status',  $inventory_status, null,['class' => 'form-control','id' => 'status']) !!}
                    @if ($errors->has('status'))
                        <p class="help-block" style="color:red;font-weight:bold">*{{ $errors->first('status') }}</p>
                    @endif
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Return Date</strong>
                    {!! Form::hidden('return_date',null, ['class' => 'form-control','id' => 'return_date','placeholder' =>'Return Date']) !!}

                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">

                    <strong>Current Member</strong>

                    <select class=" current_user_id form-control" id="current_user_id" name="current_user_id"></select>

                    @if ($errors->has('current_user_id'))
                        <p class="help-block" style="color:red;font-weight:bold">*{{ $errors->first('current_user_id') }}</p>
                    @endif

                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Quantity</strong>
                    {!! Form::text('quantity',null, ['class' => 'form-control','id' => 'quantity','placeholder' =>'Quantity']) !!}
                    @if ($errors->has('quantity'))
                        <p class="help-block" style="color:red;font-weight:bold">*{{ $errors->first('quantity') }}</p>
                    @endif
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Note</strong>
                    {!! Form::textarea('note',null, ['style'=>"height:30px",'class' => 'form-control','id' => 'note','placeholder' =>'Note']) !!}

                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Description</strong>
                    {!! Form::textarea('specs',null, ['style'=>"height:30px",'class' => 'form-control','id' => 'specs','placeholder' =>'Description']) !!}
                </div>
            </div>



            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Domain Account</strong>
                    {!! Form::textarea('domain_account',null, ['style'=>"height:30px",'class' => 'form-control','id' => 'domain_account','placeholder' =>'Domain Account']) !!}

                </div>
            </div>


        </div>


        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                {!! Form::hidden('inventory_hidden',json_encode($row), ['style'=>"height:30px",'class' => 'form-control','id' => 'inventory_hidden' ]) !!}

            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary">Update</button>
        </div>
    </div>

    {!! Form::close() !!}


@stop


@section('css')
    <link rel="stylesheet" href="{{ URL::asset('css/bootstrap-datetimepicker.min.css') }}" />
@stop

@section('js')
    <script type="text/javascript" src="{{ URL::asset('js/moment.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/bootstrap-datetimepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/inventory_create.js') }}"></script>
    <script>

        var dataType2s = {
            id: '<?php echo $type2s_name["id"] ?>',
            name:'<?php echo $type2s_name["name"] ?>'
        };
        var dataModelName = {
            id: '<?php echo $model_name["id"] ?>',
            model_name:'<?php echo $model_name["model_name"] ?>'
        };

        var dataMemberName = {
            id: '<?php echo $member_name["id"] ?>',
            name:'<?php echo $member_name["name"] ?>'
        };
        var type2s = new Option(dataType2s.name, dataType2s.id, false, false);
        $('#type2_id').append(type2s).trigger('change');
        var modelName = new Option(dataModelName.model_name, dataModelName.id, false, false);
        $('#model_id').append(modelName).trigger('change');

        var memberName = new Option(dataMemberName.name, dataMemberName.id, false, false);
        $('#current_user_id').append(memberName).trigger('change');
    </script>

    <script type="text/javascript">
        var URL_GET_TYPE2 = '<?php echo url('/inventory/getType2s')?>';
        var URL_GET_MODEL_NAME = '<?php echo url('/inventory/getModelName')?>';
        var URL_GET_MEMBER_NAME = '<?php echo url('/member/ajaxSearch')?>';
    </script>

    <script type="text/javascript" src="{{ URL::asset('js/event_select2.js') }}"></script>
@stop

