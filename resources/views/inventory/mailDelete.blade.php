<h4>Mail Create </h4>
<p>Action: <h5><strong>DELETE</strong></h5></p>
<p>Delete By: <h5>{{ $mailData["user_name"] }}</h5></p>
<hr/>
<h2><strong>Data Delete</strong></h2>
<table class="table table-bordered">
    <tr>
        <th>Serial Number</th>
        <th>Type1 name</th>
        <th>Type2 name</th>
        <th>Model name</th>
        <th>Status</th>
        <th>Internal status</th>
        <th>Import Date</th>
        <th>Return Date</th>
        <th>Quantity</th>
        <th>Member</th>
        <th>Unit</th>
        <th>It Tag</th>
        <th>Fa No</th>
        <th>Spect</th>
        <th>Note</th>
        <th>Domain Account</th>
    </tr>
    <tr>

        <td>{{ $mailData["new_data"]->serial_number }}</td>
        <td>{{ $mailData["new_data"]->type1_name}}</td>
        <td>{{ $mailData["new_data"]->type2_name }}</td>
        <td>{{ $mailData["new_data"]->model_name }}</td>
        <td>{{ $mailData["new_data"]->status }}</td>
        <td>{{ $mailData["new_data"]->internal_status }}</td>
        <td>{{ $mailData["new_data"]->import_date }}</td>
        <td>{{ $mailData["new_data"]->return_date }}</td>
        <td>{{ $mailData["new_data"]->quantity }}</td>
        <td>{{ $mailData["new_data"]->username }}</td>
        <td>{{ $mailData["new_data"]->unit_name }}</td>
        <td>{{ $mailData["new_data"]->it_tag }}</td>
        <td>{{ $mailData["new_data"]->fa_no }}</td>
        <td>{{ $mailData["new_data"]->specs }}</td>
        <td>{{ $mailData["new_data"]->note }}</td>
        <td>{{ $mailData["new_data"]->domain_account }}</td>

    </tr>

</table>


