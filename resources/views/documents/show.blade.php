@extends('layouts.master')


@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Show Document Information</h2>
            </div>
            <div class="pull-right">
                @include('inc.document_reject_approve_button')

                <a class="btn btn-primary" href="{{ route('documents.index') }}"> Back</a>
            </div>
        </div>
        <div class="col-lg-12">
            {{--Modal--}}
            @include('inc.document_reject_modal')
            @include('inc.document_approve_modal')
        </div>
    </div>


    @include('inc.input_alert')

    <div class="">

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-3">
                <div class="form-group">
                    <strong>Document No.:</strong>
                    <div>{{ $document->document_no }}</div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-3">
                <div class="form-group">
                    <strong>Type:</strong>
                    <div>{{ $document->type }}</div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6">
                <div class="form-group">
                    <label>Trạng thái</label>
                    <div>
                        <!--<span class="badge">PENDING</span>-->
                        @include('inc.document_status',['documentStatus' => $document->status])
                    </div>
                </div>
            </div>
            <member-document is-deliver='true' is-readonly="true" :props-member="{{json_encode($document->deliver)}}"></member-document>
            @if($document->type!=config('constant.DOCUMENT_TYPE.CANCEL.key'))
                <member-document is-readonly="true" :props-member="{{json_encode($document->receiver)}}"></member-document>
            @endif
        </div>

        <inventory-list
                can-edit="true" can-delete="true"
                :inventories='{{ json_encode($inventories) }}'
                :repair-inventories='{{ json_encode($repairInventories) }}'
                :readonly="Boolean(true)"
                :type="'{{$document->type}}'"
        ></inventory-list>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Reason:</strong>
                    <textarea name="reason" disabled class="form-control" rows="3" >{{$document->reason}}</textarea>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <!--<div class="form-group">-->
                <strong>Attached Files:</strong>


                <!--<div id="fileBoxId" class="dropzone" >-->
                <!--<input name="file" type="file" multiple />-->
                <!--</div>-->
                <!--<form :action="filesStoreRoute" class="dropzone" id="fileDocumentUpload" enctype="multipart/form-data">-->
                <!--<input type="hidden" name="_token" v-model="token" />-->
                <!--<div class="fallback">-->
                <!--<input name="file" type="file" multiple />-->
                <!--</div>-->
                <!--<div>-->
                <!--&lt;!&ndash;<h3>Upload Multiple File By Click On Box</h3>&ndash;&gt;-->
                <!--</div>-->

                <!--</form>-->
                <div class="">
                    @foreach($document->files as $file)
                        <div>{{$file->file_name}}</div>
                    @endforeach
                </div>

                <!--</div>-->
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Note:</strong>
                    <br/>
                    <textarea name="note" disabled class="form-control" rows="3" disabled >{{ $document->note }}</textarea>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 mt-1">
                <div class="form-group">
                    <label>Thời gian dự kiến trả lại/ Return Date: </label>

                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input name="returnDate" class="form-control datepicker pull-right" disabled value="{{$document->return_date}}"/>
                    </div>
                    <!-- /.input group -->
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Thời gian duyệt/ Approved Date: </label>

                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input name="approvedDate" class="form-control datepicker pull-right" disabled value="{{$document->approved_date}}"/>
                    </div>
                    <!-- /.input group -->
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-6">
                <div class="form-group">
                    <strong>Người duyệt: </strong>
                    <br/>
                    <input type="text" name="approved_by" class="form-control"
                           disabled value="{{$document->approvedBy == null ? 'N/A' : ($document->approvedBy->name.' ('.$document->approvedBy->email .')')}}" />
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6">
                <div class="form-group">
                    <strong>Lý do từ chối: </strong>
                    <br/>
                    <textarea name="reject_reason"  class="form-control" rows="2" disabled >{{$document->reject_reason}}</textarea>

                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <a type="button" class="btn btn-primary " href="{{ route('documents.index') }}">Close</a>
            </div>
        </div>
    </div>


@endsection

