@extends('layouts.master')


@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Update Document Files</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('documents.index') }}"> Back</a>
            </div>
        </div>
    </div>


    @include('inc.input_alert')

    <document-update-files
            :document="{{ json_encode($document) }}"
            :files="{{ json_encode($files) }}"
            files-store-route="{{ route('files.dropzoneStore') }}"
            documents-update-files-route="{{ route('documents.updateFiles',$document->id) }}"
    ></document-update-files>


@endsection
