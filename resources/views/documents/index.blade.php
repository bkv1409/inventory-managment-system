@extends('layouts.master')


@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Documents</h2>
            </div>
            <div class="pull-right">
                @can('document-create')
                    <a class="btn btn-success" href="{{ route('documents.create') }}">Create New Document</a>
                @endcan
            </div>
        </div>
    </div>
    <div class="row mb-3">
        <div class="col-md-12">
            <form id="documents-search-id" action="{{ route('documents.index') }}">
                <div class="row align-middle">
                    <div class="col-xs-12 col-sm-12 col-md-2">
                        <div class="form-group">
                            <label>Document No: </label>
                            <div class="input-group">
                                {!! Form::text('document_no', $inputs['document_no'] ?? '', array('placeholder' => 'Enter document no','class' => 'form-control','id' => 'document_no_input')) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-2">
                        <div class="form-group">
                            <label>Status: </label>
                            <div class="input-group">
                                {!! Form::select('status', $documentStatus, $inputs['status'] ?? '' , array('class' => 'form-control','id' => 'status_input')) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-2">
                        <div class="form-group">
                            <label>Type: </label>
                            <div class="input-group">
                                {!! Form::select('type', $documentTypes, $inputs['type'] ?? '' , array('class' => 'form-control','id' => 'type_input')) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-2">
                        <div class="form-group">
                            <label>Reason: </label>
                            <div class="input-group">
                                {!! Form::text('reason', $inputs['reason'] ?? '', array('placeholder' => 'Enter Reason','class' => 'form-control','id' => 'reason_input')) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-2">
                        <div class="form-group">
                            <label>Note: </label>
                            <div class="input-group">
                                {!! Form::text('note', $inputs['note'] ?? '', array('placeholder' => 'Enter Note','class' => 'form-control','id' => 'note_input')) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-2" >
                        <div class="form-group">
                            <label>Deliver/ Receiver: </label>
                            <div class="input-group">
                                {!! Form::text('member_name', $inputs['member_name'] ?? '', array('placeholder' => 'Enter Deliver/ Receiver','class' => 'form-control','id' => 'member_name_input')) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row align-middle">
                    <div class="col-xs-12 col-sm-12 col-md-2">
                        <div class="form-group">
                            <label>Created Date (Start):</label>

                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                {!! Form::text('start_date', $inputs['start_date'] ?? '', array('placeholder' => 'Enter start date','class' => 'form-control datepicker pull-right','id' => 'start_date_input')) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-2">
                        <div class="form-group">
                            <label>Created Date (End):</label>
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                {!! Form::text('end_date', $inputs['end_date'] ?? '', array('placeholder' => 'Enter end date','class' => 'form-control datepicker pull-right','id' => 'end_date_input')) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-2">
                        <div class="form-group">
                            <label>Approved Date (Start):</label>
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                {!! Form::text('approved_start_date', $inputs['approved_start_date'] ?? '', array('placeholder' => 'Enter approved start date','class' => 'form-control datepicker pull-right','id' => 'approved_start_date_input')) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-2">
                        <div class="form-group">
                            <label>Approved Date (End):</label>
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                {!! Form::text('approved_end_date', $inputs['approved_end_date'] ?? '', array('placeholder' => 'Enter approved end date','class' => 'form-control datepicker pull-right','id' => 'approved_end_date_input')) !!}
                            </div>
                        </div>
                    </div>
                    <div>
                        {!! Form::input('hidden','page',null, ['id' => 'page_input_hidden']) !!}
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-4 text-center mt-4" style="vertical-align: middle;">
                        <button type="button" class="btn btn-primary documents-search-ctrl" style="margin: 0 auto !important;	width: 100px;"><i class="fa fa-fw fa-search"></i>Search</button>
                        <button type="button" class="btn btn-light documents-clear-ctrl" style="margin: 0 auto !important;	width: 100px;"><i class="fa fa-fw  fa-recycle"></i>Reset</button>
                        <button type="button" class="btn btn-info documents-export-ctrl" style="margin: 0 auto !important;	width: 100px;"><i class="fa fa-fw  fa-table"></i>Export</button>
                    </div>
                    {{--<div class="col-xs-12 col-sm-12 col-md-2 text-center mt-4" style="vertical-align: middle;">--}}
                    {{--<button type="button" class="btn btn-light documents-clear-ctrl" style="margin: 0 auto !important;	width: 100px;"><i class="fa fa-fw  fa-recycle"></i>Reset</button>--}}
                    {{--</div>--}}
                </div>
            </form>
        </div>
    </div>


    @include('inc.index_alert')

    <div style="overflow-x:auto;">

    <table class="table table-bordered mt-3 table-responsive">
        {{--<tbody style=" overflow-y:auto;">--}}
        <tr>
            <th width="2%;">No</th>
            <th width="2%;">ID</th>
            <th width="8%;">Document No.</th>
            <th width="5%;">Deliver</th>
            <th width="5%;">Receiver</th>
            <th width="15%;">Reason</th>
            <th width="13%;">Note</th>
            <th width="5%;">Approve By</th>
            <th width="5%;">Status</th>
            <th width="10%;">Reject/ Approve reason</th>
            <th width="4%;">Type</th>
            <th width="5%;">Created At</th>
            <th width="5%;">Approved Time</th>
            <th width="16%">Action</th>
        </tr>
        @php
            $i = 0;
        @endphp
        @foreach ($documents as $document)

            <tr>
                <td>{{ ++$i }}</td>
                <td>{{ $document->id }}</td>
                <td>{{ $document->document_no }}</td>
                <td>{{ $document->deliver->name }}</td>
                <td>{{ $document->receiver ? $document->receiver->name : 'N/A'}}</td>
                <td class="ellipsis" style="max-width: 150px;">{{ $document->reason }}</td>
                <td class="ellipsis" style="max-width: 150px;">{{ $document->note }}</td>
                <td>{{ $document->approvedBy == null ? 'N/A' : $document->approvedBy->name }}</td>
                {{--<td>{{ $document->status }}</td>--}}
                <td>
                    @include('inc.document_status',['documentStatus' => $document->status])
                </td>
                <td class="ellipsis" style="max-width: 150px;">{{ $document->reject_reason }}</td>
                <td>{{ $document->type }}</td>
                <td>{{ $document->created_at }}</td>
                <td>{{ $document->approved_date }}</td>
                <td>

                    <a class="btn btn-info " href="{{ route('documents.show',$document->id) }}">Show</a>
                    {{--@if(strcmp($document->status,"PENDING")==0)--}}
                    @include('inc.document_reject_approve_button')

                    @if($document->status == "APPROVED")
                        @can('document-show')
                        <a class="btn btn-yahoo" id="model_document_preview_{{$document->id}}"
                           href="{{route('documents.previewPart',$document->id)}}" >
                            Preview
                        </a>
                        @endcan
                        {{--<a class="btn btn-foursquare" id="model_document_dl_pdf_{{$document->id}}"--}}
                           {{--href="{{route('documents.downloadPdf',$document->id)}}" >--}}
                            {{--PDF--}}
                        {{--</a>--}}
                    @endif


                    <!-- Modal -->
                    @include('inc.document_reject_modal')
                    @include('inc.document_approve_modal')
                </td>
            </tr>
        @endforeach
        {{--</tbody>--}}

    </table>

    </div>

    {{--{!! $documents->links() !!}--}}
    {!!  $documents->appends($_GET)->links()  !!}



@endsection

@push('js')
    <script>
        $(function() {
            $('.documents-search-ctrl').click(function(e) {
                e.preventDefault();
                console.log('eee')
                $('#page_input_hidden').val($('.pagination li.active span').html());
                $( "#documents-search-id" ).submit();

            })

            $('.documents-clear-ctrl').click(function(e) {
                e.preventDefault();
                $( "#document_no_input" ).val('');
                $( "#status_input" ).val('');
                $( "#type_input" ).val('');
                $( "#member_name_input" ).val('');
                $( "#reason_input" ).val('');
                $( "#note_input" ).val('');
                $( "#start_date_input" ).val('');
                $( "#end_date_input" ).val('');
                $( "#approved_start_date_input" ).val('');
                $( "#approved_end_date_input" ).val('');
                $('#page_input_hidden').val(1);
                $( "#documents-search-id" ).submit();
            })

            $('.documents-export-ctrl').click(function (e) {
                e.preventDefault();
                let href = '{{route('documents.exportExcel')}}'+'?'+$( "#documents-search-id" ).serialize();
                window.location.href = href;
                // window.open(href,'_blank');
            })

        });

    </script>

@endpush

@section('css')
    <style>
        ellipsis {
            white-space: nowrap;
            /*border: solid 1px;*/
            width: 100px;
            /*height: 18px;*/
            text-overflow: ellipsis;
            overflow: hidden;
        }
        td {
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
        }
        .break-word {
            word-wrap: break-word;
        }
    </style>
    @endsection


