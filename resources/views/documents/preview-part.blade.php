@extends('layouts.master')


@section('content')

    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Preview Document</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-yahoo" href="{{ route('documents.preview', $document->id) }}"> Preview Full</a>
                <a class="btn btn-dropbox" href="{{ route('documents.printPdf',$document->id) }}"> Show PDF</a>
                <a class="btn btn-foursquare" href="{{ route('documents.downloadPdf',$document->id) }}"> Download PDF</a>
                <a class="btn btn-primary" href="{{ route('documents.index') }}"> Back</a>
            </div>
        </div>
    </div>

    @include('documents.pdf-core')

@endsection

@section('css')

    @include('documents.pdf-style')

@endsection
