@extends('layouts.master')


@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Add New Document</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('documents.index') }}"> Back</a>
            </div>
        </div>
    </div>


    @include('inc.input_alert')

    <document-create-new
            :document-types="{{ json_encode($documentTypes) }}"
            max-date="{{ Helper::maxDate() }}"
            files-store-route="{{ route('files.dropzoneStore') }}"
            inventories-store-route="{{ route('inventory.store') }}"
            :list-type1s="{{ json_encode($listType1s) }}"
            :list-type2s="{{ json_encode($listType2s) }}"
            :list-unit="{{ json_encode($listUnit) }}"
            :list-member="{{ json_encode($listMember) }}"
            :list-status="{{ json_encode($listStatus) }}"
            :list-model-dictionary="{{ json_encode($listModelDictionary) }}"
            inventory-get-type2s-route="{{ route('inventory.getType2s') }}"
    ></document-create-new>


@endsection

