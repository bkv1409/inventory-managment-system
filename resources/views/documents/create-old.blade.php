@extends('layouts.master')


@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Add New Document</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('documents.index') }}"> Back</a>
            </div>
        </div>
    </div>


    @include('inc.input_alert')

    <div class="wapper">
        {{--{!! Form::open(array('route' => 'documents.store','method'=>'POST')) !!}--}}
        {{--<form method="POST" action="{{route('documents.store')}}" accept-charset="UTF-8">--}}
        {{--@csrf--}}
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6">
                <div class="form-group">
                    <strong>Type:</strong>
                    {!! Form::select('type', $documentTypes, null, array('class' => 'form-control')) !!}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6">
                <div class="form-group">
                    <label>Trạng thái</label>
                    <div>
                        {{--<span class="badge">PENDING</span>--}}
                        <span class="label label-default">INIT</span>
                    </div>
                </div>
            </div>
            <member-document is-deliver='true'></member-document>
            <member-document ></member-document>
        </div>
        <inventory-list></inventory-list>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Reason:</strong>
                    {!! Form::textarea('reason', null, array('placeholder' => 'Name','class' => 'form-control','rows' => 3)) !!}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                {{--<div class="form-group">--}}
                <strong>Attached Documents:</strong>

                {{--<div id="fileBoxId" class="fallback" class="dropzone">--}}
                {{--<input name="file" type="file" multiple />--}}
                {{--</div>--}}

                {!! Form::open([ 'route' => [ 'dropzone.store' ], 'files' => true, 'enctype' => 'multipart/form-data', 'class' => 'dropzone', 'id' => 'image-upload' ]) !!}
                <div>
                    {{--<h3>Upload Multiple File By Click On Box</h3>--}}
                </div>
                {!! Form::close() !!}

                {{--</div>--}}
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Note:</strong>
                    <br/>
                    {!! Form::textarea('note', null, array('class' => 'form-control','rows' => 3)) !!}
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6">
                <div class="form-group">
                    <label>Thời gian dự kiến trả lại/ Return Date: </label>

                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        {!! Form::text('return_date', Helper::maxDate() , array('placeholder' => 'Enter start date','class' => 'form-control datepicker pull-right','id' => 'return_date_input')) !!}
                    </div>
                    <!-- /.input group -->
                </div>
            </div>
            <div class="col-md-6"></div>
            <div class="col-xs-12 col-sm-12 col-md-6">
                <div class="form-group">
                    <strong>Người duyệt: </strong>
                    <br/>
                    {!! Form::text('arppoved_by', null, array('class' => 'form-control','disabled')) !!}
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="button" class="btn btn-primary js-submit-create-document">Submit</button>
            </div>
        </div>
        {{--</form>--}}
        {{--{!! Form::close() !!}--}}
    </div>


@endsection

@section('js')
    <script type="text/javascript">

        $(document).ready(function() {
            Dropzone.options.imageUpload = {
                maxFilesize         :       1,
                // acceptedFiles: ".jpeg,.jpg,.png,.gif"
            };

            // var myDropzone = new Dropzone("div#fileBoxId", {
            //     url: "/file/post"
            // });
            // $("div#myId").dropzone({
            //     url: "/file/post"
            // });
            $('.js-submit-create-document').click(() => {
                axios.post('/documents/create', {
                    firstName: 'Fred',
                    lastName: 'Flintstone'
                })
                .then(function (response) {
                    console.log(response);
                })
                .catch(function (error) {
                    console.log(error);
                });
            })
        });
    </script>
@endsection
