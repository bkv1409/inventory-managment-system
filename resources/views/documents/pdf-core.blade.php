<div class="header">

</div>
<div class="information">
    <table width="100%">
        <tr>
            <td align="left" style="width: 40%;">
                <h3>CÔNG TY TNHH LG ELECTRONICS VIỆT NAM HẢI PHÒNG</h3>

            </td>
            <td align="center">
                {{--<img src="/path/to/logo.png" alt="Logo" width="64" class="logo"/>--}}
            </td>
            <td align="right" style="width: 40%;">

                @if(!empty($isWeb) && $isWeb)
                    {{--                    <img src="{{asset('images/LGE_logo.png')}}" alt="Logo" width="64" class="logo"/>--}}
                    <div class="upn"></div>
                @else
                    <img src="{{ public_path() . '/images/LGE_logo.png' }}" alt="Logo" width="64" class="logo">
                @endif


            </td>
        </tr>

    </table>
    <table width="100%" >
        <tr>
            <td align="left" style="width: 20%;">

            </td>
            <td align="center">
                <h4>ASSET HANDOVER DOCUMENT</h4>
                <h4>BIÊN BẢN BÀN GIAO</h4>
                <p><i>Ngày {{$document->created_at->day}} tháng {{$document->created_at->month}} năm {{$document->created_at->year}}</i></p>
                <h5>IT WAREHOUSE {{$document->type}} ({{config('constant.DOCUMENT_TYPE')[$document->type]['value']}})</h5>
            </td>
            <td align="right" style="width: 20%;">
            </td>
        </tr>
    </table>
</div>

<div class="invoice">
    <h5>No/Số: {{$document->document_no}}</h5>
    <div id="deliver">
        <h5> - Deliver/ Đại diện bên giao</h5>
        <table width="100%" class="margin-bottom-1 margin-top">

            <tbody>
            <tr>
                <td class="text-bold">Name/ Tên: </td>
                <td>{{$document->deliver->name}}</td>
                <td align="left" class="text-bold">ID/ Email: </td>
                <td>{{$document->deliver->email}}</td>
            </tr>
            <tr>
                <td class="text-bold">Dept./ Phòng: </td>
                <td>{{$document->deliver->department}}</td>
                <td align="left" class="text-bold">Company (if not LGEVH/ Công ty (nếu khác LGEVH): </td>
                <td>{{$document->deliver->company}}</td>
            </tr>
            <tr>
                <td class="text-bold">Phone/ Điện thoại</td>
                <td>{{$document->deliver->phone_no}}</td>
                <td></td>
                <td></td>
            </tr>
            </tbody>
        </table>
    </div>
    @if($document->receiver)
        <div id="receiver">
            <h5> - Receiver/ Đại diện bên nhận tài sản</h5>
            <table width="100%" class="margin-bottom-1 margin-top">

                <tbody>
                <tr>
                    <td class="text-bold">Name/ Tên: </td>
                    <td>{{$document->receiver->name}}</td>
                    <td align="left" class="text-bold">ID/ Email: </td>
                    <td>{{$document->receiver->email}}</td>
                </tr>
                <tr>
                    <td class="text-bold">Dept./ Phòng: </td>
                    <td>{{$document->receiver->department}}</td>
                    <td align="left" class="text-bold">Company (if not LGEVH/ Công ty (nếu khác LGEVH): </td>
                    <td>{{$document->receiver->company}}</td>
                </tr>
                <tr>
                    <td class="text-bold">Phone/ Điện thoại</td>
                    <td>{{$document->receiver->phone_no}}</td>
                    <td></td>
                    <td></td>
                </tr>
                </tbody>
            </table>
        </div>
    @endif

    <h5>List of asset/ Xác nhận việc giao nhận tài sản như sau: </h5>
    <div>
        <table width="100%" class="has-border text-center margin-bottom-1 margin-top " style="border-collapse: collapse;" >
            <thead>
            <tr>
                <th class="has-border">No/ STT</th>
                <th class="has-border">Type1</th>
                <th class="has-border">Mã tài sản/ FA No</th>
                <th class="has-border">IT Tag No</th>
                <th class="has-border">Type2</th>
                <th class="has-border">Model/ Tên item</th>
                <th class="has-border">Serial Number</th>
                <th class="has-border">Q.ty/ SL</th>
                <th class="has-border">Unit/ Đơn vị tính</th>
                <th class="has-border">Trạng thái/ Status</th>
            </tr>
            </thead>
            <tbody>
            @php
                $i = 0;
            @endphp
            @foreach($document->inventories as $inventory)
                <tr>
                    <th class="has-border">{{++$i}}</th>
                    <th class="has-border">{{$inventory->type1->name}}</th>
                    <th class="has-border break-word">{{$inventory->fa_no}}</th>
                    <th class="has-border break-word">{{$inventory->it_tag}}</th>
                    <th class="has-border">{{$inventory->type2->name}}</th>
                    <th class="has-border">{{$inventory->model->model_name}}</th>
                    <th class="has-border break-word">{{$inventory->serial_number}}</th>
                    <th class="has-border">{{$inventory->quantity}}</th>
                    <th class="has-border">{{$inventory->unitData->name}}</th>
                    <th class="has-border">{{$inventory->status}}</th>
                </tr>
            @endforeach
            </tbody>

            <tfoot>
            <tr>
                <td colspan="2">Total</td>
                <td align="center" style="background-color:LightGray;" colspan="8">{{$i}}</td>
            </tr>
            </tfoot>
        </table>
    </div>

    <div class="margin-bottom">
        @php
            $i = 0;
        @endphp
        @foreach($repairInventories as $repairInventory)
            @if(count($repairInventory->data) > 0)
                <h5>Danh sách Parts cho Device No. : {{$i +1 }} - Internal ID: {{$repairInventory->id}} - S/N: {{ $repairInventory->obj->serial_number}}:  </h5>
                <table width="100%" class="has-border text-center margin-bottom-1 margin-top " style="border-collapse: collapse;" >
                    <thead>
                    <tr>
                        <th class="has-border">No/ STT</th>
                        <th class="has-border">Type1</th>
                        <th class="has-border">Mã tài sản/ FA No</th>
                        <th class="has-border">IT Tag No</th>
                        <th class="has-border">Type2</th>
                        <th class="has-border">Model/ Tên item</th>
                        <th class="has-border">Serial Number</th>
                        <th class="has-border">Q.ty/ SL</th>
                        <th class="has-border">Unit/ Đơn vị tính</th>
                        <th class="has-border">Trạng thái/ Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php
                        $j = 0;
                    @endphp
                    @foreach($repairInventory->data as $repair)
                        <tr>
                            <th class="has-border">{{$i + 1}}.{{++$j}}</th>
                            <th class="has-border">{{$repair->type1_name}}</th>
                            <th class="has-border break-word">{{$repair->fa_no}}</th>
                            <th class="has-border break-word">{{$repair->it_tag}}</th>
                            <th class="has-border">{{$repair->type2_name}}</th>
                            <th class="has-border">{{$repair->model_name}}</th>
                            <th class="has-border break-word">{{$repair->serial_number}}</th>
                            <th class="has-border">{{$repair->quantity}}</th>
                            <th class="has-border">{{$repair->unit_name}}</th>
                            <th class="has-border">{{$repair->status}}</th>
                        </tr>
                    @endforeach
                    </tbody>

                    <tfoot>
                    <tr>
                        <td colspan="2">Total</td>
                        <td align="center" style="background-color:LightGray;" colspan="8">{{$j}}</td>
                    </tr>
                    </tfoot>
                </table>
                @php
                    $i++;
                @endphp
            @endif

        @endforeach
    </div>

    <div class="margin-bottom-1">
        <h5>Reason/ Lý do:</h5>
        <p>{{$document->reason}}</p>
    </div>
    <div class="margin-bottom-1">
        <h5>Attached Documents:</h5>
        <div class="">
            @foreach($document->files as $file)
                <div>{{$file->file_name}}</div>
            @endforeach
        </div>
    </div>
    <div class="margin-bottom-1">
        <h5>Note/ Ghi chú:</h5>
        <p>{{$document->note}}</p>
    </div>
    <div class="margin-bottom-1">
        <h5>Thời gian dự kiến trả lại/ Return Date: </h5>
        <div style="font-size: 14px;">{{$document->return_date}}</div>
    </div>
    <div class="margin-bottom-1">
        <p>There are ... copies of this documents. For any request related to device/ equipment need to show a copy for evident</p>
        <p>Biên bản này được lập thành ... bộ. Khi có bất kỳ yêu cầu nào liên quan đến thiết bị cần mang biên bản này làm chứng</p>
    </div>
    <div>
        <p>
            Copies of this document are kept by/ Các bên giữ biên bản:
        </p>
        <div>
            - Deliver/ Bên quản lý tài sản: .... bộ
        </div>
        <div>
            - Receiver/ Bên nhận tài sản: .... bộ
        </div>
    </div>
    <table width="100%" style="text-align: center!important;margin: 0px auto!important;">
        <thead>
        <th style="text-align: center!important;margin: 0px auto!important;"><b>Đại diện bên quản lý tài sản</b><br/><i>(Ký họ tên, đóng dấu)</i></th>
        <th style="text-align: center!important;margin: 0px auto!important;"><b>Người nhận</b><br/><i>(Ký, họ tên)</i></th>
        <th style="text-align: center!important;margin: 0px auto!important;"><b>Người nhận</b><br/><i>(Ký, họ tên)</i></th>
        </thead>
        <tbody>
        <tr>
            <td>
                <br />
            </td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>
                <br />
            </td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>
                <br />
            </td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>{{Auth::user()->name}}</td>
            <td>{{$document->deliver->name}}</td>
            <td>{{$document->receiver ? $document->receiver->name : 'N/A'}}</td>
        </tr>
        </tbody>
    </table>
</div>

<br/>
<div class="information" >
    <p><i>Tôi hiểu và hoàn toàn tuân thủ theo quy định quản lý tài sản dưới đây của Công ty:</i></p>
    <ol>
        <li>Không tự ý chuyển giao thiết bị này cho người khác. Trong trường hợp không có nhu cầu sử dụng thiết bị phải trả lại bộ phận quản lý tài sản</li>
        <li>Luôn đảm bảo bảo quản máy tính/thiết bị ở trạng thái tốt nhất</li>
        <li>Phải trả lại toàn bộ chi phí trong trường hợp làm mất hoặc làm hỏng thiết bị (ngoại trừ lỗi của nhà sản xuất)</li>
    </ol>
</div>
