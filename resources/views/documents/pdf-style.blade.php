<style type="text/css">
    @page {
        margin: 0px;
    }
    body {
        margin: 0px !important;
    }
    /** {*/
    /*font-family: Verdana, Arial, sans-serif;*/
    /*}*/
    @font-face {
        /*font-family: 'Helvetica';*/
        font-family: 'DejaVu Sans';

        font-weight: normal;
        font-style: normal;
        font-variant: normal;
        /*src: url('/public/css/google_fonts_Source_Sans_Pro.css');*/
        font-size: 14px;
    }
    body {
        font-family: DejaVu Sans, sans-serif;
    }

    h5{
        margin: 0.25rem !important;
    }
    h4{
        margin: 0.25rem !important;
    }
    p {
        margin: 0px !important;
    }
    a {
        color: #fff;
        text-decoration: none;
    }
    table {
        /*font-size: x-small;*/
        font-size: small;
    }
    tfoot tr td {
        /*font-weight: bold;*/
        /*font-size: x-small;*/
    }
    .text-center {
        text-align: center;
    }
    .invoice table {
        margin: 15px;
    }
    .invoice h3 {
        margin-left: 15px;

    }
    .information {
        /*background-color: #60A7A6;*/
        background-color: #FFF;
        /*color: #FFF;*/
        /*color: #000000;*/
    }
    .information .logo {
        margin: 5px;
    }
    .information table {
        /*padding: 10px;*/
    }
    .container {
        margin-left: 1rem !important;
        margin-right: 1.5rem !important;
    }
    .margin-bottom {
        margin-bottom: 0.25rem !important;
    }
    .margin-bottom-1 {
        margin-bottom: 0.5rem !important;
    }
    .margin-top {
        margin-top: 0.25rem !important;
    }
    .margin-top-1 {
        margin-top: 0.5rem !important;
    }
    .has-border {
        border: 1px solid black;
    }
    .upn {
        background-image:url("{{ asset('images/LGE_logo.png') }}");
        background-repeat:no-repeat;
        width:80px;
        height:80px;
        /*position:absolute;*/
        /*background-position: center;*/
        position: relative;
        background-size: cover;
    }
    .text-bold { font-weight: bold; }
    .break-word {
        word-wrap: break-word;
    }

</style>
