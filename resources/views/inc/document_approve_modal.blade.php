<div class="modal fade" id="model_document_approve_{{$document->id}}" tabindex="-1" role="dialog" aria-labelledby="approveModelTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="approveModelTitle">Approve Dialog</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('documents.approve',$document->id) }}"
                      method="GET"
                      id="form_approve_document_{{$document->id}}">
                    @csrf
                    {{--<textarea name="approve_reason" required rows="3"--}}
                              {{--minlength="30" maxlength="150" cols="30" class="w-100"--}}
                              {{--placeholder="Enter Rejected Reason, Min 30 Character, Max 150 Character"--}}
                    {{--></textarea>--}}
                    {{-- no required --}}
                    <textarea name="reject_reason" rows="3"
                              cols="30" class="w-100"
                              placeholder="Enter Approve Reason"
                    ></textarea>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" form="form_approve_document_{{$document->id}}">Confirm Approve</button>
            </div>
        </div>
    </div>
</div>