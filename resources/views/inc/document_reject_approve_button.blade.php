@if($document->status == "PENDING")
    @can('document-approve')
        {{--<a class="btn btn-primary " href="{{ route('documents.approve',$document->id) }}">Approve</a>--}}
        <button type="button" class="btn btn-primary " data-toggle="modal" data-target="#model_document_approve_{{$document->id}}">
            Approve
        </button>
    @endcan


    <!-- Button trigger modal -->
    @can('document-approve')
        <button type="button" class="btn btn-danger " data-toggle="modal" data-target="#model_document_reject_{{$document->id}}">
            Reject
        </button>
    @endcan

@endif

@if($document->status != "REJECTED")
    @can('document-edit')
        <a class="btn btn-microsoft" id="model_document_edit_files_{{$document->id}}"
           href="{{route('documents.editFiles',$document->id)}}" >
            Update Files
        </a>
    @endcan
@endif