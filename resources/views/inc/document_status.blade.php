
@switch($documentStatus)
    @case(config('constant.DOCUMENT_STATUS.PENDING'))
        <span class="label label-default">{{config('constant.DOCUMENT_STATUS.PENDING')}}</span>
    @break

    @case(config('constant.DOCUMENT_STATUS.APPROVED'))
        <span class="label label-success">{{config('constant.DOCUMENT_STATUS.APPROVED')}}</span>
    @break
    @case(config('constant.DOCUMENT_STATUS.REJECTED'))
        <span class="label label-danger">{{config('constant.DOCUMENT_STATUS.REJECTED')}}</span>
    @break

    @default
            <span class="label label-warning">N/A</span>
@endswitch