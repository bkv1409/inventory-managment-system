@extends('layouts.master')

@section('content')

    @if(session('member_save_success'))
        <div class="alert alert-success">
            <strong>{!! session('member_save_success') !!}</strong>
        </div>
    @endif
    @if(session('member_delete_success'))
        <div class="alert alert-success">
            <strong>{!! session('member_delete_success') !!}</strong>
        </div>
    @endif

    @if(session('member_delete_error'))
        <div class="alert alert-warning alert-dismissable">
            <strong>{!! session('member_delete_error') !!}</strong>
        </div>
    @endif





    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Member</h2>
            </div>
            <div class="pull-right">
                @can('config-create')
                <a class="btn btn-success" href="{{route('member.create') }}"> Create New member</a>
                @endcan
            </div>

            <div class="pull-right" style="margin-right:5px ">
                @can('config-create')
                    <a class="btn btn-success" href="{{route('member.import') }}"> Import Member</a>
                @endcan
            </div>
        </div>
    </div>

    @include ('member.searchForm',array('input'=>$input))


    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Name</th>
            <th>LG ID</th>
            <th>Status</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Domain Account</th>
            <th>Department</th>
            <th>Company</th>
            <th width="220px">Action</th>
        </tr>
        @foreach ($list_member as $member)
            <tr>
                <td>{{ ++$i }}</td>
                <td>{{ $member->name }}</td>
                <td>{{ $member->lg_id }}</td>
                <td>{{ $member->status }}</td>
                <td>{{ $member->email }}</td>
                <td>{{ $member->phone_no }}</td>
                <td>{{ $member->domain_account }}</td>
                <td>{{ $member->department }}</td>
                <td>{{ $member->company }}</td>

                <td>
                    <a class="btn btn-info" href="{{ route('member.show',$member->id) }}">Show</a>
                    @can('config-edit')
                    <a class="btn btn-primary"
                       href="{{ route('member.edit',$member->id) }}">Edit</a>
                    @endcan
                    @can('config-delete')
                    {!! Form::open(['method' => 'DELETE','route' => ['member.destroy', $member->id],'style'=>'display:inline']) !!}
                    {!! Form::submit('Delete', ['class' => 'btn btn-danger','onclick' => 'return CustomJs.ConfirmDelete()']) !!}
                    {!! Form::close() !!}
                    @endcan
                </td>
            </tr>
        @endforeach
    </table>
    {!! $list_member->links() !!}

@stop