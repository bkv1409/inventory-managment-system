{!! Form::open(array('route' => 'member.index','method'=>'get','id'=>'form_clear_all')) !!}
<div class="row">
    <div class="col-md-12">
        <div class="form-group col-md-3">
            <div class="form-group">
                <strong>Name</strong>
                @if (isset($input['name']))
                    {!! Form::text('name',$input['name'], ['class' => 'form-control','id' => 'name']) !!}
                @else
                    {!! Form::text('name',null, ['class' => 'form-control',
                'id' => 'name','placeholder' =>'Member Name']) !!}
                @endif

            </div>
        </div>
        <div class="form-group col-md-3">
            <div class="form-group">
                <strong>Email</strong>
                @if (isset($input['email']))
                    {!! Form::text('email',$input['email'], ['class' => 'form-control','id' => 'email']) !!}
                @else
                    {!! Form::text('email',null, ['class' => 'form-control',
                'id' => 'email','placeholder' =>'Email']) !!}
                @endif

            </div>
        </div>
        <div class="form-group col-md-3">
            <div class="form-group">
                <strong>Phone number</strong>
                @if (isset($input['phone_no']))
                    {!! Form::text('phone_no',$input['phone_no'], ['class' => 'form-control','id' => 'phone_no']) !!}
                @else
                    {!! Form::text('phone_no',null, ['class' => 'form-control',
                'id' => 'name','placeholder' =>'Phone number']) !!}
                @endif
            </div>
        </div>
        <div class="form-group col-md-3">
            <div class="form-group">
                <strong>Company</strong>

                @if (isset($input['company']))
                    {!! Form::text('company',$input['company'], ['class' => 'form-control','id' => 'company']) !!}
                @else
                    {!! Form::text('company',null, ['class' => 'form-control',
                'id' => 'company','placeholder' =>'Company']) !!}
                @endif

            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="form-group col-md-4 "></div>
    <div class="form-group col-md-4 "></div>
    {{--<div class="form-group col-md-4">--}}
        {{--<div class="form-group col-md-5">--}}
            {{--{!! Form::submit('Export', array('class' => 'btn btn-info center-block','name'=>'btnsmt')) !!}--}}
        {{--</div>--}}
        {{--<div class="form-group col-md-5">--}}
            {{--{!! Form::submit('Search', array('class' => 'btn btn-info center-block','name'=>'btnsmt')) !!}--}}
        {{--</div>--}}

        {{--<div class="form-group col-md-2">--}}
            {{--<a href="{{ route('member.index') }}" class="btn btn-danger" role="button">Reset</a>--}}
        {{--</div>--}}
    {{--</div>--}}
    <div class="form-group col-md-4 ">
        <div class="form-group col-md-4">
            {!! Form::submit('Search', array('class' => 'btn btn-primary center-block')) !!}
        </div>
        <div class="form-group col-md-4">
            {!! Form::button('Reset', array('class' => 'btn btn-light center-block','id'=>'btn_clear_all')) !!}
        </div>
        <div class="form-group col-md-4">
            {!! Form::button('Export', array('class' => 'btn btn-info center-block','id'=>'btn_export')) !!}
        </div>
    </div>


</div>
{!! Form::close() !!}
