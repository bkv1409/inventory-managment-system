
@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> Create member</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('member.index') }}"> Back</a>
            </div>
        </div>
    </div>

    <form action="{{ route('member.store') }}" method="POST">
        @csrf


        <div class="row">
            <div class=" col-md-6">

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Name</strong>
                        <input type="text" name="name" value ="{{old('name')}}" class="form-control" placeholder="Member Name">
                        @if ($errors->has('name'))
                            <p class="help-block" style="color:red;font-weight:bold">*{{ $errors->first('name') }}</p>
                        @endif
                    </div>

                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Lg Id</strong>
                        <input type="lg_id" name="lg_id" value ="{{old('lg_id')}}" class="form-control" placeholder="Lg id">
                        @if ($errors->has('lg_id'))
                            <p class="help-block" style="color:red;font-weight:bold">*{{ $errors->first('lg_id') }}</p>
                        @endif
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Email</strong>
                        <input type="text" name="email" class="form-control"  value ="{{old('email')}}"placeholder="Member Email">
                        @if ($errors->has('email'))
                            <p class="help-block" style="color:red;font-weight:bold">*{{ $errors->first('email') }}</p>
                        @endif
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Phone number</strong>
                        <input type="phone_no" name="phone_no" class="form-control" value ="{{old('phone_no')}}" placeholder="Phone number">
                        @if ($errors->has('phone_no'))
                            <p class="help-block" style="color:red;font-weight:bold">*{{ $errors->first('phone_no') }}</p>
                        @endif
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Is Member</strong>
                        {!! Form::select('is_lg_member',$is_member, old('is_lg_member'),['class' => 'form-control']) !!}

                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Status</strong>
                        {!! Form::select('status',$status_member, old('status'),['class' => 'form-control']) !!}

                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Department</strong>
                        <input type="text" name="department" value ="{{old('department')}}" class="form-control" placeholder="Department">
                    </div>
                </div>


                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Company</strong>
                        <input type="text" name="company" value ="{{old('company')}}" class="form-control" placeholder="Company">
                        @if ($errors->has('company'))
                            <p class="help-block" style="color:red;font-weight:bold">*{{ $errors->first('company') }}</p>
                        @endif

                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Domain Account</strong>
                        <input type="text" name="domain_account" value ="{{old('domain_account')}}" class="form-control" placeholder="Domain Account">
                        @if ($errors->has('domain_account'))
                            <p class="help-block" style="color:red;font-weight:bold">*{{ $errors->first('domain_account') }}</p>
                        @endif

                    </div>
                </div>



                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>

            </div>


        </div>

    </form>



@stop







