@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> Update member</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('member.index') }}"> Back</a>
            </div>
        </div>
    </div>

    {!! Form::Model($row,['route' => ['member.update', $row->id]]) !!}
    @csrf
    @method('PUT')


        <div class="row">
            <div class=" col-md-6">

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Name</strong>
                        {!! Form::text('name',null, ['class' => 'form-control','id' => 'name','placeholder' =>'Member Name']) !!}
                        @if ($errors->has('name'))
                            <p class="help-block" style="color:red;font-weight:bold">*{{ $errors->first('name') }}</p>
                        @endif
                    </div>

                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Lg Id</strong>
                        {!! Form::text('lg_id',null, ['class' => 'form-control','id' => 'lg_id','placeholder' =>'Lg id']) !!}
                        @if ($errors->has('lg_id'))
                            <p class="help-block" style="color:red;font-weight:bold">*{{ $errors->first('lg_id') }}</p>
                        @endif
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Email</strong>
                        {!! Form::text('email',null, ['class' => 'form-control','id' => 'email','placeholder' =>'Member Email']) !!}
                        @if ($errors->has('email'))
                            <p class="help-block" style="color:red;font-weight:bold">*{{ $errors->first('email') }}</p>
                        @endif
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Phone number</strong>
                        {!! Form::text('phone_no',null, ['class' => 'form-control','id' => 'phone_no','placeholder' =>'Phone number']) !!}
                        @if ($errors->has('phone_no'))
                            <p class="help-block" style="color:red;font-weight:bold">*{{ $errors->first('phone_no') }}</p>
                        @endif
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Is Member</strong>
                        {!! Form::select('is_lg_member',$is_member, null,['class' => 'form-control']) !!}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Status</strong>
                        {!! Form::select('status',$status_member, old('is_lg_member'),['class' => 'form-control']) !!}

                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Department</strong>
                        {!! Form::text('department',null, ['class' => 'form-control','id' => 'department','placeholder' =>'Department']) !!}
                    </div>
                </div>


                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Company</strong>
                        {!! Form::text('company',null, ['class' => 'form-control','id' => 'company','placeholder' =>'Company']) !!}
                        @if ($errors->has('company'))
                            <p class="help-block" style="color:red;font-weight:bold">*{{ $errors->first('company') }}</p>
                        @endif

                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Domain Account</strong>
                        {!! Form::text('domain_account',null, ['class' => 'form-control','id' => 'domain_account','placeholder' =>'Domain Account']) !!}
                        @if ($errors->has('domain_account'))
                            <p class="help-block" style="color:red;font-weight:bold">*{{ $errors->first('domain_account') }}</p>
                        @endif

                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>

            </div>


        </div>

    </form>




@stop

