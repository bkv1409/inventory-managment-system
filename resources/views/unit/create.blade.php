
@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> Create Unit</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('unit.index') }}"> Back</a>
            </div>
        </div>
    </div>

    <form action="{{ route('unit.store') }}" method="POST">
        @csrf


        <div class="row">
            <div class=" col-md-6">

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Name</strong>
                        <input type="text" name="name" class="form-control" placeholder="unit Name">
                        @if ($errors->has('name'))
                            <p class="help-block" style="color:red;font-weight:bold">*{{ $errors->first('name') }}</p>
                        @endif
                    </div>

                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>

            </div>


        </div>

    </form>



@stop







