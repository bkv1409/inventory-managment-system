@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> Update unit</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('unit.index') }}"> Back</a>
            </div>
        </div>
    </div>

    {!! Form::Model($row,['route' => ['unit.update', $row->id]]) !!}
    @csrf
    @method('PUT')


        <div class="row">
            <div class=" col-md-6">

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Name</strong>
                        {!! Form::text('name',null, ['class' => 'form-control','id' => 'name','placeholder' =>'unit Name']) !!}
                        @if ($errors->has('name'))
                            <p class="help-block" style="color:red;font-weight:bold">*{{ $errors->first('name') }}</p>
                        @endif
                    </div>

                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>

            </div>


        </div>

    </form>




@stop

