{!! Form::open(array('route' => 'unit.index','method'=>'get','id'=>'form_clear_all')) !!}
<div class="row">
    <div class="col-md-12">
        <div class="form-group col-md-3">
            <div class="form-group">
                @if (isset($input['name']))
                    {!! Form::text('name',$input['name'], ['class' => 'form-control','id' => 'name']) !!}
                @else
                    {!! Form::text('name',null, ['class' => 'form-control',
                'id' => 'name','placeholder' =>'Unit Name']) !!}
                @endif
            </div>
        </div>

        <div class="form-group col-md-1">

            <div class="form-group ">
                {!! Form::submit('Search', array('class' => 'btn btn-info ')) !!}
            </div>

        </div>

        <div class="form-group col-md-1">

            <div class="form-group ">
                {!! Form::button('Reset', array('class' => 'btn btn-danger ','id'=>'btn_clear_all')) !!}
            </div>

        </div>

    </div>

</div>

{!! Form::close() !!}
