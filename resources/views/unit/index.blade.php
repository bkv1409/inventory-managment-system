@extends('layouts.master')


@section('content')

    @if(session('unit_save_success'))
        <div class="alert alert-success">
            <strong>{!! session('unit_save_success') !!}</strong>
        </div>
    @endif
    @if(session('unit_delete_success'))
        <div class="alert alert-success">
            <strong>{!! session('unit_delete_success') !!}</strong>
        </div>
    @endif

    @if(session('unit_delete_error'))
        <div class="alert alert-warning alert-disable">
            <strong>{!! session('unit_delete_error') !!}</strong>
        </div>
    @endif


    @include ('unit.searchForm',array('input'=>$input))


    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Unit</h2>
            </div>
            <div class="pull-right">
                @can('config-create')
                <a class="btn btn-success" href="{{route('unit.create') }}"> Create new unit</a>
                    @endcan
            </div>
        </div>
    </div>


    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Name</th>
            <th width="220px">Action</th>
        </tr>
        @foreach ($list_unit as $unit)
            <tr>
                <td>{{ ++$i }}</td>
                <td>{{ $unit->name }}</td>

                <td>
                    @can('config-edit')
                    <a class="btn btn-primary"
                       href="{{ route('unit.edit',$unit->id) }}">Edit</a>
                    @endcan

                    @can('config-delete')
                    {!! Form::open(['method' => 'DELETE','route' => ['unit.destroy', $unit->id],'style'=>'display:inline']) !!}
                    {!! Form::submit('Delete', ['class' => 'btn btn-danger','onclick' => 'return CustomJs.ConfirmDelete()']) !!}
                    {!! Form::close() !!}
                    @endcan

                </td>
            </tr>
        @endforeach
    </table>
    {!! $list_unit->links() !!}

@stop