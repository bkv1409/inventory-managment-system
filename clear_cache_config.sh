php artisan config:clear
php artisan cache:clear
php artisan route:clear
php artisan view:clear
supervisorctl restart ims-worker:*
chmod -R 777 public
